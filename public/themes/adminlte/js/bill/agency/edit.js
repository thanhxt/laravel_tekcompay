var app = angular.module('Bill', ['ngMaterial', 'ngMessages', 'ngMaterialDatePicker']);
app.config(function ($httpProvider) {
    // Enable cross domain calls
    $httpProvider.defaults.useXDomain = true;
    // Remove the header used to identify ajax call  that would prevent CORS from working
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
});
app.controller('BillCtrl', function($rootScope, $scope, $element, $location) {
    xxx = $scope;
    $scope.IsDisabled = false;
    $scope.searchTerm = null;
    $scope.clearSearchTerm = function() {
        $scope.searchTerm = '';
    };
    $element.find('input').on('keydown', function(ev) {
        ev.stopPropagation();
    });
	
    $scope.formData = {
        fee: null,
        id: null,
        fee_amount: null,
        base_amount: null,
        machine_list: null,
        bill_machine: null,
        bill_detail: null,
        bill_image: null,
        is_submit: 0
    };
    $scope.$watch('formData.total', function(newValue, oldValue) {
        $scope.formData.real_payment = $scope.formData.total;
        return $scope.formData.real_payment;
    });
   
    $scope.uploadBillImage = function(bill_detail_id){

    };
   
	$scope.convertToNumber = function(value){
        return parseFloat(value);
    };

    $scope.calFee = function(fee, total){
		var fee_amount = parseFloat(fee) * total / 100;
		$scope.formData.fee_amount = fee_amount;
        return fee_amount;
    };
	
    $scope.removeImage = function(imageId) {
       $.ajax({
           url: '/backend/bill/bill/remove-image',
           type: 'POST',
           data: {id:imageId},
           success: function (resp, success) {
               window.location.reload();
           }
       });
    };
	
    $scope.showImage = function(data) {
       var result = false;
	   if (data.image_url != '') {
		   result = true;
	   }
	   return result;
    };
	
    $scope.notify = function(msg, type) {
        $.notify({
            // options
            message: msg
        },{
            // settings
            type: type
        });

    };
	
	$scope.secret = function () {
		return "";
	};
	
    $scope.initData = function () {
        if ($scope.$errorInitData) {
            console.log('error init data');
        }
        if ($scope.$data) {
            $scope.data = $scope.$data;
        }
        if ($scope.data) {
            $scope.formData = $scope.data;

        }
    };
    $scope.$on("fileProgress", function(e, progress) {
        $scope.progress = progress.loaded / progress.total;
    });
    $scope.initData();
	
	 $scope.submitData = function () {
		$scope.formData.is_submit = 1;
    };

});
/* ========================================================================================= */
app.run(['$rootScope', '$http', '$locale', function ($rootScope, $http, $locale, $scope) {
    if ($('#bill-id').length > 0) {
        var batch_id = $('#bill-id').val();
        var url = '/backend/bill/bill/get-data?id=' + batch_id;
        $.ajax({
            method: 'GET',
            url: url,
            async: false,
            success: function (response) {
                if (response.status && response.data) {
					
					var data = response.data;
					// calculate total of machines ;
					var machines = [];
					angular.forEach(data.bill_machine, function(machine, key){
						var total = 0;
						angular.forEach(data.bill_detail, function(value, key){
							if(value.batch_pos_id==machine.id ){
								total = total + parseFloat(value.amount) ;
							}
						});
						machine.total = total;
						machines.push(machine);
						
					});
					data.bill_machine = machines;
					//end calculate machines;
                    $rootScope.$data = data;
					
                    $rootScope.$apply();
                } else {
                    $rootScope.$errorInitData = true;
                }
            },
            error: function () {
                $rootScope.$errorInitData = true;
            }
        });
    }
}]);

app.directive('rsTime', ['$interval', '$filter',
    function($interval, $filter) {
        return clock($interval, $filter);
    }
]);

app.directive("imgUpload",function($http,$compile){
    return {
        restrict : 'AE',
        template : 	'<input class="fileUpload" type="file" multiple data-id="{{info.id}}" />'+
            '<div class="dropzone">'+
            '<p class="msg">Click or Drag and Drop files to upload</p>'+
            '</div>'+
            '<div class="preview clearfix">'+
            '<div class="previewData clearfix" ng-repeat="data in previewData track by $index">'+
            '<img src={{data.src}}></img>'+
            '<div class="previewDetails">'+
            '<div class="detail"><b>Name : </b>{{data.name}}</div>'+
            '<div class="detail"><b>Type : </b>{{data.type}}</div>'+
            '<div class="detail"><b>Size : </b> {{data.size}}</div>'+
            '</div>'+
            '<div class="previewControls">'+
            '<span ng-click="upload(data)" class="circle upload">'+
            '<i class="fa fa-check"></i>'+
            '</span>'+
            '<span ng-click="remove(data)" class="circle remove">'+
            '<i class="fa fa-close"></i>'+
            '</span>'+
            '</div>'+
            '</div>'+
            '</div>',
        link : function(scope,elem,attrs){
            var formData = new FormData();
            scope.previewData = [];

            function previewFile(file, item_id){
                var reader = new FileReader();
                var obj = new FormData().append('file',file);
                reader.onload=function(data){
                    var src = data.target.result;
                    var size = ((file.size/(1024*1024)) > 1)? (file.size/(1024*1024)) + ' mB' : (file.size/		1024)+' kB';
                    scope.$apply(function(){
                        scope.previewData.push({'name':file.name,'size':size,'type':file.type,
                            'src':src,'data':obj, 'item_id': item_id});
                    });
                }
                reader.readAsDataURL(file);
            }

            function uploadFile(e,type){
                e.preventDefault();
                var files = "";
                var item_id = e.currentTarget.getAttribute('data-id');
                if(type == "formControl"){
                    files = e.target.files;
                } else if(type === "drop"){
                    files = e.originalEvent.dataTransfer.files;
                }
                console.log(files);
                for(var i=0;i<files.length;i++){
                    var file = files[i];
                    if(file.type.indexOf("image") !== -1){
                        previewFile(file, item_id);
                    } else {
                        alert(file.name + " is not supported");
                    }
                }
            }
            elem.find('.fileUpload').bind('change',function(e){
                uploadFile(e,'formControl');
            });

            elem.find('.dropzone').bind("click",function(e){
                $compile(elem.find('.fileUpload'))(scope).trigger('click');
            });

            elem.find('.dropzone').bind("dragover",function(e){
                e.preventDefault();
            });

            elem.find('.dropzone').bind("drop",function(e){
                uploadFile(e,'drop');
            });
            scope.upload=function(obj){
                $.ajax({
                    method: 'POST',
                    url:    '/backend/bill/bill/upload-image',
                    data: { data: obj},
                    success:function (response) {
                        window.location.reload();
                    }
                });
                var index= scope.previewData.indexOf(obj);
                scope.previewData.splice(index,1);
            };

            scope.remove=function(data){
                var index= scope.previewData.indexOf(data);
                scope.previewData.splice(index,1);
            }
        }
    }
});
/**
 Format Time
 */
function clock($interval, $filter) {
    return {
        restrict: 'EA',
        scope: {
            live: '=isLive',
            digitalFormat: '=digitalFormat',
            value: '=startTime',
            diff: '=diff'
        },
        template: '{{digital}}',
        link: function(scope, element, attrs){
            var config = {};
            config.formate = scope.digitalFormat || 'hh:mm a';
            config.value = scope.value;
            config.diff= scope.diff || false;
            scope.digital = logicC(config);
            if(scope.live){
                $interval(function(){
                    scope.digital = logicC(config);
                },1000);
            }else{
                scope.digital = logicC(config);
            }
        }
    }
}

Date.prototype.format = function (format, utc){
    return formatDate(this, format, utc);
};




function logicC(config){

    if(config.diff){
        return daysBetween(new Date(config.value*1000), new Date());
    }
    else if(config.value != undefined){
        return new Date(config.value*1000).format(config.formate);
    } else{
        return (new Date().format(config.formate));
    }
}



function formatDate(date, format, utc){
    var MMMM = ["\x00", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var MMM = ["\x01", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var dddd = ["\x02", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var ddd = ["\x03", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    function ii(i, len) {
        var s = i + ""; len = len || 2;
        while (s.length < len) s = "0" + s; return s;
    }

    var y = utc ? date.getUTCFullYear() : date.getFullYear();
    format = format.replace(/(^|[^\\])yyyy+/g, "$1" + y);
    format = format.replace(/(^|[^\\])yy/g, "$1" + y.toString().substr(2, 2));
    format = format.replace(/(^|[^\\])y/g, "$1" + y);

    var M = (utc ? date.getUTCMonth() : date.getMonth()) + 1;
    format = format.replace(/(^|[^\\])MMMM+/g, "$1" + MMMM[0]);
    format = format.replace(/(^|[^\\])MMM/g, "$1" + MMM[0]);
    format = format.replace(/(^|[^\\])MM/g, "$1" + ii(M));
    format = format.replace(/(^|[^\\])M/g, "$1" + M);

    var d = utc ? date.getUTCDate() : date.getDate();
    format = format.replace(/(^|[^\\])dddd+/g, "$1" + dddd[0]);
    format = format.replace(/(^|[^\\])ddd/g, "$1" + ddd[0]);
    format = format.replace(/(^|[^\\])dd/g, "$1" + ii(d));
    format = format.replace(/(^|[^\\])d/g, "$1" + d);

    var H = utc ? date.getUTCHours() : date.getHours();
    format = format.replace(/(^|[^\\])HH+/g, "$1" + ii(H));
    format = format.replace(/(^|[^\\])H/g, "$1" + H);

    var h = H > 12 ? H - 12 : H == 0 ? 12 : H;
    format = format.replace(/(^|[^\\])hh+/g, "$1" + ii(h));
    format = format.replace(/(^|[^\\])h/g, "$1" + h);

    var m = utc ? date.getUTCMinutes() : date.getMinutes();
    format = format.replace(/(^|[^\\])mm+/g, "$1" + ii(m));
    format = format.replace(/(^|[^\\])m/g, "$1" + m);

    var s = utc ? date.getUTCSeconds() : date.getSeconds();
    format = format.replace(/(^|[^\\])ss+/g, "$1" + ii(s));
    format = format.replace(/(^|[^\\])s/g, "$1" + s);

    var f = utc ? date.getUTCMilliseconds() : date.getMilliseconds();
    format = format.replace(/(^|[^\\])fff+/g, "$1" + ii(f, 3));
    f = Math.round(f / 10);
    format = format.replace(/(^|[^\\])ff/g, "$1" + ii(f));
    f = Math.round(f / 10);
    format = format.replace(/(^|[^\\])f/g, "$1" + f);

    var T = H < 12 ? "AM" : "PM";
    format = format.replace(/(^|[^\\])TT+/g, "$1" + T);
    format = format.replace(/(^|[^\\])T/g, "$1" + T.charAt(0));

    var t = T.toLowerCase();
    format = format.replace(/(^|[^\\])tt+/g, "$1" + t);
    format = format.replace(/(^|[^\\])t/g, "$1" + t.charAt(0));

    var tz = -date.getTimezoneOffset();
    var K = utc || !tz ? "Z" : tz > 0 ? "+" : "-";
    if (!utc)
    {
        tz = Math.abs(tz);
        var tzHrs = Math.floor(tz / 60);
        var tzMin = tz % 60;
        K += ii(tzHrs) + ":" + ii(tzMin);
    }
    format = format.replace(/(^|[^\\])K/g, "$1" + K);

    var day = (utc ? date.getUTCDay() : date.getDay()) + 1;
    format = format.replace(new RegExp(dddd[0], "g"), dddd[day]);
    format = format.replace(new RegExp(ddd[0], "g"), ddd[day]);

    format = format.replace(new RegExp(MMMM[0], "g"), MMMM[M]);
    format = format.replace(new RegExp(MMM[0], "g"), MMM[M]);

    format = format.replace(/\\(.)/g, "$1");

    return format;
};



var daysBetween = function( date1, date2 ) {
    //Get 1 day in milliseconds
    var one_day=1000*60*60*24;
    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();
    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;
    //take out milliseconds
    difference_ms = difference_ms/1000;
    var seconds = Math.floor(difference_ms % 60);
    difference_ms = difference_ms/60;
    var minutes = Math.floor(difference_ms % 60);
    difference_ms = difference_ms/60;
    var hours = Math.floor(difference_ms % 24);
    var days = Math.floor(difference_ms/24);
    console.log(seconds,hours,minutes);
    if(days > 0){
        if(days>31){
            days = humanise(days);
        }else{
            days = days +" Days"
        }
        return days;
    }else{
        if(hours > 0){
            return hours+" Hour"+(hours > 1 ? 's' : '') ;
        }
        if(minutes > 0){
            return minutes+" Minute"+(minutes > 1 ? 's' : '');
        }

        if(seconds > 0){
            return seconds+" Second"+(seconds > 1 ? 's' : '');
        }
    }
}

function humanise(diff) {
    // The string we're working with to create the representation
    var str = '';
    // Map lengths of `diff` to different time periods
    var values = [[' Year', 365], [' Month', 30], [' Day', 1]];
    var tmp = 0;
    // Iterate over the values...
    for (var i=0;i<values.length;i++) {
        var amount = Math.floor(diff / values[i][1]);

        // ... and find the largest time value that fits into the diff
        if (amount >= 1 && tmp < 2) {
            // If we match, add to the string ('s' is for pluralization)
            str += amount + values[i][0] + (amount > 1 ? 's' : '') + ' ';
            tmp++;
            // and subtract from the diff
            diff -= amount * values[i][1];
        }
    }

    return str;
}