<?php

return [
	'system' => [
        'common' => [
			'not assign' => 'N/A',
			'currency txt' => 'đ',
			'currency txt VND' => 'VND',
		],
    ],
    'button' => [
        'member create' => 'Tạo mới người dùng',
        'search' => 'Tìm kiếm',
    ],
	'icon' => [
		'caption view' => 'Xem chi tiết',
		'caption edit' => 'Sửa',
		'caption delete' => 'Xóa',
	],
    'messages' => [
        'user created' => 'Người dùng mới vừa được tạo',
        'user update' => 'Người dùng vừa được cập nhật',
        'user deleted' => 'Người dùng vừa được xóa bỏ',
    ],
    'title' => [
        'system' => 'Quản lý hệ thống',
        'member' => 'Quản lý người dùng',
        'new-member' => 'Tạo người dùng',
        'edit-member' => 'Cập nhập người dùng'
    ],
    'breadcrumb' => [
        'member' => 'Quản lý người dùng',
    ],
    'sidebar' => [
       'title' => 'Quản lý người dùng',
       'list' => 'Người dùng',
    ],
    'form' => [
        'first-name' => 'Họ và Tên',
        'email' => 'Email',
        'status' => 'Trạng Thái',
        'password' => 'Mật khẩu',
        'password-confirmation' => 'Nhập lại mật khẩu',
        'phone' => 'Số điện thoại',
        'new password' => 'New password',
        'new password confirmation' => 'New password confirmation',
        'is activated' => 'Activated',
        'website' => 'Website',
        'position' => 'Vị trí',
        'role' => 'Roles',
        'agency' => 'Đại lý',
        'avarta' => 'Gravatar email',
        'description' => 'Tiểu sử',
        'chosen role' => 'Chọn quyền',
    ],
    'table' => [
        'name' => 'Họ và tên',
		'email' => 'Email',
		'created-at' => 'Ngày tạo',
		'status' => 'Trạng thái',
		'actions' => 'Hành động',
		'active' => 'Hoạt động',
		'deactive' => 'Chặn',
		'empty' => 'Không có bản ghi nào',
    ],
    'navigation' => [
        
    ],
];
