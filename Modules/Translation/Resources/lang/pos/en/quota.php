<?php

return [
    'button' => [
        'create' => 'Tạo mới hạn mức',
        'edit' => 'Sửa',
        'delete' => 'Xóa',
    ],
    'messages' => [
		'valid required' => 'Bạn cần phải nhập thông tin cho trường này',
		'valid code unique' => 'Đã tồn tại mã của máy POS này!',
		'valid name unique' => 'Đã tồn tại tên của máy POS này!',
        'created success' => 'Tạo mới hạn mức thành công!',
        'updated success' => 'Cập nhật hạn mức thành công!',
        'valid amount min' => 'Hạn mức phải lớn hơn hoặc bằng không',
        'valid pos_id unique' => 'Máy POS này đã có hạn mức',
        'store fail' => 'Có lỗi xảy ra trong quá trình lưu hạn mức',
        'store success' => 'Lưu hạn mức thành công',
    ],
    'title' => [
        'list' => 'Hạn mức máy',
		'create quota' => 'Tạo hạn mức',	
        'edit quota' => 'Sửa hạn mức',
        'view quota' => 'Thông tin chi tiết hạn mức',
    ],
    'breadcrumb' => [
        
    ],
    'sidebar' => [
       
    ],
    'form' => [
        'name' => 'Tên máy POS',
        'amount' => 'Hạn mức',
        'pos name' => 'Máy POS',
        'chosen pos name' => 'Chọn máy',
		'update_balance' => 'Cập nhật hạn mức cho máy',
    ],
    'table' => [
        'name' => 'Tên máy POS',
        'code' => 'Mã',
        'contact_name' => 'Người liên lạc',
        'phone' => 'Số điện thoại',
		'type_id' => 'Kiểu máy POS',
        'status' => 'Trạng thái',
        'address' => 'Địa chỉ',
        'spend' => 'Đã sử dụng (vnđ)',
        'balance' => 'Còn lại (vnđ)',
        'amount' => 'Hạn mức trong ngày',
        'updated_at' => 'Thời gian cập nhật',
    ],
    'navigation' => [
        
    ],
];
