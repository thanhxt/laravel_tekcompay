<?php

return [
    'button' => [
        'create' => 'Tạo mới máy POS',
        'edit' => 'Sửa',
        'delete' => 'Xóa',
    ],
    'messages' => [
		'valid required' => 'Bạn cần phải nhập thông tin cho trường này',
		'valid code unique' => 'Đã tồn tại mã của máy POS này!',
		'valid name unique' => 'Đã tồn tại tên của máy POS này!',
        'updated success' => 'Cập nhật máy POS thành công!',
        'delete success' => 'Xóa máy POS thành công!',
    ],
    'title' => [
        'list' => 'Quản lý máy POS',
        'edit pos' => 'Sửa máy POS',
        'create pos' => 'Tạo mới máy POS',
        'view pos' => 'Thông tin chi tiết máy POS',
    ],
    'breadcrumb' => [
        
    ],
    'sidebar' => [
       
    ],
    'form' => [
        'name' => 'Tên máy POS',
        'code' => 'Mã',
        'contact_name' => 'Người liên lạc',
        'phone' => 'Số điện thoại',
        'type_id' => 'Kiểu máy POS',
        'status' => 'Trạng thái',
        'address' => 'Địa chỉ',
        'branch_name' => 'Tên chi nhánh',
        'branch_address' => 'Địa chỉ chi nhánh',
        'from' => 'Từ',
        'to' => 'Đến',
    ],
    'table' => [
        'name' => 'Tên máy POS',
        'code' => 'Mã',
        'contact_name' => 'Người liên lạc',
        'phone' => 'Số điện thoại',
		'type_id' => 'Kiểu máy POS',
        'status' => 'Trạng thái',
        'address' => 'Địa chỉ',
        'spend' => 'Đã sử dụng (vnđ)',
        'balance' => 'Còn lại (vnđ)',
        'created_at' => 'Ngày tạo',
        'updated_at' => 'Ngày cập nhật',
        'create_by' => 'Người tạo',
        'edit_by' => 'Người cập nhật',
    ],
    'navigation' => [
        
    ],
];
