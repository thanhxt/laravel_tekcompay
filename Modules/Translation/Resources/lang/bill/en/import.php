<?php

return [
    'button' => [
    ],
    'messages' => [
		'import success' => 'Đã import hóa đơn thành công',
		'import fail' => 'Có lỗi trong quá trình import dữ liệu',
		'import file valid' => 'Định dạng file không hợp lệ'
    ],
    'title' => [
        'import' => 'Quản lý hóa đơn đầu vào',
        'list' => 'Tải hóa đơn lên',
        'new-import-file' => 'Upload file mới',
        'import file demo' => 'File mẫu',
        'import download' => 'Tải về',
    ],
    'breadcrumb' => [
    ],
    'form' => [
        'status' => 'Trạng thái',
        'agency_id' => 'Đại lý',
        'upload-file' => 'Chọn file',
        'bill_type' => 'Loại hóa đơn',
        'agency-id' => 'Đại lý',
        'description' => 'Mô tả',
    ],
    'table' => [
        'file_name' => 'Tên file',
		'file_name_old' => 'Tên file cũ',
		'path' => 'Đường dẫn file',
		'status' => 'Trạng thái',
		'created-at' => 'Ngày tạo',
		'agency_name' => 'Đại lý',
    ],
    'navigation' => [
        
    ],
];
