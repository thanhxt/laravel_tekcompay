<?php

return [
    'button' => [
        'create' => 'Tạo mới danh sách cà thẻ',
        'save bill' => 'Lưu Khách Hàng',
    ],
    'messages' => [
		'valid required' => 'Bạn cần phải nhập thông tin cho trường này',
		'valid name unique' => 'Danh sách cà thẻ đã tồn tại',
		'valid agency required' => 'Bạn cần phải chọn đại lý',
        'store success' => 'Lưu danh sách cà thẻ thành công',
        'store fail' => 'Có lỗi xảy ra trong quá trình lưu danh sách',
        'delete success' => 'Xóa danh sách cà thẻ thành công',
        'delete fail' => 'Có lỗi khi xóa danh sách cà thẻ',
        'createBill success' => 'Tạo hóa đơn thành công',
        'valid total' => 'Không có hóa đơn hợp với số tiền bạn muốn cà',
        'deleteBatch success' => 'Xóa hóa đơn thành công',
        'deleteBatch fail' => 'Có lỗi khi xóa dữ liệu',
    ],
    'title' => [
        'create bill' => 'Cập nhật danh sách',
        'list' => 'Danh sách cà thẻ',
        'create' => 'Tạo danh sách cà thẻ',
        'info' => '1. Thông tin chung',
        'customer info' => '2. Thông tin khách hàng',
        'bill list' => '3. Danh sách Khách Hàng',
    ],
    'breadcrumb' => [
        
    ],
    'sidebar' => [
       
    ],
    'form' => [
		'agency' => 'Đại lý',
		'name' => 'Tên danh sách',
		'description' => 'Ghi chú',
		'total' => 'Tổng danh sách',
		'cust_name' => 'Tên KH',
		'cust_phone' => 'Số điện thoại',
		'identify_card' => 'CMND / CCCD',
		'card type' => 'Loại thẻ',
		'bank name' => 'Ngân Hàng',
		'chosen' => 'Chọn',
		'card_number' => 'Số thẻ',
		'amount' => 'Số tiền cần cà',
    ],
    'print' => [

    ],
    'table' => [
		'name' => 'Tên danh sách',
		'description' => 'Ghi chú',
		'create_at' => 'Ngày tạo',
		'batch amount' => 'Bill Chi Tiết',
		'batch total' => 'Tổng Bill',
		'fee' => 'Số phí',
		'customer info' => 'Thông tin KH',
    ],
    'navigation' => [
        
    ],
];
