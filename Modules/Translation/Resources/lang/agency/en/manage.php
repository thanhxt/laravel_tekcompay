<?php

return [
    'button' => [
        'agency create' => 'Tạo mới đại lý',
        'edit' => 'Sửa',
        'delete' => 'Xóa',
    ],
    'messages' => [
		'valid required' => 'Bạn cần phải nhập thông tin cho trường này',
		'valid code unique' => 'Đã tồn tại mã của đại lý này!',
		'valid name unique' => 'Đã tồn tại tên của đại lý này!',
        'updated success' => 'Cập nhật đại lý thành công!',
        'delete success' => 'Xóa đại lý thành công!',
    ],
    'title' => [
        'list' => 'Quản lý đại lý',
        'edit agency' => 'Sửa đại lý',
        'create agency' => 'Tạo mới đại lý',
        'view agency' => 'Thông tin chi tiết đại lý',
    ],
    'breadcrumb' => [
        
    ],
    'sidebar' => [
       
    ],
    'form' => [
        'name' => 'Tên đại lý',
        'code' => 'Mã',
        'contact_name' => 'Người liên lạc',
        'phone' => 'Số điện thoại',
        'type_id' => 'Kiểu đại lý',
        'visible' => 'Trạng thái',
        'address' => 'Địa chỉ',
		'chosen' => 'Chọn đại lý',
    ],
    'table' => [
        'name' => 'Tên đại lý',
        'code' => 'Mã',
        'contact_name' => 'Người liên lạc',
        'phone' => 'Số điện thoại',
		'type_id' => 'Kiểu đại lý',
        'visible' => 'Trạng thái',
        'address' => 'Địa chỉ',
        'chosen' => 'Chọn đại lý',
    ],
    'navigation' => [
        
    ],
];
