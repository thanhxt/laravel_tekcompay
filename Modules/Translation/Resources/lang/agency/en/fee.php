<?php

return [
    'button' => [
        'fee create' => 'Tạo mới phí đại lý',
        'edit' => 'Sửa',
        'delete' => 'Xóa',
    ],
    'messages' => [
		'valid required' => 'Bạn cần phải nhập thông tin cho trường này',
		'valid fee_name unique' => 'Đã tồn tại tên của chi phí này!',
        'created success' => 'Thêm mới phí thành công!',
        'updated success' => 'Cập nhật phí thành công!',
        'delete success' => 'Xóa đại phí thành công!',
    ],
    'title' => [
        'list' => 'Quản lý phí đại lý',
        'edit fee' => 'Sửa phí',
        'create fee' => 'Tạo mới phí',
        'view fee' => 'Chi tiết phí đại lý',
    ],
    'breadcrumb' => [
        
    ],
    'sidebar' => [
       
    ],
    'form' => [
        'agency_id' => 'Tên đại lý',
        'fee' => 'Phí',
        'fee_name' => 'Tên của chi phí',
        'date_effect' => 'Ngày tạo',
        'status' => 'Trạng thái',
    ],
    'table' => [
        'agency_id' => 'Tên đại lý',
        'fee' => 'Phí (%)',
        'fee_name' => 'Tên của chi phí',
        'date_effect' => 'Ngày tạo',
        'status' => 'Trạng thái',
    ],
    'navigation' => [
        
    ],
];
