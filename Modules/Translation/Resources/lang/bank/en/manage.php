<?php

return [
    'button' => [
        'bank create' => 'Tạo mới ngân hàng',
        'edit' => 'Sửa',
        'delete' => 'Xóa',
        'search' => 'Tìm kiếm',
    ],
    'messages' => [
		'valid required' => 'Bạn cần phải nhập thông tin cho trường này',
		'valid code unique' => 'Đã tồn tại mã của ngân hàng này!',
		'valid name unique' => 'Đã tồn tại tên của ngân hàng này!',
        'updated success' => 'Cập nhật ngân hàng thành công!',
        'delete success' => 'Xóa ngân hàng thành công!',
    ],
    'title' => [
        'list' => 'Quản lý ngân hàng',
        'edit bank' => 'Sửa ngân hàng',
        'create bank' => 'Tạo mới ngân hàng',
        'view bank' => 'Thông tin chi tiết ngân hàng',
    ],
    'breadcrumb' => [
        
    ],
    'sidebar' => [
       
    ],
    'form' => [
        'name' => 'Tên ngân hàng',
        'code' => 'Mã',
        'contact_name' => 'Người liên lạc',
        'phone' => 'Số điện thoại',
        'type_id' => 'Kiểu ngân hàng',
        'status' => 'Trạng thái',
        'address' => 'Địa chỉ',
    ],
    'table' => [
        'name' => 'Tên ngân hàng',
        'code' => 'Mã',
        'contact_name' => 'Người liên lạc',
        'phone' => 'Số điện thoại',
		'type_id' => 'Kiểu ngân hàng',
        'status' => 'Trạng thái',
        'address' => 'Địa chỉ',
        'created_at' => 'Ngày tạo',
        'updated_at' => 'Ngày cập nhật',
        'create_by' => 'Người tạo',
        'edit_by' => 'Người cập nhật',
    ],
    'navigation' => [
        
    ],
];
