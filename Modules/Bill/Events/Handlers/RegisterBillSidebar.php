<?php

namespace Modules\Bill\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterBillSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }
    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('member::members.title.system'), function (Group $group) {
            $group->weight(801);
            $group->item(trans('bill::batch.title.batch-list'), function (Item $item) {
                $item->weight(1);
                $item->icon('fa fa-list-ol');
                $item->authorize(
                    $this->auth->hasAccess('bill.batch.all')
                );

                $item->item(trans('bill::batch-list.title.list'), function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-list-alt');
                    $item->route('admin.bill.batch-list.index');
                    $item->authorize(
                        $this->auth->hasAccess('bill.batch.all')
                    );
                });
            });
        });

        $menu->group(trans('member::members.title.system'), function (Group $group) {
            $group->weight(908);
            $group->item(trans('bill::import.title.import'), function (Item $item) {
                $item->weight(5);
                $item->icon('fa fa-file-excel-o');
                $item->authorize(
                    $this->auth->hasAccess('bill.import')
                );

                $item->item(trans('bill::import.title.list'), function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-folder-open');
                    $item->route('admin.bill.import.index');
                    $item->authorize(
                        $this->auth->hasAccess('bill.import')
                    );
                });

                $item->item(trans('bill::bill.title.list'), function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-file-pdf-o');
                    $item->route('admin.bill.bill.index');
                    $item->authorize(
                        $this->auth->hasAccess('bill.import')
                    );
                });
            });
        });

        $menu->group(trans('member::members.title.system'), function (Group $group) {
            $group->weight(908);
            $group->item(trans('bill::batch.title.manage'), function (Item $item) {
                $item->weight(4);
                $item->icon('fa fa-tags');
                $item->authorize(
                    $this->auth->hasAccess('bill.batch.all')
                );

                $item->item(trans('bank::manage.title.list'), function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-bank');
                    $item->route('admin.bank.manage.index');
                    $item->authorize(
                        $this->auth->hasAccess('bank.manage.all')
                    );
                });

                $item->item(trans('bill::batch.title.list'), function (Item $item) {
                    $item->weight(1);
                    $item->icon('fa fa-money');
                    $item->route('admin.bill.batch.index');
                    $item->authorize(
                        $this->auth->hasAccess('bill.batch.all')
                    );
                });

            });
        });

        return $menu;
    }
}
