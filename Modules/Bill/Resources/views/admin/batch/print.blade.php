@extends('layouts.master-bill-print')

@section('content')
@inject('helpBill','Modules\Bill\Utility\Help')
	<?php 
		$created_at = date("d-m-Y h:m", $model->created_at);
		$bill_total_amount = round($model->base_amount, 0);
		$base_amount = $helpBill->formatMoney($bill_total_amount, ",");
		$base_amount_txt = $helpBill->convert_number_to_words($bill_total_amount);
		$card_number = $helpBill->formatCardNumber($model->card_number);
		$agency_info = $helpBill->getAgencyInformationById($model->agency_id);			
	?>
	<div class="header">
		<p class="title">{{ trans('bill::batch.print.bill text') }}</p>
		<p class="sub-title">{{ trans('bill::batch.print.bill number') }}: {{ $model->id}} - {{ trans('bill::batch.print.bill day') }}: {{ $created_at }}</p>
	</div>
	<div class="address">
		<p><b>{{ $agency_info->name }}</b></p>
		<p><b>{{ trans('bill::batch.print.agency address') }}:</b> </p>
	</div>
	<div class="customer">
		<table class="table table-striped">
			<tbody>
			<tr>
				<td style="width: 130px;"><b>{{ trans('bill::batch.print.customer name') }}:</b></td>
				<td>{{ $model->cust_name}}</td>
			</tr>
			<tr>
				<td><b>{{ trans('bill::batch.print.customer phone') }}:</b></td>
				<td>{{ $model->cust_phone}}</td>
			</tr>
			<tr>
				<td><b>{{ trans('bill::batch.print.payment method') }}:</b></td>
				<td>{{ trans('bill::batch.print.payment method type') }}</td>
			</tr>
			<tr>
				<td><b>{{ trans('bill::batch.print.customer card number') }}:</b></td>
				<td>{{ $card_number }}</td>
			</tr>
			</tbody>
		</table>
	</div>
	<div class="detail">
		<table style="width:100%">
			<tbody>
				<tr>
					<td colspan="5" style="text-align: center;"><b>{{ trans('bill::batch.print.payment details') }}</b></td>
				</tr>
				<tr>
					<td style="width: 138px;">{{ trans('bill::batch.form.no') }}</td>
					<td>{{ trans('bill::batch.form.customer_code') }}</td>
					<td>{{ trans('bill::batch.form.customer_name') }}</td>
					<td>{{ trans('bill::batch.form.session') }}</td>
					<td>{{ trans('bill::batch.form.money amount') }}</td>
				</tr>
				<?php 
					$bills = $model->getAchieveBill();
				?>
				<?php $i=1;if(count($bills)): ?>
					<?php foreach($bills as $bill):?>
					<?php 
						$pay_month = date("m", $bill->pay_date);
						$amount = $helpBill->formatMoney($bill->amount, ",");
					?>
					<tr>
						<td>{{ $i }}</td>
						<td>{{ $bill->cust_number }}</td>
						<td style="text-transform: uppercase;">{{ strtoupper($bill->cust_name) }}</td>
						<td>{{ trans('bill::batch.form.month') }} {{ $pay_month }}</td>
						<td>{{ $amount }} {{ trans('member::members.system.common.currency txt VND') }}</td>
					</tr>
					
					<?php $i++;endforeach; ?>
				<?php endif; ?>	
				<tr>
					<td colspan="3" style="font-weight: bold;">{{ trans('bill::batch.print.total') }}:</td>
					<td colspan="2" style="font-weight: bold;">
					{{ $base_amount }}                    
					</td>
				</tr>
				<tr>
					<td style="font-weight: bold;">{{ trans('bill::batch.print.total money txt') }}:</td>
					<td colspan="4" style="font-style: italic;">({{ ucfirst($base_amount_txt) }} {{ trans('bill::batch.print.money txt') }})</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="signature">
		<table style="width: 100%;">
			<tbody>
			<tr>
				<td>{{ trans('bill::batch.print.create bill') }}</td>
				<td>{{ trans('bill::batch.print.customer') }}</td>
			</tr>
			</tbody>
		</table>
	</div>
@stop