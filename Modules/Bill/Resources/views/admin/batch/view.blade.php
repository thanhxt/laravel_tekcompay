@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('bill::batch.title.view batch') }}{{ $model->id}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ URL::route('admin.bill.batch.index') }}">{{ trans('bill::batch.title.list') }}</a></li>
        <li class="active">{{ trans('bill::batch.title.view batch') }}</li>
    </ol>
@stop

@section('content')
@inject('helpBill','Modules\Bill\Utility\Help')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-danger">
				<div class="box-header">
					<a class="btn btn-info" href="{{ route('admin.bill.batch.print', ['id' => $model->id]) }}"><i class="fa fa-pencil"></i> {{ trans('bill::batch.button.print') }}</a>
					<a class="btn btn-success" href="{{ route('admin.bill.batch.create', ['id' => $model->id]) }}"><i class="fa fa-pencil"></i> {{ trans('bill::batch.button.create') }}</a>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-xs-12">
							<h2 class="page-header">
								{{ trans('bill::batch.form.bill number') }}: {{ $model->id}}  
								<?php 
									$created_at = date("d-m-Y h:m", $model->created_at);
									$base_amount = $helpBill->formatMoney($model->base_amount);
									$card_number = $helpBill->formatCardNumber($model->card_number);
								?>
								<small class="pull-right">{{ trans('bill::batch.form.bill day') }}: {{ $created_at }}</small>
							</h2>
						</div>
					</div>
					<!-- -->
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							<b>{{ trans('bill::batch.form.invoice') }}</b><br>
							<br>
							<address>
								<b>{{ trans('bill::batch.form.money amount') }}:</b>
								{{ $base_amount }} {{ trans('member::members.system.common.currency txt VND') }}        
								<br>
							</address>
						</div>
						<div class="col-sm-4 invoice-col">
							<strong>{{ trans('bill::batch.form.customer txt') }}</strong>
							<br><br>
							<address>
								<b>{{ trans('bill::batch.form.cust_name') }}:</b> {{ $model->cust_name}}<br>
								<b>{{ trans('bill::batch.form.cust_phone') }}:</b> {{ $model->cust_phone}}<br>
								<b>{{ trans('bill::batch.form.payment method') }}:</b> {{ trans('bill::batch.form.payment method type') }}<br>
								<b>{{ trans('bill::batch.form.card_number') }}:</b> {{ $card_number}} </address>
						</div>
						<div class="col-sm-4 invoice-col">
							<b>{{ trans('bill::batch.form.branch') }}</b><br>
							<br>
							<?php 
								$agency_info = $helpBill->getAgencyInformationById($model->agency_id);
							?>
							
							<address>
								<b>{{ $agency_info->name }}</b><br>
								<b>{{ trans('bill::batch.form.address') }}: {{ $agency_info->address }}</b>             
							</address>
						</div>
					</div>
					<!-- invoice-info -->
					<?php 
						$bills = $model->getAchieveBill();
					?>
					
					<div class="row">
						<div class="col-xs-12 table-responsive">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>{{ trans('bill::batch.form.no') }}</th>
										<th>{{ trans('bill::batch.form.customer_code') }}</th>
										<th>{{ trans('bill::batch.form.customer_name') }}</th>
										<th>{{ trans('bill::batch.form.session') }}</th>
										<th>{{ trans('bill::batch.form.money amount') }}</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1;if(count($bills)): ?>
										<?php foreach($bills as $bill):?>
										<?php 
											$pay_month = date("m", $model->pay_date);
											$amount = $helpBill->formatMoney($bill->amount);
										?>
										<tr>
											<td>{{ $i }}</td>
											<td>{{ $bill->cust_number }}</td>
											<td>{{ $bill->cust_name }}</td>
											<td>{{ trans('bill::batch.form.month') }} {{ $pay_month }}</td>
											<td>{{ $amount }} {{ trans('member::members.system.common.currency txt VND') }}</td>
										</tr>
										
										<?php $i++;endforeach; ?>
									<?php endif; ?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
        </div>
    </div>
	@include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('page::pages.navigation.back to index') }}</dd>
    </dl>
@stop

@section('scripts')
    <script type="text/javascript">
        $(function() {
           /* CKEDITOR.replaceAll(function( textarea, config ) {
                if (!$(textarea).hasClass('ckeditor')) {
                    return false;
                }
                config.language = '<?php echo App::getLocale() ?>';
            } );*/
        });
    </script>
    <script>
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.page.page.index') ?>" }
                ]
            });
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

            $('input[type="checkbox"]').on('ifChecked', function(){
                $(this).parent().find('input[type=hidden]').remove();
            });

            $('input[type="checkbox"]').on('ifUnchecked', function(){
                var name = $(this).attr('name'),
                    input = '<input type="hidden" name="' + name + '" value="0" />';
                $(this).parent().append(input);
            });
        });
    </script>
@stop
