@extends('layouts.master')

@section('styles')
	{!! Theme::style('vendor/angular/css/angular-material.min.css') !!}
	{!! Theme::style('css/upload-style.css') !!}
@stop


@section('content-header')
	<?php 
		$translate_path = 'bill::batch.title.create batch';
	?>
    <h1>
        {{ trans($translate_path) }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ URL::route('admin.bill.batch.index') }}">{{ trans('bill::batch.title.list') }}</a></li>
        <li class="active">{{ trans($translate_path) }}</li>
    </ol>
@stop

@section('content')
	@inject('helpBill','Modules\Bill\Utility\Help')
	<div ng-app="Bill" ng-controller="BillCtrl" class="ng-scope">
		{!! Form::open(['route' => ['admin.bill.bill.store'], 'method' => 'post', 'id' => 'bill_form']) !!}
		<div class="row">
			<div class="col-md-4">
				@include('bill::admin.partials.batch.agency.edit.search')
			</div>
			<!-- col-md-->
			<div class="col-md-8">
				@include('bill::admin.partials.batch.agency.edit.customer')
			</div>
		</div>
		<div class='row'>
			<div class="col-md-12">
				<div class='box box-danger'>
					<div class='box-header'>
						<h3 class="box-title">{{ trans('bill::batch.title.bill list') }}</h3>
					</div>
					<div class='box-body'>
						@include('bill::admin.partials.batch.agency.edit.table')
					</div>
				</div>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('page::pages.navigation.back to index') }}</dd>
    </dl>
@stop

@section('scripts')
	{!! Theme::script('vendor/angular/js/angular.min.js') !!}
	{!! Theme::script('vendor/angular/js/angular-animate.min.js') !!}
	{!! Theme::script('vendor/angular/js/angular-aria.min.js') !!}
	{!! Theme::script('vendor/angular/js/angular-messages.min.js') !!}
	{!! Theme::script('vendor/angular/js/angular-material.min.js') !!}
	{!! Theme::script('vendor/angular/js/angular-material-datetimepicker.min.js') !!}
	
	{!! Theme::script('vendor/bootstrap-notify/js/bootstrap-notify.min.js') !!}
	
	{!! Theme::script('vendor/inputmask/js/jquery.inputmask.js') !!}
	{!! Theme::script('js/bill/agency/edit.js') !!}
    <script>
        $( document ).ready(function() {
            $(".money-format").inputmask({"alias":"numeric","groupSeparator":",","autoGroup":true,"removeMaskOnSubmit":true});
            $("#card_number").inputmask({"mask":"9999-XXXX-XXXX-9999","alias":"text","removeMaskOnSubmit":true});
        });
    </script>
@stop
