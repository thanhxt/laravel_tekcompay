@extends('layouts.master')

@section('content-header')
	<?php 
		$translate_path = 'bill::batch-list.title.create';
	?>
    <h1>
        {{ trans($translate_path) }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ URL::route('admin.bill.batch-list.index') }}">{{ trans('bill::batch-list.title.list') }}</a></li>
        <li class="active">{{ trans($translate_path) }}</li>
    </ol>
@stop

@section('content')
@inject('helpAgency','Modules\Agency\Utility\Help')
    {!! Form::open(['route' => ['admin.bill.batch-list.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <div class="tab-content">
					<div class="tab-pane active" id="tab_1-1">
						<div class="box-body">
							{!! Form::hidden('id', $model->id, []) !!}
							<?php if($current_agency_id>0): ?>
								{!! Form::hidden('agency_id', $current_agency_id, []) !!}
							<?php else: ?>
								<div class="row">
									<?php 
										$agency_arr = $helpAgency->getAgencyArr();
									?>
									<div class="col-sm-6">
										<div class="form-group">
											{!! Form::label('agency_id', trans('bill::batch-list.form.agency')) !!}: 
											<?php if($model->agency_id): ?>
												{{ $agency_arr[$model->agency_id] }}
												{!! Form::hidden('agency_id', $model->agency_id, []) !!}
											<?php else: ?>
												{!! Form::select("agency_id", $agency_arr, null, ['class' => "form-control", 'placeholder' => trans('agency::manage.form.chosen'),'required'=>'required']) !!}
											<?php endif; ?>
											{!! $errors->first('agency_id', '<span class="help-block">:message</span>') !!}
										</div>
									</div>
									
								</div>
							<?php endif; ?>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
										{!! Form::label('name', trans('bill::batch-list.form.name')) !!}
										{!! Form::text("name", $model->name, ['class' => "form-control", 'placeholder' => trans('bill::batch-list.form.name'), "id"=>"name",'required'=>'required']) !!}
										{!! $errors->first('name', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
										{!! Form::label('description', trans('bill::batch-list.form.description')) !!}
										{!! Form::textarea('description', $model->description, ['class' => 'form-control', 'placeholder' => trans('bill::batch-list.form.description')]) !!}
										{!! $errors->first('description', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="box-footer">
						<?php 
							$btn_txt = 'core::core.button.save';
						?>
                        <button type="submit" class="btn btn-primary btn-success">{{ trans($btn_txt) }}</button>
                        <button class="btn btn-default btn-flat" name="button" type="reset">{{ trans('core::core.button.reset') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ URL::route('admin.bill.batch-list.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>

    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
