@extends('layouts.master')

@section('styles')
	{!! Theme::style('vendor/datepicker/css/bootstrap-datepicker.css') !!}
@stop

@section('content-header')
<h1>
    {{ trans('bill::batch-list.title.list') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class="active">{{ trans('bill::batch-list.title.list') }}</li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-body">
				@include('bill::admin.partials.batch-list.search')
			</div>
		</div>
		<div class="box box-danger">
			<div class="box-body">
				@include('bill::admin.partials.batch-list.table')
			</div>
		</div>
	</div>
</div>

@include('core::partials.delete-modal')
@stop

@section('scripts')
	{!! Theme::script('vendor/datepicker/js/bootstrap-datepicker.js') !!}
    <script>
        $( document ).ready(function() {
			$.fn.datepicker.defaults.format = "dd-mm-yyyy";
			$('.created-at').datepicker({});
        });
    </script>
@stop