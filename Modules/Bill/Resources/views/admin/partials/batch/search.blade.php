@inject('helpBill','Modules\Bill\Utility\Help')
@inject('helpAgency','Modules\Agency\Utility\Help')
<div class="row">
	<div class="col-md-12">
		<div class="nav-tabs-custom">
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1-1">
					{!! Form::open(['route' => ['admin.bill.batch.index'], 'method' => 'get', 'id' => 'search_form']) !!}
						<div class="row">
							<?php if($current_agency_id>0): ?>
								{!! Form::hidden('agency_id', $current_agency_id, []) !!}
							<?php else: ?>
								<?php 
									$agency_arr = $helpAgency->getAgencyArr();
								?>
								<div class="col-sm-3">
									<div class="form-group">
										{!! Form::label('agency_id', trans('bill::batch-list.form.agency')) !!}: 
										{!! Form::select("agency_id", $agency_arr, null, ['class' => "form-control", 'placeholder' => trans('agency::manage.form.chosen'),'required'=>'required']) !!}
										{!! $errors->first('agency_id', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							<?php endif; ?>
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('created_at', trans('bill::batch.form.created_at')) !!}
									{!! Form::text("created_at",old("created_at"), ['class' => "form-control created-at", 'placeholder' => trans('bill::batch.form.created_at')]) !!}
								</div>
							</div>
							<?php 
								$pos_arr = $helpBill->getPosByAgencyId();
								$bank_arr = $helpBill->getBankArr();
							?>
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('pos_id', trans('pos::quota.form.pos name')) !!}
									{!! Form::select("pos_id", $pos_arr, null, ['class' => "form-control", 'placeholder' => trans('pos::quota.form.chosen pos name')]) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('bank_id', trans('bill::batch.form.bank name')) !!}
									{!! Form::select("bank_id", $bank_arr, null, ['class' => "form-control", 'placeholder' => trans('bill::batch.form.chosen bank')]) !!}
								</div>
							</div>
						</div>
						<div class="row">	
							<div class="col-sm-6">
								<div class="form-group search-control">
									<button type="submit" class="btn btn-primary btn-danger">{{ trans('member::members.button.search') }}</button>
									<label>&nbsp;</label>
									<a href="{{ URL::route('admin.bill.batch.create') }}" class="btn btn-primary btn-success" style="padding: 4px 10px;">
										<i class="fa fa-pencil"></i> {{ trans('bill::batch.button.create') }}
									</a>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>	
			</div>	
		</div>	
	</div>	
</div>	

			
