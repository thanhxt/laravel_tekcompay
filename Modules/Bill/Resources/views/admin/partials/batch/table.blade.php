@inject('helpBill','Modules\Bill\Utility\Help')
<table class="data-table table table-bordered table-hover">
	<thead>
		<tr>
			<th>#</th>
			<th>{{ trans('bill::batch.table.customer info') }}</th>
			<th>{{ trans('bill::batch.table.bank name') }}</th>
			<th>{{ trans('bill::batch.table.pos name') }}</th>
			<th>{{ trans('bill::batch.table.fee info') }}</th>
			<th>{{ trans('bill::batch.table.base amount') }}</th>
			<th>{{ trans('bill::batch.table.amount') }}</th>
			<th>{{ trans('bill::batch.table.staff info') }}</th>
			<th data-sortable="false">{{ trans('member::members.table.actions') }}</th>
		</tr>
	</thead>
	<tbody>
	<?php if (count($batches)): ?>
		<?php foreach ($batches as $item): ?>
		<tr>
			<td>
				{{ $item->id }}
			</td>
			<td>
				<?php 
					$customer_name = $item->cust_name;
					$customer_phone = $item->cust_phone;
					$customer_card_number = $item->card_number;
				?>
				- {{ trans('bill::batch.table.item.customer') }}: <b class="text-red">{{ $customer_name }}</b><br>- {{ trans('bill::batch.table.item.customer phone') }}: {{ $customer_phone }}<br>- {{ trans('bill::batch.table.item.customer card number') }}: {{ $customer_card_number }}
			</td>
			<td>
				{{ $item->bank_name }}
			</td>
			<td>
				<?php 
					$pos_info = $helpBill->getListPosByBatchId($item->id);
				?>
				<?php if(count($pos_info)):?>
					<?php foreach($pos_info as $pos): ?>
						
						-{{ trans('bill::batch.table.item.pos') }}: {{ $pos->pos_name }}<br>
						-{{ trans('bill::batch.table.item.batch') }}: {{ $pos->transaction_batch }}<br />
					<?php endforeach; ?>
				<?php endif; ?>
				
			</td>
			<td>
				<?php 
					$fee = $item->fee;
					$fee_amount = $helpBill->formatMoney($item->fee_amount);
				?>
				- {{ trans('bill::batch.table.item.fee') }}: <b class="text-red">{{ $fee }}% </b><br>- {{ trans('bill::batch.table.item.amount') }}: <b class="text-red">{{ $fee_amount }} {{ trans('member::members.system.common.currency txt VND') }} </b>
			</td>
			<td>
				<?php 
					$amount = $helpBill->formatMoney($item->amount);
					$base_amount = $helpBill->formatMoney($item->base_amount);
				?>
				{{ $amount }} {{ trans('member::members.system.common.currency txt VND') }}
			</td>
			<td>
				{{ $base_amount }} {{ trans('member::members.system.common.currency txt VND') }}
			</td>
			<td>
				<?php 
					$staff_name = $item->staff_name;
					$created_at = $helpBill->formatDateForDisplay($item->created_at, 'd/m/Y H:m:s');
				?>
				- {{ trans('bill::batch.table.item.staff') }}:<b class="text-red">{{ $staff_name }}</b><br>- {{ trans('bill::batch.table.item.created_at') }}: {{ $created_at }}
			</td>
			<td>
				<div class="btn-group">
					<a href="{{ route('admin.bill.batch.view', [$item->id]) }}" class="btn btn-default btn-flat" title="{{ trans('member::members.icon.caption view') }}"><i class="fa fa-eye"></i></a>
					<a href="{{ route('admin.bill.batch.edit', [$item->id]) }}" class="btn btn-default btn-flat" title="{{ trans('member::members.icon.caption edit') }}"><i class="fa fa-pencil"></i></a>
					<button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.bill.batch.delete', [$item->id]) }}" title="{{ trans('member::members.icon.caption delete') }}"><i class="fa fa-trash"></i></button>
				</div>
			</td>
		</tr>
		<?php endforeach; ?>
	<?php else: ?>
		<tr>
			<td colspan='9'>{{ trans('member::members.table.empty') }}</td>
		</tr>	
	<?php endif; ?>
	</tbody>
</table>
<div class='text-center'>
	<?php
		$data = request()->all();
		unset($data["_token"]);
	?>
	<?php  echo $batches->appends($data)->render() ?>
</div>