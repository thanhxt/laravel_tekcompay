<div class='box box-danger' style='min-height: 335px;'>
	<div class='box-header'>
		<h3 class="box-title">{{ trans('bill::batch.title.bill search') }}</h3>
	</div>
	<input type="hidden" id="bill-id" name="batch_id" value="{{ $model->id}}"/>
	<div class='box-body'>
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group{{ $errors->has('find_amount') ? ' has-error' : '' }}">
					{!! Form::label('find_amount', trans('bill::batch.form.find_amount')) !!}
					<div class="input-group">
						{!! Form::text("find_amount",$model->find_amount, ['class' => "form-control money-format", 'placeholder' => trans('bill::batch.form.find_amount'), "id"=>"find_amount",'required'=>'required']) !!}
						<span class="input-group-addon">{{ trans('member::members.system.common.currency txt VND') }}</span>
					</div>
					{!! $errors->first('find_amount', '<span class="help-block">:message</span>') !!}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group{{ $errors->has('base_amount') ? ' has-error' : '' }}">
					{!! Form::label('base_amount', trans('bill::batch.form.base_amount')) !!}
					<div class="input-group">
						<b class="text-red total-invoice-cost" style="font-size:16px;">
							<?php 
								$base_amount = $helpBill->formatMoney($model->base_amount, ",");
							?>
							{{ $base_amount }} {{ trans('member::members.system.common.currency txt VND') }}
						</b>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group{{ $errors->has('fee') ? ' has-error' : '' }}">
					{!! Form::label('fee', trans('bill::batch.form.fee')) !!}
					<div class="input-group">
						{!! Form::text("fee",5, ['class' => "form-control", 'placeholder' => trans('bill::batch.form.fee'), "id"=>"fee", 'ng-model' => 'formData.fee', 'required' => 'true', 'ng-change' => 'calFee()']) !!}
						<span class="input-group-addon">%</span>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				{!! Form::label('fee_amount', trans('bill::batch.form.fee_amount')) !!}
				{!! Form::text('fee_amount', null, ['ng-model'=>'formData.fee_amount', 'style'=>'display: none;']) !!}
				<div class="input-group">
					@verbatim
						<b class="text-red" style="font-size:16px;">{{calFee(formData.fee, formData.base_amount) | number: 0}} VND</b>
					@endverbatim
				</div>
			</div>
		</div>
	</div>
	<!-- box-body -->
</div>
<!-- box box-danger -->