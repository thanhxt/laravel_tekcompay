<div class='box box-warning'>
	<div class='box-header'>
		<h3 class="box-title">{{ trans('bill::batch.title.bill info') }}</h3>
	</div>
	<div class='box-body'>
		<div class="row">
			<div class="col-sm-4">
				<?php 
					$card_arr = $helpBill->getCardTypeArr();
				?>
				<div class="form-group{{ $errors->has('card_type') ? ' has-error' : '' }}">
					{!! Form::label('card_type', trans('bill::batch.form.card type')) !!}
					{!! Form::select("card_type", $card_arr, $model->card_type, ['class' => "form-control", 'placeholder' => trans('bill::batch.form.chosen card') ,'required'=>'required']) !!}
					{!! $errors->first('card_type', '<span class="help-block">:message</span>') !!}
				</div>
			</div>
			<div class="col-sm-4">
				<?php 
					$bank_arr = $helpBill->getBankArr();
				?>
				<div class="form-group{{ $errors->has('bank_id') ? ' has-error' : '' }}">
					{!! Form::label('bank_id', trans('bill::batch.form.bank name')) !!}
					{!! Form::select("bank_id", $bank_arr, $model->bank_id, ['class' => "form-control", 'placeholder' => trans('bill::batch.form.chosen bank') ,'required'=>'required']) !!}
					{!! $errors->first('bank_id', '<span class="help-block">:message</span>') !!}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group{{ $errors->has('card_number') ? ' has-error' : '' }}">
					{!! Form::label('card_number', trans('bill::batch.form.card_number')) !!}
					{!! Form::text("card_number",$model->card_number, ['class' => "form-control", 'placeholder' => trans('bill::batch.form.card_number'), "id"=>"card_number",'required'=>'required']) !!}
					{!! $errors->first('card_number', '<span class="help-block">:message</span>') !!}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group{{ $errors->has('cust_phone') ? ' has-error' : '' }}">
					{!! Form::label('cust_phone', trans('bill::batch.form.cust_phone')) !!}
					{!! Form::text("cust_phone",$model->cust_phone, ['class' => "form-control", 'placeholder' => trans('bill::batch.form.cust_phone'), "id"=>"cust_phone",'required'=>'required']) !!}
					{!! $errors->first('cust_phone', '<span class="help-block">:message</span>') !!}
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
					{!! Form::label('amount', trans('bill::batch.form.amount')) !!}
					<div class="input-group">
						{!! Form::text("amount",round($model->amount, 0), ['class' => "form-control money-format", 'placeholder' => trans('bill::batch.form.amount'), "id"=>"amount",'required'=>'required']) !!}
						<span class="input-group-addon">VND</span>
					</div>
					{!! $errors->first('amount', '<span class="help-block">:message</span>') !!}
				</div>
			</div>
		</div>
		<!-- row -->
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group{{ $errors->has('identify_card') ? ' has-error' : '' }}">
					{!! Form::label('identify_card', trans('bill::batch.form.identify_card')) !!}
					{!! Form::text("identify_card",$model->identify_card, ['class' => "form-control", 'placeholder' => trans('bill::batch.form.identify_card'), "id"=>"identify_card",'required'=>'required']) !!}
					{!! $errors->first('identify_card', '<span class="help-block">:message</span>') !!}
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group{{ $errors->has('cust_name') ? ' has-error' : '' }}">
					{!! Form::label('cust_name', trans('bill::batch.form.cust_name')) !!}
					{!! Form::text("cust_name",$model->cust_name, ['class' => "form-control", 'placeholder' => trans('bill::batch.form.cust_name'), "id"=>"cust_name",'required'=>'required']) !!}
					{!! $errors->first('cust_name', '<span class="help-block">:message</span>') !!}
				</div>
			</div>
		</div>
		<!-- row -->
		<div class="row">
			<div class="form-group col-xs-12">
				<button type="submit" class="btn btn-success" ng-click="submitData()">{{ trans('core::core.button.save') }}</button>                            
			</div>
		</div>
		<!-- row -->
	</div>
</div>