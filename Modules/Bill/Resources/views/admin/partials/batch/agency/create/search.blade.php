@inject('helpAgency','Modules\Agency\Utility\Help')
<div class='box box-danger' style='min-height: 335px;'>
	<div class='box-header'>
		<h3 class="box-title">{{ trans('bill::batch.title.bill search') }}</h3>
	</div>
	{!! Form::text('time_lock', null, ['ng-model'=>'formData.time_lock', 'style'=>'display: none;']) !!}
	<div class='box-body'>
		<?php if($current_agency_id>0): ?>
			{!! Form::text('agency_id', $current_agency_id, ['ng-model'=>'formData.agency_id', 'style'=>'display: none;']) !!}
		<?php else: ?>
			<div class="row">
				<?php 
					$agency_arr = $helpAgency->getAgencyArr();
				?>
				<div class="col-sm-12">
					<div class="form-group">
						{!! Form::label('agency_id', trans('bill::batch-list.form.agency')) !!}: 
						{!! Form::select("agency_id", $agency_arr, null, ['class' => "form-control", 'placeholder' => trans('agency::manage.form.chosen'), 'ng-model'=>'formData.agency_id' ,'required'=>'required', 'ng-change' => 'changeAgency()']) !!}
						{!! $errors->first('agency_id', '<span class="help-block">:message</span>') !!}
					</div>
				</div>
				
			</div>
		<?php endif; ?>
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group{{ $errors->has('find_amount') ? ' has-error' : '' }}">
					{!! Form::label('find_amount', trans('bill::batch.form.find_amount')) !!}
					<div class="input-group">
						{!! Form::text("find_amount",null, ['class' => "form-control money-format", 'placeholder' => trans('bill::batch.form.find_amount'), "id"=>"find_amount",'required'=>'required', 'ng-model' => 'formData.expect_price', 'ng-change' => 'ShowButton()']) !!}
						<span class="input-group-addon">{{ trans('member::members.system.common.currency txt VND') }}</span>
					</div>
					{!! $errors->first('find_amount', '<span class="help-block">:message</span>') !!}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group{{ $errors->has('base_amount') ? ' has-error' : '' }}">
					{!! Form::label('base_amount', trans('bill::batch.form.base_amount')) !!}
					{!! Form::text('base_amount', null, ['ng-model'=>'formData.total', 'style'=>'display: none']) !!}
					
					<div class="input-group">
						@verbatim
							<b class="text-red total-invoice-cost" style="font-size:16px;" ng-model="formData.total">
								{{total(formData.obj_invoice)| number}} VND
							</b>
						@endverbatim
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group{{ $errors->has('fee') ? ' has-error' : '' }}">
					{!! Form::label('fee', trans('bill::batch.form.fee')) !!}
					<div class="input-group">
						{!! Form::text("fee",5, ['class' => "form-control", 'placeholder' => trans('bill::batch.form.fee'), "id"=>"fee", 'ng-model' => 'formData.fee', 'required' => 'true', 'ng-change' => 'calFee()']) !!}
						<span class="input-group-addon">%</span>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				{!! Form::label('fee_amount', trans('bill::batch.form.fee_amount')) !!}
				{!! Form::text('fee_amount', null, ['ng-model'=>'formData.fee_amount', 'style'=>'display: none;']) !!}
				<div class="input-group">
					@verbatim
						<b class="text-red" style="font-size:16px;">{{calFee(formData.fee, formData.total) | number: 0}} VND</b>
					@endverbatim
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<a class="btn btn-danger find-invoice pull-right" ng-click="getInvoice()" ng-hide="IsDisabled" href="javascript:" aria-hidden="true" style='margin: 15px 0 0;'>{{ trans('bill::batch.button.search') }}</a>
			</div>
		</div>
	</div>
	<!-- box-body -->
</div>
<!-- box box-danger -->