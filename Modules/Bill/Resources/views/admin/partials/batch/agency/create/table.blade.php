<div class='row'>
	<div class="col-sm-12">
		<div class="form-group">
			<input type="hidden" name="is_payment_merge" value="0">
			<label>
				<input type="checkbox" id="bill-is_payment_merge" name="is_payment_merge" value="1" ng-model="formData.is_payment_merge" ng-change="paymentMerge()" class="ng-pristine ng-untouched ng-valid ng-not-empty" aria-invalid="false">{!! Form::label('is_payment_merge', trans('bill::batch.form.is_payment_merge')) !!}
			</label>
		</div>
	</div>
	<div class="col-sm-12">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>#</th>
					<th>{{ trans('bill::batch.table.customer name') }}</th>
					<th>{{ trans('bill::batch.table.money amount') }}</th>
					<th>{{ trans('bill::batch.table.pos name') }}</th>
					<th>{{ trans('bill::batch.table.lot number') }}</th>
					<th>{{ trans('bill::batch.table.trading code') }}</th>
					<th>{{ trans('bill::batch.table.mid') }}</th>
				</tr>
			</thead>
			<tbody>
				@verbatim
				<tr ng-repeat="(k, obj) in formData.obj_invoice">
					
					<td class="align-middle">
						<a ng-click="onRemoveBill($index, obj.id)">
							<i class="fa fa-trash"></i>
						</a>
					</td>
					<td class="align-middle"><b class="text-red">{{obj.customer_name}}</b>
						<input type="hidden" name="Invoice[{{$index}}][id]"
							   value="{{obj.id}}"/>
						<br/>
						<b>Ngày:</b> {{obj.pay_date * 1000 | date: "dd-MM-y hh:mm"}}
						<br/>
						<b>Kì Hoá Đơn:</b> {{obj.recurring_invoice}}
					</td>
					<td class="align-middle"><b class="text-red">{{obj.amount | number}} VND</b> <input
								type="hidden"
								name="Invoice[{{$index}}][amount]"
								value="{{obj.amount}}"/></td>
					<td class="align-middle" ng-if="formData.is_payment_merge && $first"
						rowspan="{{formData.obj_invoice.length}}">
						<md-input-container>
							<label>Chọn máy </label>
							<md-select data-md-container-class="selectdemoSelectHeader-{{$index}}"
									   class="machine-{{$index}}" name="Invoice[{{$index}}][machine_id]"
									   ng-model="formData.machine_id[$index]" placeholder="Chọn máy cà"
									   required>
								<md-option
										ng-repeat="option in formData.machine_list.data | filter:searchTerm[$index]"
										ng-value="option.id"
										ng-if="totalQuantum(formData.obj_invoice) < option.balance">
									{{option.name}}<br/><b class="text-red">{{option.balance |
										number}} đ</b>
								</md-option>
							</md-select>
						</md-input-container>
					</td>
					<td class="align-middle" ng-if="!formData.is_payment_merge">
						<md-input-container>
							<label>Chọn máy</label>
							<md-select data-md-container-class="selectdemoSelectHeader-{{$index}}"
									   class="machine-{{$index}}" name="Invoice[{{$index}}][machine_id]"
									   ng-model="formData.machine_id[$index]" placeholder="Chọn máy cà"
									   required>
								<md-option
										ng-repeat="option in formData.machine_list.data | filter:searchTerm[$index]"
										ng-value="option.id" ng-if="convertToNumber(obj.amount) < option.balance">
									{{option.name}}<br/><b class="text-red">{{option.balance |
										number}} đ</b>
								</md-option>
							</md-select>
						</md-input-container>
					</td>
					<td class="align-middle" ng-if="formData.is_payment_merge && $first"
						rowspan="{{formData.obj_invoice.length}}">
						<input name="Invoice[{{$index}}][batch]" class="form-control col-xs-2 col-md-2"
							   type="text" placeholder="Số lô" required/>
					</td>
					<td class="align-middle" ng-if="!formData.is_payment_merge">
						<input name="Invoice[{{$index}}][batch]" class="form-control col-xs-2 col-md-2"
							   type="text" placeholder="Số lô" required/>
					</td>
					<td class="align-middle" ng-if="formData.is_payment_merge && $first"
						rowspan="{{formData.obj_invoice.length}}">
						<input name="Invoice[{{$index}}][trace]" class="form-control" type="text"
							   placeholder="Mã giao dịch" required/>
					</td>
					<td class="align-middle" ng-if="!formData.is_payment_merge">
						<input name="Invoice[{{$index}}][trace]" class="form-control" type="text"
							   placeholder="Mã giao dịch" required/>
					</td>
					<td class="align-middle" ng-if="formData.is_payment_merge && $first"
						rowspan="{{formData.obj_invoice.length}}">
						<input name="Invoice[{{$index}}][mid]" class="form-control" type="number" maxlength="8"
							   placeholder="MID" required/>
					</td>
					<td class="align-middle" ng-if="!formData.is_payment_merge">
						<input name="Invoice[{{$index}}][mid]" class="form-control" type="number" maxlength="8"
							   placeholder="MID" required/>
					</td>
				</tr>
				@endverbatim
			</tbody>
		</table>
	</div>	
</div>