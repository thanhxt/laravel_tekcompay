<div class='row'>
	<div class="col-sm-12">
		<table class="table tab-primary">
			<tbody>
				<tr>
					<th>{{ trans('bill::batch.table.tranaction info') }}</th>
					<th>{{ trans('bill::batch.table.invoice info') }}</th>
					<th>{{ trans('bill::batch.table.picture') }}</th>
					<th>{{ trans('bill::batch.table.upload') }}</th>
				</tr>
				<?php 
					$currency_txt = trans('member::members.system.common.currency txt VND'); 
				?>
				@verbatim
				<tr ng-repeat="(key, info) in formData.bill_machine">
					<td>
						-<?php echo trans('bill::batch.table.bill_item.pos'); ?>: <b class="text-red">{{info.pos_name}}</b><br/>
						-<?php echo trans('bill::batch.table.bill_item.batch'); ?>: <b class="text-red">{{info.transaction_batch}}</b><br/>
						-<?php echo trans('bill::batch.table.bill_item.code'); ?>: <b class="text-red">{{info.transaction_code}}</b><br/>
						-<?php echo trans('bill::batch.table.bill_item.created_at'); ?>: <b class="text-red">
							<rs-time start-time="info.created_at" digital-format="'dd/M/y - h:mm TT'"></rs-time>
						</b><br/>
						-<?php echo trans('bill::batch.table.bill_item.mid'); ?>: <b class="text-red">{{info.transaction_mid}}</b><br/>
					</td>
					<td>
						<p ng-repeat="invoice in formData.bill_detail" ng-if="info.id == invoice.batch_pos_id">
							-<?php echo trans('bill::batch.table.bill_item.amount'); ?>: <b class="text-red">{{invoice.amount | number}} <?php echo $currency_txt; ?> </b>
						</p>
						<br/>
						<p>-<?php echo trans('bill::batch.table.bill_item.total'); ?>: <b class="text-red">{{info.total | number}} <?php echo $currency_txt; ?></b>
						</p>
					</td>
					<td>
						<div class="img-thumbnail" ng-if="showImage(info)">
							<a class="close-btn" ng-click="removeImage(info.id)">&times;</a>
							<img src="{{info.image_url}}" width="120px">
						</div>
					</td>
					<td>
						<div class="col-xs-12" img-upload></div>
					</td>

				</tr>
				@endverbatim
			</tbody>

		</table>
	</div>	
</div>