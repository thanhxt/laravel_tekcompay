<div class="row">
	<div class="col-md-12">
		<div class="nav-tabs-custom">
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1-1">
					{!! Form::open(['route' => ['admin.bill.bill.index'], 'method' => 'get', 'id' => 'search_form']) !!}
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('cust_number', trans('bill::bill.form.cust_number')) !!}
									{!! Form::text("cust_number",old("cust_number"), ['class' => "form-control", 'placeholder' => trans('bill::bill.form.cust_number'), "id"=>"cust_number"]) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('cust_name', trans('bill::bill.form.cust_name')) !!}
									{!! Form::text("cust_name", old("cust_name"), ['class' => "form-control", 'placeholder' => trans('bill::bill.form.cust_name'), "id"=>"cust_name"]) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('amount', trans('bill::bill.form.amount number')) !!}
									{!! Form::text("amount", old("amount"), ['class' => "form-control money-format", 'placeholder' => trans('bill::bill.form.amount number'), "id"=>"amount"]) !!}
								</div>
							</div>
						</div>
						<div class="row">
							<?php 
								$status_arr = $helpBill->getBillStatusArr();
								$agency_arr = $helpAgency->getAgencyArr();
							?>
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('status', trans('bill::bill.form.status')) !!}
									{!! Form::select("status", $status_arr, old("status"), ['class' => "form-control", 'placeholder' => trans('bill::bill.form.chosen')]) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('agency_id', trans('bill::batch-list.form.agency')) !!}
									{!! Form::select("agency_id", $agency_arr, old("agency_id"), ['class' => "form-control", 'placeholder' => trans('bill::bill.form.chosen')]) !!}
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group search-control">
									<button type="submit" class="btn btn-primary btn-danger">{{ trans('member::members.button.search') }}</button>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>	
			</div>	
		</div>	
	</div>	
</div>	

			
