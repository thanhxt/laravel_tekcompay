<table class="data-table table table-bordered table-hover">
	<thead>
		<tr>
			<th>#</th>
			<th>{{ trans('bill::bill.table.cust_number') }}</th>
			<th>{{ trans('bill::bill.table.cust_name') }}</th>
			<th>{{ trans('bill::bill.table.pay_date') }}</th>
			<th>{{ trans('bill::bill.table.amount') }}</th>
			<th>{{ trans('bill::bill.table.status') }}</th>
			<th>{{ trans('bill::bill.table.agency') }}</th>
			<th data-sortable="false">{{ trans('member::members.table.actions') }}</th>
		</tr>
	</thead>
	<tbody>
	<?php if (count($bills)): ?>
		<?php 
			$per_page = $bills->perPage();
			$current_page = $bills->currentPage();
			$index = $per_page * ($current_page -1) ;
		?>
		<?php $i=$index + 1;foreach ($bills as $item): ?>
		
		<tr>
			<td>
				{{ $i }}
			</td>
			<td>
				<span class="text-red"><i>(không có)</i></span>
			</td>
			<td>
				{{ $item->cust_name }}
			</td>
			<td>
				<?php 
					$pay_date = date('d-m-Y H:m', $item->pay_date);
					$amount = $helpBill->formatMoney($item->amount, ",");
					$status_arr = $helpBill->getBillStatusArr();
					$status = 1;
					if ($item->time_lock > 0) {
						$status = 2;
					}
				?>
				{{ $pay_date }}
			</td>
			<td>
				{{ $amount }} {{ trans('member::members.system.common.currency txt') }}
			</td>
			<td>
				{{ $status_arr[$status] }}
			</td>
			<td>
				{{ $item->agency_name }}
			</td>
			<td>
				<div class="btn-group">
					<a href="{{ route('admin.bill.bill.edit', ['id' => $item->id]) }}" class="btn btn-default btn-flat" title="{{ trans('member::members.icon.caption edit') }}"><i class="fa fa-pencil"></i></a>
				</div>
			</td>
		</tr>
		<?php $i++;endforeach; ?>
	<?php else: ?>
		<tr>
			<td colspan='6'>{{ trans('member::members.table.empty') }}</td>
		</tr>	
	<?php endif; ?>
	</tbody>
</table>
<div class='text-center'>
	<?php
		$data = request()->all();
		unset($data["_token"]);
	?>
	<?php  echo $bills->appends($data)->render() ?>
</div>