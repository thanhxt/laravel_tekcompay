@inject('help','Modules\Bill\Utility\Help')
@inject('helpAgency','Modules\Agency\Utility\Help')
<div class="row">
	<div class="col-md-12">
		<div class="nav-tabs-custom">
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1-1">
					{!! Form::open(['route' => ['admin.bill.import.index'], 'method' => 'get', 'id' => 'search_form']) !!}
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('file_name', trans('bill::import.table.file_name')) !!}
									{!! Form::text("file_name",old("file_name"), ['class' => "form-control", 'placeholder' => trans('bill::import.table.file_name')]) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('file_name_old', trans('bill::import.table.file_name_old')) !!}
									{!! Form::text("file_name_old",old("file_name_old"), ['class' => "form-control", 'placeholder' => trans('bill::import.table.file_name_old')]) !!}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('file_path', trans('bill::import.table.path')) !!}
									{!! Form::text("file_path",old("file_path"), ['class' => "form-control", 'placeholder' => trans('bill::import.table.path')]) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group{{ $errors->has('agency_id') ? ' has-error' : '' }}">
									{!! Form::label('agency_id', trans('bill::import.table.agency_name')) !!}
									<?php
										$agency_arr = $helpAgency->getAgencyArr();
									?>
									{!! Form::select("agency_id", $agency_arr, old("agency_id"), ['class' => "form-control select-search", 'placeholder' => trans('agency::manage.table.chosen')]) !!}
									{!! $errors->first('agency_id', '<span class="help-block">:message</span>') !!}
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group search-control">
									<button type="submit" class="btn btn-primary btn-danger">{{ trans('member::members.button.search') }}</button>
									<label>&nbsp;</label>
									<a href="{{ URL::route('admin.bill.import.create') }}" class="btn btn-primary btn-success" style="padding: 4px 10px;">
										<i class="fa fa-pencil"></i> {{ trans('bill::import.title.new-import-file') }}
									</a>
									(<a href="{{ route('admin.bill.import.downloadDemo') }}" class="navbar-link">{{ trans('bill::import.title.import file demo') }}:<i class="fa fa-download"></i>{{ trans('bill::import.title.import download') }}</a>)
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>	
			</div>	
		</div>	
	</div>	
</div>	

			
