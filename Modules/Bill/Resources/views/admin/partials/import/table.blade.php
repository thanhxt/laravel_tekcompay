@inject('help','Modules\Bill\Utility\Help')
<?php
	$status = $help->getImportStatusArr();
?>
<table class="data-table table table-bordered table-hover">
	<thead>
		<tr>
			<th>#</th>
			<th>{{ trans('bill::import.table.file_name') }}</th>
			<th>{{ trans('bill::import.table.file_name_old') }}</th>
			<th>{{ trans('bill::import.table.path') }}</th>
			<th>{{ trans('bill::import.table.status') }}</th>
			<th>{{ trans('agency::manage.table.name') }}</th>
		</tr>
	</thead>
	<tbody>
	<?php if (count($imports)): ?>
		<?php foreach ($imports as $item): ?>
			<tr>
				<td>
					{{ $item->id }}
				</td>
				<td>
					{{ $item->file_name }}
				</td>
				<td>
					{{ $item->file_name_old }}
				</td>
				<td>
					{{ $item->file_path }}
				</td>
				<td>
					<?php
						$label_class = $item->status ? "label-success" : "label-danger"
					?>
					<span class="label {{$label_class}}">{{ $status[$item->status] }}</span>
				</td>
				<td>
					{{ $item->name }}
				</td>
			</tr>
		<?php endforeach; ?>
	<?php else: ?>
		<tr>
			<td colspan='6'>{{ trans('member::members.table.empty') }}</td>
		</tr>		
	<?php endif; ?>
	</tbody>
</table>
<div class='text-center'>
	<?php
		$data = request()->all();
		unset($data["_token"]);
	?>
	<?php echo $imports->appends($data)->render() ?>
</div>