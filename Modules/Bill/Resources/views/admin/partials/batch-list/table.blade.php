@inject('helpBill','Modules\Bill\Utility\Help')
<table class="data-table table table-bordered table-hover">
	<thead>
		<tr>
			<th>#</th>
			<th width="250">{{ trans('bill::batch-list.table.name') }}</th>
			<th>{{ trans('bill::batch-list.table.description') }}</th>
			<th>{{ trans('bill::batch-list.table.create_at') }}</th>
			<th data-sortable="false">{{ trans('member::members.table.actions') }}</th>
		</tr>
	</thead>
	<tbody>
	<?php if (count($list_batches)): ?>
		<?php foreach ($list_batches as $item): ?>
		<tr>
			<td>
				{{ $item->id }}
			</td>
			<td> 
				{{ $item->name }}
			</td>
			<td>
				{{ $item->description }}
			</td>
			<td>
				<?php 
					$created_at = $helpBill->formatDateForDisplay($item->created_at, 'd/m/Y');
				?>
				{{ $created_at }}
			</td>
			<td>
				<div class="btn-group">
					<a href="{{ route('admin.bill.batch-list.edit', ['id' => $item->id]) }}" class="btn btn-default btn-flat" title="{{ trans('member::members.icon.caption edit') }}"><i class="fa fa-pencil"></i></a>
					<a href="{{ route('admin.bill.batch-list.view', [$item->id]) }}" class="btn btn-default btn-flat" title="{{ trans('member::members.icon.caption view') }}"><i class="fa fa-reorder"></i></a>
					<!--
					<button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.bill.batch.delete', [$item->id]) }}" title="{{ trans('member::members.icon.caption delete') }}"><i class="fa fa-trash"></i></button>
					-->
				</div>
			</td>
		</tr>
		<?php endforeach; ?>
	<?php else: ?>
		<tr>
			<td colspan='4'>{{ trans('member::members.table.empty') }}</td>
		</tr>	
	<?php endif; ?>
	</tbody>
</table>
<div class='text-center'>
	<?php
		$data = request()->all();
		unset($data["_token"]);
	?>
	<?php  echo $list_batches->appends($data)->render() ?>
</div>