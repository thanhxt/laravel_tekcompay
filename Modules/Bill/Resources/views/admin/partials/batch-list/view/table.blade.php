@inject('helpBill','Modules\Bill\Utility\Help')
<table class="data-table table table-bordered table-hover">
	<thead>
		<tr>
			<th>{{ trans('bill::batch-list.table.customer info') }}</th>
			<th>{{ trans('bill::batch-list.table.batch amount') }}</th>
			<th>{{ trans('bill::batch-list.table.batch total') }}</th>
			<th>{{ trans('bill::batch-list.table.fee') }}</th>
			<th width="10" data-sortable="false">#</th>
		</tr>
	</thead>
	<tbody>
	<?php if (count($batches)): ?>
		<?php foreach ($batches as $item): ?>
		<tr>
			<td>
				<?php 
					$customer_name = $item->cust_name;
					$bank_name = $item->bank_name;
					$customer_phone = $item->cust_phone;
					$card_name = $item->card_name;
					$customer_card_number = $item->card_number;
				?>
				- {{ trans('bill::batch.table.item.customer') }}: <b class="text-red">{{ $customer_name }}</b>
				<br>- {{ trans('bill::batch.table.item.bank') }}: <b>{{ $bank_name }}</b>
				<br>- {{ trans('bill::batch.table.item.customer phone') }}: <b>{{ $customer_phone }}</b>
				<br>- {{ trans('bill::batch.table.item.customer card name') }}: <b>{{ $card_name }}</b>
				<br>- {{ trans('bill::batch.table.item.customer card number') }}: <b>{{ $customer_card_number }}</b>
			</td>
			
			
			<td>
				<?php 
					$bills_achieve = $helpBill->getBillByBatchId($item->id);
				?>
				<?php if($bills_achieve->count()):?>
					<?php $i=1;foreach($bills_achieve as $bill): ?>
						<?php 
							$bill_amount = $helpBill->formatMoney($bill->amount);
						?>
						<p>
							{{ trans('bill::batch.table.item.amount') }} {{ $i }}:<b> {{ $bill_amount }} {{ trans('member::members.system.common.currency txt VND') }}</b>
						</p>
					<?php $i++;endforeach; ?>
				<?php endif; ?>
			</td>
			<td>
			<?php 
					$base_amount = $helpBill->formatMoney($item->base_amount);
				?>
				<b class="text-red">{{ $base_amount }} {{ trans('member::members.system.common.currency txt VND') }}</b>
			</td>
			<td>
				{{ trans('member::members.system.common.not assign') }}
			</td>
			<td>
				<div class="btn-group">
					<button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.bill.batch-list.delete-batch', [$item->batch_list_id, $item->id, $item->base_amount]) }}" title="{{ trans('member::members.icon.caption delete') }}"><i class="fa fa-trash"></i></button>
				</div>
			</td>
		</tr>
		<?php endforeach; ?>
	<?php else: ?>
		<tr>
			<td colspan='9'>{{ trans('member::members.table.empty') }}</td>
		</tr>	
	<?php endif; ?>
	</tbody>
</table>