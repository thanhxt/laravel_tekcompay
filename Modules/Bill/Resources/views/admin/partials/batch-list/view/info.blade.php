<div class='box box-danger' style='min-height: 335px;'>
	{!! Form::hidden('batch_list_id', $model->id, []) !!}
	{!! Form::hidden('agency_id', $model->agency_id, []) !!}
	<div class='box-header'>
		<h3 class="box-title">{{ trans('bill::batch-list.title.info') }}</h3>
	</div>
	<div class='box-body'>
		<div class="col-xs-12 field-listbatch-name">
			<label class="control-label" for="listbatch-name">{{ trans('bill::batch-list.form.name') }}</label>
			<br>
			<b class="text-red">
				- {{ $model->name}}                           
			</b>
		</div>
		<div class="col-xs-12 field-listbatch-name">
			<label class="control-label" for="listbatch-name">{{ trans('bill::batch-list.form.description') }}</label>
			<br>
			<p>
				{{ $model->description }}                 
			</p>
		</div>
	</div>
	<!-- box-body -->
	<div class="box-footer">
		<label class="control-label" for="listbatch-name">{{ trans('bill::batch-list.form.total') }}:</label>
		<b class="text-red">
			<?php 
				$total = round($model->total, 0);
				$total = $helpBill->formatMoney($model->total, ",");
			?>
			{{ $total }} {{ trans('member::members.system.common.currency txt VND') }}
		</b>
	</div>
</div>
<!-- box box-danger -->