@extends('layouts.master')

@section('content-header')
<h1>
    {{ trans('bill::bill.title.list') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class="active">{{ trans('bill::bill.title.list') }}</li>
</ol>
@stop

@section('content')
@inject('helpAgency','Modules\Agency\Utility\Help')
@inject('helpBill','Modules\Bill\Utility\Help')
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-body">
				@include('bill::admin.partials.bill.search')
			</div>
		</div>
		<div class="box box-danger">
			<div class="box-body">
				@include('bill::admin.partials.bill.table')
			</div>
		</div>
	</div>
</div>

@include('core::partials.delete-modal')
@stop

@section('scripts')
	{!! Theme::script('vendor/inputmask/js/jquery.inputmask.js') !!}
    <script>
        $( document ).ready(function() {
			$(".money-format").inputmask({"alias":"numeric","groupSeparator":",","autoGroup":true,"removeMaskOnSubmit":true});
        });
    </script>
@stop