@extends('layouts.master')

@section('content-header')
	<?php 
		$translate_path = 'bill::bill.title.edit';
	?>
    <h1>
        {{ trans($translate_path) }}: {{ $model->id}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ URL::route('admin.bill.bill.index') }}">{{ trans('bill::bill.title.list') }}</a></li>
        <li class="active">{{ trans($translate_path) }}</li>
    </ol>
@stop

@section('content')
@inject('help','Modules\Agency\Utility\Help')
    {!! Form::open(['route' => ['admin.bill.bill.edit.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <div class="tab-content">
					<div class="tab-pane active" id="tab_1-1">
						<div class="box-body">
							{!! Form::hidden('id', @$model->id, []) !!}
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group{{ $errors->has('pay_date') ? ' has-error' : '' }}">
										{!! Form::label('pay_date', trans('bill::bill.form.pay_date')) !!}
										{!! Form::text("pay_date",@$model->pay_date, ['class' => "form-control", 'placeholder' => trans('bill::bill.form.pay_date'), "id"=>"pay_date",'required'=>'required']) !!}
										{!! $errors->first('pay_date', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group{{ $errors->has('cust_number') ? ' has-error' : '' }}">
										{!! Form::label('cust_number', trans('bill::bill.form.cust_number')) !!}
										{!! Form::text("cust_number",@$model->cust_number, ['class' => "form-control", 'placeholder' => trans('bill::bill.form.cust_number'), "id"=>"cust_number"]) !!}
										{!! $errors->first('cust_number', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group{{ $errors->has('cust_name') ? ' has-error' : '' }}">
										{!! Form::label('cust_name', trans('bill::bill.form.cust_name')) !!}
										{!! Form::text("cust_name",@$model->cust_name, ['class' => "form-control", 'placeholder' => trans('bill::bill.form.cust_name'), "id"=>"cust_name",'required'=>'required']) !!}
										{!! $errors->first('cust_name', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group{{ $errors->has('import_date') ? ' has-error' : '' }}">
										{!! Form::label('import_date', trans('bill::bill.form.import_date')) !!}
										{!! Form::text("import_date",@$model->import_date, ['class' => "form-control", 'placeholder' => trans('bill::bill.form.import_date'), "id"=>"import_date",'required'=>'required']) !!}
										{!! $errors->first('import_date', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group{{ $errors->has('recurring_invoice') ? ' has-error' : '' }}">
										{!! Form::label('recurring_invoice', trans('bill::bill.form.recurring_invoice')) !!}
										{!! Form::text("recurring_invoice",@$model->recurring_invoice, ['class' => "form-control", 'placeholder' => trans('bill::bill.form.recurring_invoice'), "id"=>"recurring_invoice"]) !!}
										{!! $errors->first('recurring_invoice', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
										{!! Form::label('description', trans('bill::bill.form.description')) !!}
										{!! Form::text("description",@$model->description, ['class' => "form-control", 'placeholder' => trans('bill::bill.form.description'), "id"=>"description",'required'=>'required']) !!}
										{!! $errors->first('description', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
										{!! Form::label('amount', trans('bill::bill.form.amount number')) !!}
										{!! Form::text("amount",@$model->amount, ['class' => "form-control", 'placeholder' => trans('bill::bill.form.amount'), "id"=>"amount",'required'=>'required']) !!}
										{!! $errors->first('amount', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group{{ $errors->has('bill_type') ? ' has-error' : '' }}">
										{!! Form::label('bill_type', trans('bill::bill.form.bill_type')) !!}
										{!! Form::text("bill_type",@$model->bill_type, ['class' => "form-control", 'placeholder' => trans('bill::bill.form.bill_type'), "id"=>"bill_type",'required'=>'required']) !!}
										{!! $errors->first('bill_type', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
										{!! Form::label('status', trans('bill::bill.form.status')) !!}
										{!! Form::text("status",@$model->status, ['class' => "form-control", 'placeholder' => trans('bill::bill.form.status'), "id"=>"status",'required'=>'required']) !!}
										{!! $errors->first('status', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group{{ $errors->has('created_at') ? ' has-error' : '' }}">
										{!! Form::label('created_at', trans('bill::bill.form.created_at')) !!}
										{!! Form::text("created_at",@$model->created_at, ['class' => "form-control", 'placeholder' => trans('bill::bill.form.created_at'), "id"=>"created_at",'required'=>'required']) !!}
										{!! $errors->first('created_at', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group{{ $errors->has('updated_at') ? ' has-error' : '' }}">
										{!! Form::label('updated_at', trans('bill::bill.form.updated_at')) !!}
										{!! Form::text("updated_at",@$model->updated_at, ['class' => "form-control", 'placeholder' => trans('bill::bill.form.updated_at'), "id"=>"updated_at",'required'=>'required']) !!}
										{!! $errors->first('updated_at', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group{{ $errors->has('create_by') ? ' has-error' : '' }}">
										{!! Form::label('create_by', trans('bill::bill.form.create_by')) !!}
										{!! Form::text("create_by",@$model->create_by, ['class' => "form-control", 'placeholder' => trans('bill::bill.form.create_by'), "id"=>"create_by",'required'=>'required']) !!}
										{!! $errors->first('create_by', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group{{ $errors->has('update_by') ? ' has-error' : '' }}">
										{!! Form::label('update_by', trans('bill::bill.form.update_by')) !!}
										{!! Form::text("update_by",@$model->update_by, ['class' => "form-control", 'placeholder' => trans('bill::bill.form.update_by'), "id"=>"update_by"]) !!}
										{!! $errors->first('update_by', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="box-footer">
						<?php 
							$btn_txt = 'core::core.button.save';
						?>
						<?php if(!$model->time_lock):?>
							<button type="submit" class="btn btn-primary btn-success">{{ trans($btn_txt) }}</button>
						<?php endif; ?>
                        <button class="btn btn-default btn-flat" name="button" type="reset">{{ trans('core::core.button.reset') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ URL::route('admin.bill.bill.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>

    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('page::pages.navigation.back to index') }}</dd>
    </dl>
@stop

