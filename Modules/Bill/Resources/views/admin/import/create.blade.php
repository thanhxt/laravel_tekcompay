@extends('layouts.master')

@section('content-header')
<h1>
    {{ trans('bill::import.title.new-import-file') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class=""><a href="{{ URL::route('admin.member.index') }}">{{ trans('bill::import.title.import') }}</a></li>
    <li class="active">{{ trans('bill::import.title.new-import-file') }}</li>
</ol>
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('bill::import.navigation.back to index') }}</dd>
    </dl>
@stop
@section('content')
@inject('help','Modules\Bill\Utility\Help')
@inject('helpAgency','Modules\Agency\Utility\Help')
{!! Form::open(['route' => 'admin.bill.import.store', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('file_import') ? ' has-error' : '' }}">
                                    {!! Form::label('file_import', trans('bill::import.form.upload-file')) !!}
                                    {!! Form::file('file_import', ['class' => 'form-control', 'placeholder' => trans('bill::import.form.upload-file')]) !!}
                                    {!! $errors->first('file_import', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
								<?php
									$bill_types = $help->getBillTypeArr();
								?>
                                <div class="form-group{{ $errors->has('bill_type') ? ' has-error' : '' }}">
                                    {!! Form::label('bill_type', trans('bill::import.form.bill_type')) !!}
									{!! Form::select("bill_type", $bill_types, old("bill_type"), ['class' => "form-control", 'placeholder' => trans('bill::import.form.bill_type')]) !!}
                                    {!! $errors->first('bill_type', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
							<div class="col-sm-4">
								<?php
									$agency_arr = $helpAgency->getAgencyArr();
								?>
                                <div class="form-group{{ $errors->has('agency_id') ? ' has-error' : '' }}">
                                    {!! Form::label('agency_id', trans('bill::import.form.agency-id')) !!}
                                    {!! Form::select("agency_id", $agency_arr, old("agency_id"), ['class' => "form-control", 'placeholder' => trans('bill::import.form.agency_id')]) !!}
                                    {!! $errors->first('agency_id', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
							<div class="col-sm-12">
								<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
									{!! Form::label('description', trans('bill::import.form.description')) !!}
									{!! Form::textarea('description', old("description"), ['class' => 'form-control', 'placeholder' => trans('bill::import.form.description')]) !!}
									{!! $errors->first('description', '<span class="help-block">:message</span>') !!}
								</div>
							</div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btn-success">{{ trans('core::core.button.save') }}</button>
                    <button class="btn btn-default btn-flat" name="button" type="reset">{{ trans('core::core.button.reset') }}</button>
                    <a class="btn btn-danger pull-right btn-flat" href="{{ URL::route('admin.bill.import.index')}}"><i class="fa fa-times"></i> {{ trans('user::button.cancel') }}</a>
                </div>
            </div>
        </div>

    </div>
</div>
{!! Form::close() !!}
@stop
@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('bill::import.navigation.back to index') }}</dd>
    </dl>
@stop
