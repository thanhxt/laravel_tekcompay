<?php

namespace Modules\Bill\Business;

use Modules\Bill\Entities\BatchList;
use Modules\Bill\Entities\Batch;

class BatchListBus
{
    public function search($agency_id, $name, $pageSize = PAGE_SIZE){
        $batch_list_model = new BatchList();
        $data = $batch_list_model->search($agency_id, $name, $pageSize);
        return $data;
    }

    public function saveData($data)
    {
        $id = @$data["id"];
        $bank_model = $this->getBatchListModelById($id);
        return $bank_model->saveData($data);
    }

   public function deleteBatch($batch_id,$batch_list_id,$amount){
        $result = false;
        $delete_amount = -1*$amount;
        \DB::beginTransaction();
        try {
           $this->updateBatchListTotal($batch_list_id, $delete_amount);

            $model = Batch::find($batch_id);
            $time_lock = $model->time_lock;
            $this->unlockAllBillByTimeLock($time_lock);
           $result  = $model->delete();

           \DB::commit();
        }catch (\Exception $e) {
           \DB::rollBack();
        }
        return $result;
   }

    public function unlockAllBillByTimeLock($time_lock) {
        \DB::table('bills')->where('time_lock', '=', $time_lock)->update(['time_lock' => 0]);
    }

    /**
     * @param $agency_id
     * @param $data
     * @param $create_by
     * @param $batch_list_id
     * @return mixed
     */
    public function createBatch($agency_id, $data, $create_by, $batch_list_id,$total_bill){


        $model = new Batch();
        $created_at = strtotime('now');
        $data['status'] = 1;
        $data['agency_id'] = $agency_id;
        $data['create_by'] = $create_by;
        $data['created_at'] = $created_at;
        $data['updated_at'] = $created_at;
        $data['batch_list_id'] = $batch_list_id;
        $model->fill($data);
        \DB::beginTransaction();
        try {
            $model->save();
            $this->updateBatchListTotal($batch_list_id, $total_bill);
            \DB::commit();
        }catch (\Exception $e) {
            \DB::rollBack();
        }
        return $model->id;
    }


    public function getBatchListModelById($id) {
        if($id){
            $model = BatchList::find($id);
        }else{
            $model = new BatchList();
        }
        return $model;
    }

    public function getBatchsByBatchListId($batch_list_id) {
        $model = new Batch();
        $data = $model->getBatchsBatchListId($batch_list_id);
        return $data;
    }


    public function updateBatchListTotal($batch_list_id, $total_bill) {
        $model = $this->getBatchListModelById($batch_list_id);
        $model->total = $model->total + $total_bill;
        $model->save();
    }
}