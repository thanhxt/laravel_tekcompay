<?php

namespace Modules\Bill\Business;

use Modules\Bill\Entities\Batch;
use Modules\Bill\Entities\BatchPos;
use Modules\Bill\Utility\Upload;

class BatchBus
{
    const BILL_LIMIT = 200;

    public function search($agency_id, $bank_id = 0, $pos_id = 0, $created_at, $pageSize = PAGE_SIZE) {
        $batch_model = new Batch();
        $data = $batch_model->search($agency_id, $bank_id, $pos_id, $created_at, $pageSize);
        return $data;
    }

    public function saveData($data) {
        $id = $data["id"];
        if ($id) {
            $updated_at = strtotime('now');
            $data['updated_at'] = $updated_at;
        }
        $agency_model = $this->getBatchModelById($id);
        return $agency_model->saveData($data);
    }

    public function getBatchModelById($id) {
        if($id){
            $model = Batch::find($id);
        }else{
            $model = new Batch();
        }
        return $model;
    }

    public function deleteBatch($batch_id) {
        $result = false;
        // get archive bills by batch_id ;
        $archrive_bills = $this->_getAchriveBillByBatchId($batch_id);

        \DB::beginTransaction();
        try {
            // move archrive bills to bill of agent;
            $this->_moveAchriveBillToBillTable($archrive_bills);
            // delete batch item by id ;
            $batch_model = Batch::find($batch_id);
            $batch_model->delete();
            \DB::commit();
            $result = true;
        }catch (\Exception $e) {
            \DB::rollBack();
        }
        return $result;
    }

    public function getBatchMachine($batch_id) {
        $model = \DB::table('batchpos')->where('batch_id', '=', $batch_id);

        $model->join('pos', function ($join) {
            $join->on('batchpos.pos_id', '=', 'pos.id');
        });

        $data = $model->select(
            [
                'batchpos.id',
                'batchpos.pos_id',
                'pos.name as pos_name',
                'batchpos.transaction_code',
                'batchpos.transaction_batch',
                'batchpos.transaction_mid',
                'batchpos.created_at',
                'batchpos.image',
            ]
        )->get();
        $data_result = [];
        if (count($data)) {
            foreach ($data as $item) {
                $item->total = 0;
                $item->created_at = strtotime($item->created_at);
                $image_url = "";
                if ($item->image) {
                    $image_url = Upload::getBatchPathUrl($item->image);
                }
                $item->image_url = $image_url;
                $data_result[]= $item ;
            }
        }
        return $data_result;
    }

    private function _getAchriveBillByBatchId($batch_id){
        $data = [];
        $model = \DB::table('billachieves');

        $model->join('billpos', function ($join) {
            $join->on('billachieves.id', '=', 'billpos.bill_id');
        });

        $model->join('batchpos', function ($join) use ($batch_id) {
            $join->on('billpos.batch_pos_id', '=', 'batchpos.id');
            $join->where('batchpos.batch_id', '=', $batch_id);
        });
        $data = $model->select(
            [
                'billachieves.id',
                'billachieves.pay_date',
                'billachieves.cust_number',
                'billachieves.cust_name',
                'billachieves.import_type',
                'billachieves.import_date',
                'billachieves.recurring_invoice',
                'billachieves.description',
                'billachieves.amount',
                'billachieves.bill_type',
                'billachieves.status',
                'billachieves.created_at',
                'billachieves.updated_at',
                'billachieves.create_by',
                'billachieves.update_by',
                'billachieves.agency_id',
                'billachieves.batch_id',
            ]
        )->get();

        return $data;
    }

    private function _moveAchriveBillToBillTable($bills){
        if (count($bills)) {
            foreach ($bills as $item) {
                $this->_insertToBill($item);
                $this->_deleteAchriveBillById($item->id);
            }
        }
    }

    private function _insertToBill($item){
        $sql_string = "insert into bills (
                                    id, 
                                    pay_date, 
                                    cust_number, 
                                    cust_name, 
                                    import_type, 
                                    import_date, 
                                    recurring_invoice, 
                                    description, 
                                    amount, 
                                    bill_type, 
                                    status, 
                                    created_at, 
                                    updated_at, 
                                    create_by, 
                                    update_by, 
                                    agency_id
                                    ) 
                        values (?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?)";
        $params = [
            $item->id,
            $item->pay_date,
            $item->cust_number,
            $item->cust_name,
            $item->import_type,
            $item->import_date,
            $item->recurring_invoice,
            $item->description,
            $item->amount,
            $item->bill_type,
            $item->status,
            $item->created_at,
            $item->updated_at,
            $item->create_by,
            $item->update_by,
            $item->agency_id
        ];

        \DB::insert($sql_string, $params);
    }

    private function _deleteAchriveBillById($id) {
        $model = \DB::table('billachieves')->where('id', '=', $id)->delete();
    }

    /* BILL MODEL */
    public function getPrepareBills($agency_id, $money, $time_lock = 0) {
        $total = 0;
        $current_money = 0;
        $second_time = false;
        $orders = $this->getBillAvaiable($agency_id, $money);
        $done = true;
        $i=1;
        $sort ="desc";
        $lastedSelect = [];
        while($done) {
            if ($orders->count()) {

                foreach($orders as $item) {
                    if ($i >= self::BILL_LIMIT) {
                        $done = false;
                        break;
                    }
                    if (array_key_exists($item->id, $lastedSelect)) {
                        $done = false;
                        break;
                    }
                    $lastedSelect[$item->id] = $item->id;
                    if (($current_money + $item->amount) <= $money) {
                        $current_money += $item->amount;
                        // update time lock when search
                        if ($time_lock) {
                            $this->_updateTimeLockForBill($item->id, $time_lock);
                        }
                        $i++;
                    }

                }
            } else {
                $done = false;
            }
            $tmp_money = $money - $current_money;

            $orders = $this->getBillAvaiable($agency_id,$tmp_money,$sort);
            if($sort=="desc") $sort="asc";

        }

        $results =  $this->getBillByTimeLock($time_lock);

        return $results;
    }


    public function getBillByTimeLock($time_lock){
        $orders = \DB::table('bills')->select('*')
            ->where('time_lock', '=', $time_lock)
            ->orderBy('amount', 'desc')
            ->get();
        return $orders;
    }
    public function getBillAvaiable($agency_id, $money, $sortby="desc" , $limit = 100) {
        $orders = \DB::table('bills')->select('*')
            ->where('amount', '<', $money)
            ->where('amount', '>', 0)
            ->where('time_lock', '=', 0)
            ->where('agency_id', '=', $agency_id)
            ->orderBy('amount', $sortby)
            ->limit($limit);
        return $orders->get();
    }

    public function unlockTimeForBill($bill_id) {
        $this->_updateTimeLockForBill($bill_id, 0);
    }

    public function unlockAllBillByTimeLock($time_lock) {
        \DB::table('bills')->where('time_lock', '=', $time_lock)->update(['time_lock' => 0]);
    }

    public function getAchieveBillsByBatchId($batch_id) {
        $batch_model = $this->getBatchModelById($batch_id);
        $data = $batch_model->getAchieveBill();
        return $data;
    }

    public function saveImageForBatchPos($batch_pos_id, $file_name = "") {
        $model = $this->getBatchPosModel($batch_pos_id);
        $result = $model->saveImage($file_name);
        return $result;
    }

    public function getBatchPosModel($id) {
        if($id){
            $model = BatchPos::find($id);
        }else{
            $model = new BatchPos();
        }
        return $model;
    }

    public function removeImageForBatchPos($batch_pos_id) {
        return $this->saveImageForBatchPos($batch_pos_id);
    }

    private function _updateTimeLockForBill($bill_id, $time_lock) {
        \DB::table('bills')->where('id', '=', $bill_id)->update(['time_lock' => $time_lock]);
    }

}