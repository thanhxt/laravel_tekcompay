<?php

namespace Modules\Bill\Business;

use Modules\Bill\Entities\Batch;
use Modules\Bill\Entities\BatchPos;
use Modules\Bill\Entities\Bill;
use Modules\Bill\Entities\BillPos;

class BillBus
{
    public function search($cus_number, $cus_name, $amount, $status, $agency_id, $pageSize = PAGE_SIZE)
    {
        $bill_model = new Bill();
        $data = $bill_model->search($cus_number, $cus_name, $amount, $status, $agency_id, $pageSize);
        return $data;
    }

    public function saveData($data) {
        $id = $data["id"];
        $bill_model = $this->getBillModelById($id);
        return $bill_model->saveData($data);
    }

    public function getBillModelById($id) {
        if($id){
            $model = Bill::find($id);
        }else{
            $model = new Bill();
        }
        return $model;
    }



    public function createBill($agency_id, $data, $create_by = 0, $batch_list_id = 0) {
        $batch_id = 0;
        \DB::beginTransaction();
        try {
            // insert batch table
            $batch_id = $this->_createBatch($agency_id, $data, $create_by, $batch_list_id);
            $time_lock = $data['time_lock'];
            $amount = $data['amount'];
            $is_payment_merge = $data['is_payment_merge'];
            // select all bills has posted time lock
            $bills_data = $this->_getAllBillByTimeLock($time_lock);
            // insert bill achieve table
            $pay_date = date("Y-m-d H:i:s");
            if (count($bills_data)) {
                $i=1;
                $current_pos_id = 0;
                $batch_pos_id = 0;
                foreach ($bills_data as $item) {
                    $item_id = $item->id;
                    $item->pos_id = 0;
                    $invoice_data = $this->_formatInvoiceData($data['Invoice']);

                    if (isset($invoice_data[$item_id])) {
                        $invoice_item = $invoice_data[$item_id];
                        $item->pos_id = $invoice_item['pos_id'];
                        $item->transaction_code = $invoice_item['transaction_code'];
                        $item->transaction_batch = $invoice_item['transaction_batch'];
                        $item->transaction_mid = $invoice_item['transaction_mid'];
                    }
                    $this->_createBillAchieve($batch_id, $item);
                    // update pos balance
                    if( $item->pos_id){
                        if($is_payment_merge && $i==1){
                            $current_pos_id = $item->pos_id;
                            $transaction_code = $item->transaction_code;
                            $transaction_batch = $item->transaction_batch;
                            $transaction_mid = $item->transaction_mid;
                            $this->_updateBalancePos($current_pos_id,$amount);
                            // update batch pos and bill post
                            $batch_pos_id =$this->_insertBatchPos($batch_id, $current_pos_id, $transaction_code, $transaction_batch, $transaction_mid);
                        }else{
                            $this->_updateBalancePos($item->pos_id,$item->amount);
                            $current_pos_id = $item->pos_id;
                            $transaction_code = $item->transaction_code;
                            $transaction_batch = $item->transaction_batch;
                            $transaction_mid = $item->transaction_mid;
                            $this->_updateBalancePos($current_pos_id,$amount);
                            // update batch pos and bill post
                            $batch_pos_id = $this->_insertBatchPos($batch_id, $current_pos_id, $transaction_code, $transaction_batch, $transaction_mid);
                        }
                    }
                    if ($batch_pos_id > 0) {
                        $this->_insertBillPos($batch_pos_id, $item_id, $item->amount);
                    }
                    $i++;
                }
            }
            // delete bill by time lock
            $this->_deleteBillByTimeLock($time_lock);
            \DB::commit();
        }catch (\Exception $e) {
            \DB::rollBack();
        }
        return $batch_id;
    }

    /**
     * @param $agency_id
     * @param $data
     * @param $create_by
     * @param $batch_list_id
     * @return mixed
     */
    private function _createBatch($agency_id, $data, $create_by, $batch_list_id){
        $model = new Batch();
        $created_at = strtotime('now');
        $data['status'] = 1;
        $data['agency_id'] = $agency_id;
        $data['create_by'] = $create_by;
        $data['created_at'] = $created_at;
        $data['updated_at'] = $created_at;
        $data['batch_list_id'] = $batch_list_id;
        $model->fill($data);
        $model->save();
        return $model->id;
    }

    /**
     * Get all bill by time lock
     * @param $time_lock
     * @return mixed
     */
    private function _getAllBillByTimeLock($time_lock){
        $bills = \DB::table('bills')->select('*')
            ->where('time_lock', '=', $time_lock);
        return $bills->get();
    }

    /**
     * Create bill achieve from post data
     * @param $batch_id
     * @param $item
     */
    private function _createBillAchieve($batch_id, $item){
        $sql_string = "insert into billachieves (
                                    id, 
                                    pay_date, 
                                    cust_number, 
                                    cust_name, 
                                    import_type, 
                                    import_date, 
                                    recurring_invoice, 
                                    description, 
                                    amount, 
                                    bill_type, 
                                    status, 
                                    created_at, 
                                    updated_at, 
                                    create_by, 
                                    update_by, 
                                    time_lock, 
                                    agency_id,
                                    batch_id
                                    ) 
                        values (?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?)";
        $params = [
            $item->id,
            $item->pay_date,
            $item->cust_number,
            $item->cust_name,
            $item->import_type,
            $item->import_date,
            $item->recurring_invoice,
            $item->description,
            $item->amount,
            $item->bill_type,
            $item->status,
            $item->created_at,
            $item->updated_at,
            $item->create_by,
            $item->update_by,
            $item->time_lock,
            $item->agency_id,
            $batch_id
        ];

        \DB::insert($sql_string, $params);
    }

    /**
     * Delete bill by time lock
     * @param $time_lock
     */
    private function _deleteBillByTimeLock($time_lock){
        \DB::table('bills')->where('time_lock', '=', $time_lock)->delete();
    }

    private function _formatInvoiceData($invoice_data) {
        $data = [];
        if (count($invoice_data)) {
            foreach ($invoice_data as $item) {
                $id = $item['id'];
                $info['pos_id'] = 0;
                $info['transaction_batch'] = '';
                $info['transaction_code'] = '';
                $info['transaction_mid'] = 0;
                if (isset($item['machine_id'])) {
                    $info['pos_id'] = $item['machine_id'];
                    $info['transaction_batch'] = $item['batch'];
                    $info['transaction_code'] = $item['trace'];
                    $info['transaction_mid'] = $item['mid'];
                }
                $data[$id] = $info;
            }
        }

        return $data;
    }

    private function _updateBalancePos($pos_id, $money) {
        $sql = 'Update pos SET spend = spend + ' . $money . ', balance = balance-' . $money . ' Where id = ' . $pos_id;
        \DB::update($sql);
    }

    private function _insertBatchPos($batch_id, $pos_id, $transaction_code, $transaction_batch, $transaction_mid) {
        $model = new BatchPos();
        $data['batch_id'] = $batch_id;
        $data['pos_id'] = $pos_id;
        $data['transaction_code'] = $transaction_code;
        $data['transaction_batch'] = $transaction_batch;
        $data['transaction_mid'] = $transaction_mid;
        $model->fill($data);
        $model->save();
        return $model->id;
    }

    private function _insertBillPos($batch_pos_id, $bill_id, $amount) {
        $model = new BillPos();
        $data['batch_pos_id'] = $batch_pos_id;
        $data['bill_id'] = $bill_id;
        $data['amount'] = $amount;
        $model->fill($data);
        return $model->save();
    }
}