<?php

namespace Modules\Bill\Business;

use Modules\Bill\Entities\Bill;
use Modules\Bill\Entities\Import;
use Modules\Bill\Utility\Help;
use Modules\Bill\Utility\SimpleXLSX;
use Modules\Bill\Utility\Upload;

class ImportBus
{
    /**
     * @var Upload
     */
    protected $_upload;

    public function __construct(Upload $upload)
    {
        $this->_upload = $upload;
    }

    public function search($file_name, $file_name_old, $file_path, $agency_id, $paging = PAGE_SIZE) {
        $import_model = new Import();
        $data = $import_model->search($file_name, $file_name_old, $file_path, $agency_id, $paging);
        return $data;
    }

    public function import($request, $user_id) {
        return $this->_importFileExcel($request, $user_id);
    }

    private function _importFileExcel($request, $user_id) {
        $data_file  = $request->file('file_import');
        $data = $request->all();

        $bill_type = $data['bill_type'];
        $agency_id = $data['agency_id'];
        $description = $data['description'];
        // save bill data to db
        $result = $this->_processSaveBillData($data_file, $bill_type, $agency_id, $user_id, $description);
        return $result;
    }

    private function _processSaveBillData($data_file, $bill_type, $agency_id, $user_id, $description) {
        $result = false;
        $create_at = strtotime('now');
        $sql_imports = $this->createSqlImportByExcelFile($data_file, $agency_id,$bill_type,$create_at, $user_id);
        \DB::beginTransaction();
        try {
           if (count($sql_imports)) {
               foreach ($sql_imports as $sql) {
                   \DB::insert(\DB::raw($sql));
               }
           }
            $this->_processSaveImportData($data_file, $bill_type, $agency_id, $user_id, $description);
            \DB::commit();
            $result = true;
        } catch (\Exception $e) {
            $message = $e->getMessage();
            //print_r($message); exit;
            \DB::rollBack();
        }

        return $result;
    }

    private function _insertBillsTableSql() {
        $table_name = "bills";
        $sql = "INSERT INTO %s (
                                pay_date, 
                                cust_number,
                                cust_name, 
                                description,
                                recurring_invoice,
                                import_type, 
                                import_date, 
                                amount, 
                                bill_type, 
                                status,
                                created_at,
                                updated_at,
                                create_by, 
                                update_by, 
                                time_lock,
                                agency_id
                                ) VALUES ";
        return  sprintf($sql,$table_name);
    }

    public  function createSqlImportByExcelFile($file,$agency_id,$bill_type,$create_at,$user_id) {

        $int_max_item = 300;
        $data = [];
        $i=1 ;
        if ($xlsx = SimpleXLSX::parse($file)) {
            $count = count($xlsx->rows()) -1 ;
            $sql_item ="";
            foreach ( $xlsx->rows() as $k => $r ) {
                if ( $k === 0 ) {
                    $header_values = $r;
                    continue;
                }
                $row = array_combine( $header_values, $r );
                if ($row['STT'] > 0) {

                    $item['cust_number'] = $row['Mã khách hàng'];
                    $item['cust_name'] = $row['Tên Khách hàng'];
                    $item['amount'] = $row['Số tiền'];
                    $pay_date = $row['Ngày thanh toán'];
                    $pay_date_arr = explode(':', $pay_date);
                    if (count($pay_date_arr) == 3) {
                        $pay_date = $pay_date_arr[0] . ":" . $pay_date_arr[1];
                    }
                    $item['pay_date'] = strtotime($pay_date);

                    $index = $i%$int_max_item;
                    $sql_item .= $this->_createBillValueInsert($item,$agency_id,$bill_type,$create_at,$user_id);
                    if(!$index || $i==$count){
                        $sql_item .= ";" ;
                        $sql_insert = $this->_insertBillsTableSql() . $sql_item;
                        $data[] = $sql_insert;
                        $sql_item ="" ;

                    }else{
                        $sql_item .= "," ;
                    }

                    $i++;
                }
            }
        }

        return $data;
    }

    /**'
     * @param $item
     * @param $agency_id
     * @param $bill_type
     * @param $create_at
     * @param $user_id
     * @return string
     */
    private function _createBillValueInsert( $item,$agency_id,$bill_type, $create_at,$user_id){
        $item_format = "(%s ,'%s', '%s', '%s','%s',%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)";

            $pay_date = $item['pay_date'];
            $cust_number = $this->_replaceSpecialCharacter($item["cust_number"]);
            $cust_name = $this->_replaceSpecialCharacter($item["cust_name"]);
            $description = "" ;
            $recurring_invoice = "";
            $import_type = 1;
            $import_date = $create_at;
            $amount = $item["amount"];
            $status = 1;
            $created_at = $create_at;
            $updated_at = $create_at;
            $create_by = $user_id;
            $update_by = $user_id;
            $time_lock = 0;

            $str_item_sql = sprintf($item_format,
                $pay_date,
                $cust_number,
                $cust_name,
                $description,
                $recurring_invoice,
                $import_type,
                $import_date,
                $amount,
                $bill_type,
                $status,
                $created_at,
                $updated_at,
                $create_by,
                $update_by,
                $time_lock,
                $agency_id
            );
        return $str_item_sql;
    }

    private function _replaceSpecialCharacter($value){
        $new_value = str_replace("'", "", $value);
        return $new_value;
    }
    private function _processSaveImportData($data_file, $bill_type, $agency_id, $user_id, $description) {
        $upload_folder 	= Upload::BILL_IMPORT_FOLDER;
        $file_extension = $data_file->extension();
        $file_name_old 	= $data_file->getClientOriginalName();
        $file_name_new  = $this->_generateImportFileName() . "." .$file_extension;
        $file_path 		= $upload_folder . $file_name_new;
        $data['status'] = 1;
        $data['file_name'] = $file_name_new;
        $data['file_name_old'] = $file_name_old;
        $data['file_path'] = $file_path;
        $data['create_by'] = $user_id;
        $data['bill_type'] = $bill_type;
        $data['agency_id'] = $agency_id;
        $data['description'] = $description;
        $this->createImport($data);
        // upload video to server
        $this->_upload->upload($data_file, $file_path);
    }

    private function _generateImportFileName() {
        $file_name = sprintf('file_url-%s', strtotime("now"));
        return $file_name;
    }

    public function createImport($data) {
        $import_model = new Import();
        return $import_model->createImport($data);
    }
}