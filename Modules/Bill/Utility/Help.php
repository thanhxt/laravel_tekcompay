<?php


namespace Modules\Bill\Utility;

use Modules\Bill\Entities\Batch;
use Modules\Bill\Entities\BillType;
use Modules\Bill\Entities\CardType;
use Modules\User\Contracts\Authentication;
use Modules\Bill\Entities\Pos;
use Modules\Bill\Entities\Bank;
use Modules\Bill\Entities\Agency;

class Help
{

    /**
     * @var Authentication
     */
    protected $auth;

    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public static function getBillTypeArr() {
        $bill_types = BillType::all();
        $data = [];
        if ($bill_types->count()) {
            foreach ($bill_types as $type) {
                $data[$type->id] = $type->name;
            }
        }

        return $data;
    }

    public static function getAgencyArr() {
        $bill_types = BillType::all();
        $data = [];
        if ($bill_types->count()) {
            foreach ($bill_types as $type) {
                $data[$type->id] = $type->name;
            }
        }

        return $data;
    }



    public static function getImportStatusArr() {
        $data = ['Not Imported','Imported'];
        return $data;
    }

    public static function formatMoney($amount, $thousands_sep = ".", $dec_point = ",") {
        return number_format($amount, 0,$dec_point , $thousands_sep);
    }

    public static function formatDateForDB($date, $type = 'Y-m-d') {
        $date = \Carbon\Carbon::parse($date)->format($type);
        return $date;
    }

    public function formatDateForDisplay($date, $type = 'd-m-Y') {
        $date = \Carbon\Carbon::parse($date)->format($type);
        return $date;
    }

    public function getAgencyByCurrentUser() {
        $agency_id = \Session::get(AGENCY_ID);
        if (!isset($agency_id)) {
            $isAgency = $this->auth->user()->hasRoleSlug('agency');
            if($isAgency) {
                $user = $this->auth->user();
                $agency_id = $user->agency_id;
                \Session::put(AGENCY_ID, $agency_id);
            } else {
                $agency_id = -1;
            }
        }

        return $agency_id;
    }

    public function getPosByAgencyId() {
        $agency_id = $this->getAgencyByCurrentUser();
        $pos_model = new Pos();
        $data_pos = $pos_model->getPosListByAgencyId($agency_id);
        $data = [];
        if ($data_pos->count()) {
            foreach ($data_pos as $item) {
                $data[$item->id] = $item->name;
            }
        }
        return $data;
    }

    public function getBankArr() {
        $bank_data = Bank::all();
        $data = [];
        if ($bank_data->count()) {
            foreach ($bank_data as $item) {
                $data[$item->id] = $item->name;
            }
        }

        return $data;
    }

    public function getCardTypeArr() {
        $card_types = CardType::all();
        $data = [];
        if ($card_types->count()) {
            foreach ($card_types as $item) {
                $data[$item->id] = $item->name;
            }
        }
        return $data;
    }

    public function getBillByBatchId($batch_id) {
        $batch_model = Batch::find($batch_id);
        $data = $batch_model->getBills();
        return $data;
    }

    public function getCurrentUserId() {
        return $this->auth->user()->id;
    }

    public static function getJustSuitableData($arr_keys,$arr_value){
        $new_data = $arr_keys;
        foreach ($arr_value as $k=>$v){
            if(array_key_exists($k,$arr_keys)){
                $new_data[$k] =$v;
            }
        }
        return $new_data;
    }

    public static function getFillableData($arr_keys,$arr_value){
        $new_data = [];
        foreach ($arr_value as $k=>$v){
            if(in_array($k,$arr_keys)){
                $new_data[$k] =$v;
            }
        }
        return $new_data;
    }

    public static function getListPosByBatchId($batch_id) {
        $model = new Batch();
        $data = $model->getListPosByBatchId($batch_id);
        return $data;
    }

    public static function formatCardNumber($number) {
        $first = substr($number,0,4);
        $second = substr($number,4);
        $card_number = $first . "-xxxx-xxxx-" . $second;
        return $card_number;
    }

    public static function getAgencyInformationById($agency_id) {
        $model = Agency::find($agency_id);
        return $model;
    }

    public static function convert_number_to_words( $number )
    {
        $hyphen = ' ';
        $conjunction = '  ';
        $separator = ' ';
        $negative = 'âm ';
        $decimal = ' phẩy ';
        $dictionary = array(
            0 => 'không',
            1 => 'một',
            2 => 'hai',
            3 => 'ba',
            4 => 'bốn',
            5 => 'năm',
            6 => 'sáu',
            7 => 'bảy',
            8 => 'tám',
            9 => 'chín',
            10 => 'mười',
            11 => 'mười một',
            12 => 'mười hai',
            13 => 'mười ba',
            14 => 'mười bốn',
            15 => 'mười năm',
            16 => 'mười sáu',
            17 => 'mười bảy',
            18 => 'mười tám',
            19 => 'mười chín',
            20 => 'hai mươi',
            30 => 'ba mươi',
            40 => 'bốn mươi',
            50 => 'năm mươi',
            60 => 'sáu mươi',
            70 => 'bảy mươi',
            80 => 'tám mươi',
            90 => 'chín mươi',
            100 => 'trăm',
            1000 => 'ngàn',
            1000000 => 'triệu',
            1000000000 => 'tỷ',
            1000000000000 => 'nghìn tỷ',
            1000000000000000 => 'ngàn triệu triệu',
            1000000000000000000 => 'tỷ tỷ'
        );

        if( !is_numeric( $number ) )
        {
            return false;
        }

        if( ($number >= 0 && (int)$number < 0) || (int)$number < 0 - PHP_INT_MAX )
        {
            // overflow
            trigger_error( 'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING );
            return false;
        }

        if( $number < 0 )
        {
            return $negative . self::convert_number_to_words( abs( $number ) );
        }

        $string = $fraction = null;

        if( strpos( $number, '.' ) !== false )
        {
            list( $number, $fraction ) = explode( '.', $number );
        }

        switch (true)
        {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens = ((int)($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if( $units )
                {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if( $remainder )
                {
                    $string .= $conjunction . self::convert_number_to_words( $remainder );
                }
                break;
            default:
                $baseUnit = pow( 1000, floor( log( $number, 1000 ) ) );
                $numBaseUnits = (int)($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = self::convert_number_to_words( $numBaseUnits ) . ' ' . $dictionary[$baseUnit];
                if( $remainder )
                {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= self::convert_number_to_words( $remainder );
                }
                break;
        }

        if( null !== $fraction && is_numeric( $fraction ) )
        {
            $string .= $decimal;
            $words = array( );
            foreach( str_split((string) $fraction) as $number )
            {
                $words[] = $dictionary[$number];
            }
            $string .= implode( ' ', $words );
        }

        return $string;
    }

    public function getBillStatusArr() {
        $data = [
            0 => 'Tất cả',
            1 => 'Chưa sử dụng',
            2 => 'Đã sử dụng',
        ];
        return $data;
    }


}