<?php 
namespace Modules\Bill\Utility;

use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Upload
{
	use DispatchesJobs;

    /**
     * @var Factory
     */
    private $filesystem;

    const BILL_IMPORT_FOLDER 	= "/assets/bill/imports/";

    const BATCH_FOLDER 	= "/assets/batch/";

    public function __construct(Factory $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    public function upload(UploadedFile $file, $file_path) {
        $path = $this->getDestinationPath($file_path);
        // upload if file not exists in path
        if (!$this->filesystem->disk('local')->exists($path)) {
			
            $stream = fopen($file->getRealPath(), 'r+');
            $this->filesystem->disk('local')->writeStream($path, $stream, [
                'visibility' => 'public',
                'mimetype' => $file->getMimeType(),
            ]);
        }
        return $file_path;
	}

    public function remove($file_path) {
        $path = $this->getDestinationPath($file_path);
		return $this->filesystem->disk('local')->delete($path);
	}

    public function uploadBase64($base_46, $path) {
        $destination_path = public_path() . $path;
        $result = \Image::make(file_get_contents($base_46))->save($destination_path);
        return $result;
	}

    public static function removeBatchFile($file_name) {
        $path = Upload::BATCH_FOLDER . $file_name;
        $destination_path = public_path() . $path;
        try {
            $result = unlink($destination_path);
        }catch (\Exception $e) {
            $result = false;
        }
        return $result;
	}

	public static function getBatchPathUrl($file_name) {
        $full_path = Upload::BATCH_FOLDER . $file_name;
        return asset($full_path);
    }

	/**
     * @param string $path
     * @return string
     */
    private function getDestinationPath($path)
    {    
         return basename(public_path()) . $path;
    }
}