<?php

namespace Modules\Bill\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\Bill\Business\BatchBus;
use Modules\Bill\Business\BillBus;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Bill\Utility\Help as HelpBill;
use Modules\Pos\Business\PosBus;
use Modules\Pos\Entities\Pos;
use Modules\Bill\Entities\Bill;
use Modules\Agency\Business\AgencyFeeBus;
use Modules\Bill\Http\Requests\BillRequest;
use Modules\Bill\Entities\Batch;
use Modules\Bill\Entities\BatchPos;
use Modules\Bill\Utility\Upload;

class BillAgencyController extends AdminBaseController
{
    protected $helpBill;

    /**
     * @var Upload
     */
    protected $upload;

    public function __construct(
        HelpBill $helpBill,
        Upload $upload
    ) {
        $this->helpBill = $helpBill;
        $this->upload = $upload;
    }

    public function ajaxGetListMachine()
    {
        $json_data = $this->_getPosJsonData();
        return $json_data;
    }

    public function ajaxFindInvoice(Request $request)
    {
        $find_amount = $request->get('cost');
        $agency_id = $request->get('agency_id');
        $json_data = $this->_getBillJsonData($find_amount, $agency_id);

        return $json_data;
    }
	
    public function ajaxUnblock(Request $request)
    {
        $bill_id = $request->get('id');
        $bas_bus = new BatchBus();
        $bas_bus->unlockTimeForBill($bill_id);
		$data['result'] = true;
        return $this->returnJson($data);
    }
	
    public function ajaxUnblockAll(Request $request)
    {
        $time_lock = $request->get('time_lock');
        $bas_bus = new BatchBus();
		$data['result'] = false;
		if ($time_lock) {
            $bas_bus->unlockAllBillByTimeLock($time_lock);
			$data['result'] = true;
		}
		
        return $this->returnJson($data);
    }

    public function ajaxGetData(Request $request)
    {
        $batch_id = $request->get('id');
        $batch_bus = new BatchBus();
		$model = $batch_bus->getBatchModelById($batch_id);
        $obj = new \stdClass();
        $obj->id = $batch_id;
        $obj->fee = $model->fee;
        $obj->base_amount = $model->base_amount;
        // get list pos data
        $agency_id = $this->helpBill->getAgencyByCurrentUser();
        $pos_bus = new PosBus();
        $data_pos = $pos_bus->getPosListByAgencyId($agency_id);
        $pos_data = [];
        if ($data_pos->count()) {
            $pos_data = $data_pos->toArray();
        }
        $obj->list_machine = $pos_data;
        $bill_machine = $batch_bus->getBatchMachine($batch_id);
        $obj->bill_machine = $bill_machine;
        $obj->bill_detail = $batch_bus->getAchieveBillsByBatchId($batch_id);

		$data['data'] = $obj;
		$data['status'] = true;

        return $this->returnJson($data);
    }

    public function ajaxUploadImage(Request $request)
    {
        $data = $request->all();
        $this->_saveFileBatch($data['data']);

        return $this->returnJson($data);
    }

    public function ajaxRemoveImage(Request $request)
    {
        $data = $request->all();
        $batch_pos_id = $data['id'];
        $bas_bus = new BatchBus();
        $batch_pos_model = $bas_bus->getBatchPosModel($batch_pos_id);
        $file_name = $batch_pos_model->image;
        // remove file link
        Upload::removeBatchFile($file_name);
        $result = $bas_bus->removeImageForBatchPos($batch_pos_id);
        $data['result'] = $result;
        return $this->returnJson($data);
    }

    public function store(BillRequest $request) {
        $data = $request->all();
        $batch_id = isset($data['batch_id']) ? $data['batch_id'] : 0;
        if ($batch_id) {// edit batch information
            $bas_bus = new BatchBus();
            $data['id'] = $batch_id;
            $bas_bus->saveData($data);
        } else {// create new batch
            $bill_bus = new BillBus();
            $agency_id = $data['agency_id'];
            $current_user_id = $this->helpBill->getCurrentUserId();
            $batch_id = $bill_bus->createBill($agency_id, $data, $current_user_id);
        }

        if ($batch_id) {
            // admin.bill.batch.view
            return redirect()->route('admin.bill.batch.view', [$batch_id])
                ->withSuccess(trans('bill::batch.messages.created success'));
        } else {
            return redirect()->route('admin.bill.batch.index')
                ->withSuccess(trans('bill::batch.messages.created fail'));
        }
    }

    public function returnJson($data){
        return response()->json($data);
    }

    private function _getPosJsonData() {
        $data = [];
        $agency_id = $this->helpBill->getAgencyByCurrentUser();
        $pos_bus = new PosBus();
        $data_pos = $pos_bus->getPosListByAgencyId($agency_id);
        if ($data_pos->count()) {
            $data['data'] = $data_pos->toArray();
        }
		$fe_bus = new AgencyFeeBus();
		$fee = $fe_bus->getFeeByAgencyId($agency_id);
		$data['fee'] = $fee;
		$data['agency_id'] = $agency_id;

        return $this->returnJson($data);
    }

    private function _getBillJsonData($find_amount, $agency_id) {
        $data = [];
        $timestamp = strtotime('now');
        $bas_bus = new BatchBus();
        $bill_data = $bas_bus->getPrepareBills($agency_id, $find_amount, $timestamp);
        if ($bill_data->count()) {
            $data['data'] = $bill_data->toArray();
            $data['time_lock'] = $timestamp;
        }

        return $this->returnJson($data);
    }


    private function _saveFileBatch($data) {
        $upload_folder  = $this->_getBatchFolder();
        $file_name      = $data['name'];
        $batch_pos_id   = $data['item_id'];
        $file_extension = pathinfo($file_name, PATHINFO_EXTENSION);
        $file_name      = $batch_pos_id .".". $file_extension;
        $data_file      = $data['src'];
        $file_path      = $upload_folder . $file_name;
        $this->upload->uploadBase64($data_file, $file_path);
        $bas_bus = new BatchBus();
        $result = $bas_bus->saveImageForBatchPos($batch_pos_id, $file_name);
        $data['result'] = $result;
        return $this->returnJson($data);
    }

    private function _getBatchFolder(){
        $soure_path = Upload::BATCH_FOLDER;
        return $soure_path;
    }
}
