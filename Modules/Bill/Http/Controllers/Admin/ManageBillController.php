<?php

namespace Modules\Bill\Http\Controllers\Admin;

use Modules\Bill\Business\BillBus;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Http\Request;

class ManageBillController extends AdminBaseController
{
    public function index()
    {
        $cus_number = isset($_REQUEST['cust_number']) ? $_REQUEST['cust_number'] : '';
        $cus_name = isset($_REQUEST['cust_name']) ? $_REQUEST['cust_name'] : '';
        $amount = isset($_REQUEST['amount']) ? $_REQUEST['amount'] : '';
        $status = isset($_REQUEST['status']) ? $_REQUEST['status'] : '';
        $agency_id = isset($_REQUEST['agency_id']) ? $_REQUEST['agency_id'] : '';

        $bill_bus = new BillBus();
        $bills = $bill_bus->search($cus_number, $cus_name, $amount, $status, $agency_id);

        return view('bill::admin.bill.index', compact('bills'));
    }

    public function edit(Request $request)
    {
        $data = $request->all();
        $id = @$data['id'];
        $bill_bus = new BillBus();
        $model = $bill_bus->getBillModelById($id);
        return view('bill::admin.bill.edit', compact('model'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $bill_bus = new BillBus();
        $bResult = $bill_bus->saveData($data);
        if ($bResult) {
            $msg_path = 'bill::bill.messages.store edit success';
            return redirect()->route('admin.bill.bill.index')
                ->withSuccess(trans($msg_path));
        } else {
            $msg_path = 'bill::bill.messages.store edit fail';
            return redirect()->route('admin.bill.bill.index')
                ->withError(trans($msg_path));
        }
    }

}