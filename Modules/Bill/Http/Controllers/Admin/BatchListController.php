<?php

namespace Modules\Bill\Http\Controllers\Admin;

use Modules\Bill\Business\BatchBus;
use Modules\Bill\Business\BillBus;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Bill\Business\BatchListBus;
use Modules\Bill\Http\Requests\BatchListRequest;
use Modules\User\Contracts\Authentication;
use Modules\Bill\Utility\Help as HelpBill;
use Illuminate\Http\Request;

class BatchListController extends AdminBaseController
{
    protected $total_bill = 0;

    protected $helpBill;

    /**
     * @var Authentication
     */
    protected $auth;

    public function __construct(
        HelpBill $helpBill,
        Authentication $auth
    ) {
        parent::__construct();
        $this->helpBill = $helpBill;
        $this->auth = $auth;
    }

    public function index()
    {
       // $agency_id = $this->_getCurrentAgencyId();
        $current_agency_id = $this->_getCurrentAgencyId();
        if(!$current_agency_id){
            $agency_id = isset($_REQUEST['agency_id']) ? $_REQUEST['agency_id'] : -1;
        }else{
            $agency_id = $current_agency_id;
        }
        $name = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';

        $batch_list_bus = new BatchListBus();
        $list_batches = $batch_list_bus->search($agency_id, $name);

        return view('bill::admin.batch-list.index', compact('list_batches', 'current_agency_id'));
    }

    public function edit(Request $request)
    {
        $data = $request->all();
        $id = @$data['id'];
        $batch_list_bus = new BatchListBus();
        $model = $batch_list_bus->getBatchListModelById($id);
        $current_agency_id = $this->_getCurrentAgencyId();

        return view('bill::admin.batch-list.edit', compact('current_agency_id', 'model'));
    }

    public function store(BatchListRequest $request)
    {
        $data = $request->all();
        $bach_list_bus = new BatchListBus();

        $current_user = $this->auth->id();
        $data['create_by'] = $current_user;
        $bResult = $bach_list_bus->saveData($data);
        if ($bResult) {
            $msg_path = 'bill::batch-list.messages.store success';
            return redirect()->route('admin.bill.batch-list.index')
                ->withSuccess(trans($msg_path));
        } else {
            $msg_path = 'bill::batch-list.messages.store fail';
            return redirect()->back()->withInput($request->input())->withError(trans($msg_path));
        }
    }

    public function view($id)
    {
        $bach_list_bus = new BatchListBus();
        $model = $bach_list_bus->getBatchListModelById($id);
        $batches = $bach_list_bus->getBatchsByBatchListId($id);
        return view('bill::admin.batch-list.view', compact('model', 'batches'));
    }

    public function createBill(Request $request) {
        $data = $request->all();
        $batch_list_id = $data['batch_list_id'];
        $results = $this->_processBill($data);
        if ($results['result']) {
            $msg_path = 'bill::batch-list.messages.createBill success';
            return redirect()->route('admin.bill.batch-list.view', [$batch_list_id])->withSuccess(trans($msg_path));
        } else {
            $msg_path = $results['message'];
            return redirect()->back()->withInput($request->input())->withError($msg_path);
        }
    }

    public function deleteBatch($batch_list_id, $batch_id, $amount)
    {
        $batch_list_bus = new BatchListBus();
        $bResult = $batch_list_bus->deleteBatch($batch_id,$batch_list_id,$amount);
        if ($bResult) {
            return redirect()->route('admin.bill.batch-list.view', [$batch_list_id])
                ->withSuccess(trans('bill::batch-list.messages.deleteBatch success'));
        } else {
            return redirect()->route('admin.bill.batch-list.view', [$batch_list_id])
                ->withError(trans('bill::batch-list.messages.deleteBatch fail'));
        }
    }


    private function _processBill($data) {
        $result['result'] = false;
        $message = "";
        $find_amount = $data['amount'];
        $time_lock = strtotime('now');
        $compare_amount =  floatval($data['amount']) - 1000;

        $data['time_lock'] = $time_lock;
        $data['find_amount'] = $find_amount;
        $data['fee'] = 0;
        $data['is_payment_merge'] = 1;

        $batch_bus = new BatchBus();
        $agency_id = $data['agency_id'];
        $batch_list_id = $data['batch_list_id'];
        $create_by = $this->auth->id();
        $bill_data = $batch_bus->getPrepareBills($agency_id, $find_amount, $time_lock);
        $data['Invoice'] = $this->_createDataInvoice($bill_data);
        $data['base_amount'] = $this->total_bill;

        if($this->total_bill<$compare_amount){
            $batch_bus->unlockAllBillByTimeLock($time_lock);
            $message = trans('bill::batch-list.messages.valid total');
        }else{
            $batch_list_bus = new BatchListBus();
            $batch_list_bus->createBatch($agency_id, $data, $create_by, $batch_list_id,$this->total_bill);
            $result['result'] = true;
        }
        $result['message'] = $message;
        return $result;
    }

    private function _createDataInvoice($bills){
        $data = [];
        $this->total_bill = 0;
        if (count($bills)) {
            $i = 1;
            foreach ($bills as $bill) {
                $item = [];
                $item['id'] = $bill->id;
                $item['amount'] = $bill->amount;
                $this->total_bill += $bill->amount;
                if($i==1){
                    $item['machine_id'] = SYSTEM_POS_ID;
                    $item['batch'] = $item['trace'] = $item['mid']= '';
                }
                $data[] = $item;
                $i++;
            }
        }
        return $data;
    }

    private function _getCurrentAgencyId(){
        $agency_id = $this->helpBill->getAgencyByCurrentUser();
        return $agency_id;
    }

}
