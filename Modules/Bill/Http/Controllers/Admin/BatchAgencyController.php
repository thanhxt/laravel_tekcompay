<?php

namespace Modules\Bill\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Http\Request;
use Modules\Bill\Business\BatchBus;
use Modules\Bill\Http\Requests\ManageRequest;
use Modules\Bill\Utility\Help as HelpBill;

class BatchAgencyController extends AdminBaseController
{
    protected $helpBill;

    public function __construct(HelpBill $helpBill)
    {
        parent::__construct();
        $this->helpBill = $helpBill;
    }

    public function index()
    {
        $current_agency_id = $this->_getCurrentAgencyId();
        if($current_agency_id<0){
            $agency_id = isset($_REQUEST['agency_id']) ? $_REQUEST['agency_id'] : 0;
        }else{
            $agency_id = $current_agency_id;
        }

        $pos_id = isset($_REQUEST['pos_id']) ? $_REQUEST['pos_id'] : '';
        $bank_id = isset($_REQUEST['bank_id']) ? $_REQUEST['bank_id'] : '';
        $created_at = isset($_REQUEST['created_at']) ? $_REQUEST['created_at'] : '';
        $batch_bus = new BatchBus();
        $batches = $batch_bus->search($agency_id, $bank_id, $pos_id, $created_at);


        return view('bill::admin.batch.index', compact('batches', 'current_agency_id'));
    }

    public function create()
    {
        $current_agency_id = $this->_getCurrentAgencyId();
        return view('bill::admin.batch.agency.create', compact('current_agency_id'));
    }

    function _getCurrentAgencyId(){
        $agency_id = $this->helpBill->getAgencyByCurrentUser();
        return $agency_id;
    }

    public function edit($id)
    {
        $batch_bus = new BatchBus();
        $model = $batch_bus->getBatchModelById($id);
        return view('bill::admin.batch.agency.edit', compact('model'));
    }

    public function store(ManageRequest $request)
    {
        $data = $request->all();
        $batch_bus = new BatchBus();
        $bResult = $batch_bus->saveData($data);
        if ($bResult) {
            $msg_path = 'bill::batch.messages.created success';
            if ($data['id']) {
                $msg_path = 'bill::batch.messages.updated success';
            }
            return redirect()->route('admin.bill.batch.index')
                ->withSuccess(trans($msg_path));
        } else {
            $msg_path = 'bill::batch.messages.created fail';
            if ($data['id']) {
                $msg_path = 'bill::batch.messages.updated fail';
            }
            return redirect()->route('admin.bill.batch.index')
                ->withError(trans($msg_path));
        }
    }

    public function delete($id)
    {
        $batch_bus = new BatchBus();
        $bResult = $batch_bus->deleteBatch($id);

        if ($bResult) {
            return redirect()->route('admin.bill.batch.index')
                ->withSuccess(trans('bill::batch.messages.delete success'));
        } else {
            return redirect()->route('admin.bill.batch.index')
                ->withError(trans('bill::batch.messages.delete fail'));
        }
    }

    public function view($id)
    {
        $batch_bus = new BatchBus();
        $model = $batch_bus->getBatchModelById($id);
        return view('bill::admin.batch.view', compact('model'));
    }

    public function printing($id)
    {
        $batch_bus = new BatchBus();
        $model = $batch_bus->getBatchModelById($id);
        return view('bill::admin.batch.print', compact('model'));
    }
}
