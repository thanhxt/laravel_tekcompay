<?php

namespace Modules\Bill\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Bill\Http\Requests\CreateImportRequest;
use Modules\Bill\Utility\Upload;
use Modules\Bill\Business\ImportBus;
use Modules\User\Contracts\Authentication;
use Modules\Bill\Utility\SimpleXLSX;

class ImportController extends AdminBaseController
{
    /**
     * @var Upload
     */
    protected $upload;

    /**
     * @var Authentication
     */
    protected $auth;

    protected $valid_excel_arr = [
        'STT',
        'Mã KHáCH HàNG',
        'TêN KHáCH HàNG',
        'Mã HóA đơN',
        'Lộ TRìNH',
        'Mã GIAO DịCH',
        'Số TIềN',
        'NGàY THANH TOáN',
        'HìNH THứC THANH TOáN'
    ];

    public function __construct(
        Upload $upload,
        Authentication $auth
    ) {
        parent::__construct();
        $this->upload = $upload;
        $this->auth = $auth;
    }

    public function index()
    {
        $file_name = isset($_REQUEST['file_name']) ? $_REQUEST['file_name'] : "";
        $file_name_old = isset($_REQUEST['file_name_old']) ? $_REQUEST['file_name_old'] : "";
        $file_path = isset($_REQUEST['file_path']) ? $_REQUEST['file_path'] : "";
        $agency_id = isset($_REQUEST['agency_id']) ? $_REQUEST['agency_id'] : "";

        $import_bus = new ImportBus($this->upload);
        $imports = $import_bus->search($file_name, $file_name_old, $file_path, $agency_id);
        return view('bill::admin.import.index', compact('imports'));
    }

    public function create() {
        return view('bill::admin.import.create');
    }

    public function store(CreateImportRequest $request)
    {
        // valid file
        $isValid = $this->_validFormat($request);
        // implement import if it is valid
        if($isValid){
            return redirect()->back()->withInput()->withError(trans('bill::import.messages.import file valid'));
        } else {
            $user_id = $this->auth->id();
            $import_bus = new ImportBus($this->upload);
            $result = $import_bus->import($request, $user_id);
            if ($result) {
                return redirect()->route('admin.bill.import.index')
                    ->withSuccess(trans('bill::import.messages.import success'));
            } else {
                return redirect()->route('admin.bill.import.create')
                    ->withError(trans('bill::import.messages.import fail'));
            }
        }

    }

    public function downloadDemo() {
        $file= public_path(). "/assets/bill/invoice_template.xlsx";
        $headers = array(
            'Content-Type: application/vnd.ms-excel',
        );
        return response()->download($file, 'invoice_template.xlsx', $headers);
    }

    private function _validFormat($request){
        $bResult = true;
        $data_file = $request->file('file_import');
        if ($data_file) {
            if ($xlsx = SimpleXLSX::parse($data_file)) {
                $row_data = $xlsx->rows();
                $header_data = $row_data['0'];
                $header_compare = [];
                foreach ($header_data as $item) {
                    $header_compare[] = strtoupper($item);
                }
                if ($header_compare === $this->valid_excel_arr) {
                    $bResult = false;
                }
            }
        }

        return $bResult;
    }

}
