<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/bill'], function (Router $router) {
	// import
    $router->get('import', [
        'as' => 'admin.bill.import.index',
        'uses' => 'ImportController@index',
        'middleware' => 'can:bill.import',
    ]);
	
	$router->get('import/create', [
        'as' => 'admin.bill.import.create',
        'uses' => 'ImportController@create',
        'middleware' => 'can:bill.import',
    ]);
	
	$router->post('import/store', [
        'as' => 'admin.bill.import.store',
        'uses' => 'ImportController@store',
        'middleware' => 'can:bill.import',
    ]);

    $router->get('import/downloadDemo', [
        'as' => 'admin.bill.import.downloadDemo',
        'uses' => 'ImportController@downloadDemo',
        'middleware' => 'can:bill.import',
    ]);
    // batch
    $router->get('batch/index', [
        'as' => 'admin.bill.batch.index',
        'uses' => 'BatchAgencyController@index',
        'middleware' => 'can:bill.batch.all',
    ]);

    $router->get('batch/create', [
        'as' => 'admin.bill.batch.create',
        'uses' => 'BatchAgencyController@create',
        'middleware' => 'can:bill.batch.all',
    ]);

    $router->get('batch/edit/{id}', [
        'as' => 'admin.bill.batch.edit',
        'uses' => 'BatchAgencyController@edit',
        'middleware' => 'can:bill.batch.all',
    ]);

    $router->post('batch/store', [
        'as' => 'admin.bill.batch.store',
        'uses' => 'BatchAgencyController@store',
        'middleware' => 'can:bill.batch.all',
    ]);

    $router->get('batch/view/{id}', [
        'as' => 'admin.bill.batch.view',
        'uses' => 'BatchAgencyController@view',
        'middleware' => 'can:bill.batch.all',
    ]);

    $router->get('batch/print/{id}', [
        'as' => 'admin.bill.batch.print',
        'uses' => 'BatchAgencyController@printing',
        'middleware' => 'can:bill.batch.all',
    ]);

    $router->delete('batch/delete/{id}', [
        'as' => 'admin.bill.batch.delete',
        'uses' => 'BatchAgencyController@delete',
        'middleware' => 'can:bill.batch.all',
    ]);

    /* AJAX get list machine */
    $router->get('bill/get-list-machine', [
        'as' => 'admin.bill.bill.get-list-machine',
        'uses' => 'BillAgencyController@ajaxGetListMachine',
        'middleware' => 'can:bill.batch.all',
    ]);
    /* AJAX find invoice */
    $router->post('bill/find-invoice', [
        'as' => 'admin.bill.bill.find-invoice',
        'uses' => 'BillAgencyController@ajaxFindInvoice',
        'middleware' => 'can:bill.batch.all',
    ]);
	
    /* AJAX unblock */
    $router->post('bill/unblock', [
        'as' => 'admin.bill.bill.unblock',
        'uses' => 'BillAgencyController@ajaxUnblock',
        'middleware' => 'can:bill.batch.all',
    ]);
	
    /* AJAX unblock */
    $router->post('bill/unblock-all', [
        'as' => 'admin.bill.bill.unblock-all',
        'uses' => 'BillAgencyController@ajaxUnblockAll',
        'middleware' => 'can:bill.batch.all',
    ]);

    /* AJAX get-data */
    $router->get('bill/get-data', [
        'as' => 'admin.bill.bill.get-data',
        'uses' => 'BillAgencyController@ajaxGetData',
        'middleware' => 'can:bill.batch.all',
    ]);
    /* AJAX upload file */
    $router->post('bill/upload-image', [
        'as' => 'admin.bill.bill.upload-image',
        'uses' => 'BillAgencyController@ajaxUploadImage',
        'middleware' => 'can:bill.batch.all',
    ]);
    /* AJAX upload file */
    $router->post('bill/remove-image', [
        'as' => 'admin.bill.bill.remove-image',
        'uses' => 'BillAgencyController@ajaxRemoveImage',
        'middleware' => 'can:bill.batch.all',
    ]);

    $router->post('bill/store', [
        'as' => 'admin.bill.bill.store',
        'uses' => 'BillAgencyController@store',
        'middleware' => 'can:bill.batch.all',
    ]);

    /* Batch list manage */
    $router->get('batch-list/index', [
        'as' => 'admin.bill.batch-list.index',
        'uses' => 'BatchListController@index',
        'middleware' => 'can:bill.batch.all',
    ]);

    $router->get('batch-list/create', [
        'as' => 'admin.bill.batch-list.create',
        'uses' => 'BatchListController@edit',
        'middleware' => 'can:bill.batch.all',
    ]);

    $router->get('batch-list/edit', [
        'as' => 'admin.bill.batch-list.edit',
        'uses' => 'BatchListController@edit',
        'middleware' => 'can:bill.batch.all',
    ]);

    $router->post('batch-list/store', [
        'as' => 'admin.bill.batch-list.store',
        'uses' => 'BatchListController@store',
        'middleware' => 'can:bill.batch.all',
    ]);

    $router->get('batch-list/view/{id}', [
        'as' => 'admin.bill.batch-list.view',
        'uses' => 'BatchListController@view',
        'middleware' => 'can:bill.batch.all',
    ]);

    $router->post('batch-list/create-bill', [
        'as' => 'admin.bill.batch-list.create-bill',
        'uses' => 'BatchListController@createBill',
        'middleware' => 'can:bill.batch.all',
    ]);

    $router->delete('batch-list/delete-batch/{batch_list_id}/{batch_id}/{amount}', [
        'as' => 'admin.bill.batch-list.delete-batch',
        'uses' => 'BatchListController@deleteBatch',
        'middleware' => 'can:bill.batch.all',
    ]);

    // bill manage
    $router->get('bill/index', [
        'as' => 'admin.bill.bill.index',
        'uses' => 'ManageBillController@index',
        'middleware' => 'can:bill.batch.all',
    ]);

    $router->get('bill/edit', [
        'as' => 'admin.bill.bill.edit',
        'uses' => 'ManageBillController@edit',
        'middleware' => 'can:bill.batch.all',
    ]);

    $router->post('bill/edit/store', [
        'as' => 'admin.bill.bill.edit.store',
        'uses' => 'ManageBillController@store',
        'middleware' => 'can:bill.batch.all',
    ]);

});
