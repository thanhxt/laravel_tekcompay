<?php

namespace Modules\Bill\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BatchListRequest extends FormRequest
{
    public function rules()
    {
        $agency_id = request()->get('agency_id');
        $name_unique = 'required|unique:batchlist,name,NULL,id,agency_id,' . $agency_id;

        return [
            'name' => $name_unique,
            'agency_id' => 'required',
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'name.unique' => trans('bill::batch-list.messages.valid name unique'),
            'agency_id.required' => trans('bill::batch-list.messages.valid agency required')
        ];
    }
}