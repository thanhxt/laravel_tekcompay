<?php

namespace Modules\Bill\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateImportRequest extends FormRequest
{
    public function rules()
    {
        return [
            /*'file_import' => 'required',
            'bill_type' => 'required',
            'agency_id' => 'required'*/
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }
}