<?php

namespace Modules\Bill\Entities;

use Illuminate\Database\Eloquent\Model;

class Pos extends Model
{
    protected $table = 'pos';

    protected $fillable = [
        'name',
        'code',
        'status',
        'agency_id',
        'branch_name',
        'branch_address',
        'create_by',
        'edit_by',
        'spend',
        'balance',
        'quota',
        'visible',
    ];

    public function search($name, $code, $agency_id, $spend_from, $spend_to, $balance_from, $balance_to, $pageSize = PAGE_SIZE)
    {

        $model = self::where('visible', '=', 1);

        if ($name != '') {
            $search_name = '%' . $name . '%';
            $model->where('name', 'like', $search_name);
        }

        if ($code != '') {
            $search_code = '%' . $code . '%';
            $model->where('code', 'like', $search_code);
        }

        if ($agency_id > 0) {
            $model->where('agency_id', '=', $agency_id);
        }

        if ($spend_from > 0) {
            $model->where('spend', '>=', $spend_from);
        }

        if ($spend_to > 0) {
            $model->where('spend', '<=', $spend_to);
        }

        if ($balance_from > 0) {
            $model->where('balance', '>=', $balance_from);
        }

        if ($balance_to > 0) {
            $model->where('balance', '<=', $balance_to);
        }

        $result = $model->paginate($pageSize);
        return $result;
    }

    public function saveData($data)
    {
        $data['visible'] = 1;
        $model = self::fill($data);
        $bResult = $model->save();
        return $bResult;
    }

    public function getPosListByAgencyId($agency_id = 0, $status = 1) {
        $model = Pos::where('status', '=', $status);
        if ($agency_id > 0) {
            $model->where('agency_id', '=', $agency_id);
        }

        $model->where('visible', '=', 1);

        $result = $model->get();
        return $result;
    }
}
