<?php

namespace Modules\Bill\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Bill\Entities\Batch;
use Modules\Bill\Entities\BatchPos;
use Modules\Bill\Entities\BillPos;
use Modules\Pos\Entities\Pos;

class Bill extends Model
{
    protected $table = 'bills';

    protected $fillable = [
        'pay_date',
        'cust_number',
        'cust_name',
        'import_type',
        'import_date',
        'rec_invoice',
        'description',
        'amount',
        'bill_type',
        'status',
        'create_by',
        'update_by',
        'agency_id',
        'time_lock',
    ];

    public $translatedAttributes = [];

	public function search($cus_number, $cus_name, $amount, $status, $agency_id, $pageSize = PAGE_SIZE)
    {
        $model = self::where('bills.id', '>', 0);

        $model->select([
            'bills.id',
            'bills.pay_date',
            'bills.cust_number',
            'bills.cust_name',
            'bills.import_type',
            'bills.import_date',
            'bills.recurring_invoice',
            'bills.description',
            'bills.amount',
            'bills.bill_type',
            'bills.status',
            'bills.created_at',
            'bills.updated_at',
            'bills.create_by',
            'bills.update_by',
            'bills.time_lock',
            'bills.agency_id',
            'agencies.name as agency_name'
        ]);

        $model->join('agencies', function ($join) {
            $join->on('bills.agency_id', '=', 'agencies.id');
        });

        if ($cus_number != '') {
            $cus_number = '%' . $cus_number . '%';
            $model->where('cust_number', 'like', $cus_number);
        }

        if ($cus_name != '') {
            $cus_name = '%' . $cus_name . '%';
            $model->where('cust_name', 'like', $cus_name);
        }

        if ($amount > 0) {
            $model->where('amount', '<=', $amount);
        }

        if ($status) {
            if($status==2){
                $model->where('time_lock', '>', '0');
            }else{
                $model->where('time_lock', '=', '0');
            }
        }

        if ($agency_id > 0) {
            $model->where('agency_id', '=', $agency_id);
        }

        return $model->paginate($pageSize);
    }


    public function saveData($data)
    {
        $model = self::fill($data);
        $bResult = $model->save();
        return $bResult;
    }

    public function createFromImport($data) {
        return self::create($data);
    }

}
