<?php

namespace Modules\Bill\Entities;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{

    protected $table = 'agencies';

    protected $fillable = [
        'code',
        'name',
        'contact_name',
        'phone',
        'type_id',
        'visible',
        'address',
        'description',
    ];

    public $timestamps = false;

    public function search($name, $code, $contact_name, $phone, $pageSize = PAGE_SIZE)
    {
        $model = self::where('id', '>', 0);

        if ($name != '') {
            $search_name = '%' . $name . '%';
            $model->where('name', 'like', $search_name);
        }

        if ($code != '') {
            $search_code = '%' . $code . '%';
            $model->where('code', 'like', $search_code);
        }

        if ($contact_name != '') {
            $search_contact_name = '%' . $contact_name . '%';
            $model->where('contact_name', 'like', $search_contact_name);
        }

        if ($phone != '') {
            $search_phone = '%' . $phone . '%';
            $model->where('phone', 'like', $search_phone);
        }

        return $model->paginate($pageSize);
    }

    public function saveData($data)
    {
        $model = self::fill($data);
        $bResult = $model->save();
        return $bResult;
    }
}
