<?php

namespace Modules\Bill\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Bill\Utility\Help as HelpBill;

class BillPos extends Model
{

    protected $table = 'billpos';

    protected $fillable = [
        'batch_pos_id',
        'bill_id',
        'amount',
    ];

    public $timestamps = false;
}
