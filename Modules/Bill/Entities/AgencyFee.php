<?php

namespace Modules\Bill\Entities;

use Illuminate\Database\Eloquent\Model;

class AgencyFee extends Model
{
    protected $table = 'agencyfees';

    protected $fillable = [
        'agency_id',
        'fee',
        'fee_name',
        'date_effect',
        'status',
    ];

    public $timestamps = false;

    public function search($agency_name, $fee_name, $pageSize = PAGE_SIZE)
    {
        $model = self::where('agencyfees.id', '>', 0);

        if ($agency_name != '') {
            $search_agency_name = '%' . $agency_name . '%';
            $model->join('agencies', function ($join) use ($search_agency_name) {
                $join->on('agencyfees.agency_id', '=', 'agencies.id')
                    ->where('name', 'like', $search_agency_name);
            });
        }

        if ($fee_name != '') {
            $search_fee_name = '%' . $fee_name . '%';
            $model->where('fee_name', 'like', $search_fee_name);
        }


        return $model->paginate($pageSize);
    }

    public function saveData($data)
    {
        $shop = self::fill($data);
        $bResult = $shop->save();
        return $bResult;
    }
}
