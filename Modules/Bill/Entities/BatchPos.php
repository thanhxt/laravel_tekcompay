<?php

namespace Modules\Bill\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Bill\Utility\Help as HelpBill;

class BatchPos extends Model
{

    protected $table = 'batchpos';

    protected $fillable = [
        'batch_id',
        'pos_id',
        'transaction_code',
        'transaction_batch',
        'transaction_mid',
        'image',
    ];

    public function saveImage($image = '') {
        $this->image = $image;
        return $this->save();
    }
}
