<?php

namespace Modules\Bill\Entities;

use Illuminate\Database\Eloquent\Model;

class AgencyType extends Model
{
    protected $table = 'agencytypes';

    protected $fillable = [
        'name',
    ];

}
