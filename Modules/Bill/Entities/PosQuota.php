<?php

namespace Modules\Bill\Entities;

use Illuminate\Database\Eloquent\Model;

class PosQuota extends Model
{

    protected $table = 'posquota';

    protected $fillable = [
        'amount',
        'date_effect',
        'pos_id',
        'create_by',
        'edit_by',
    ];

    public function search($agency_id = 0, $quota_from, $quota_to, $update_from, $update_to, $pos_name, $pageSize = PAGE_SIZE)
    {
        $model = self::where('posquota.id', '>', 0)->select(
                                [
                                    'posquota.id',
                                    'posquota.amount',
                                    'posquota.date_effect',
                                    'posquota.pos_id',
                                    'posquota.create_by',
                                    'posquota.edit_by',
                                    'pos.name',
                                ]
        );

        $model->join('pos', function ($join) use ($agency_id, $pos_name) {
            $join->on('posquota.pos_id', '=', 'pos.id');
            if ($agency_id > 0) {
                $join->where('pos.agency_id', '=', $agency_id);
            }

            if ($pos_name != '') {
                $search_name = '%' . $pos_name . '%';
                $join->where('name', 'like', $search_name);
            }
        });

        if ($quota_from > 0) {
            $model->where('amount', '>=', $quota_from);
        }

        if ($quota_to > 0) {
            $model->where('amount', '<=', $quota_to);
        }

        if ($update_from != '') {
            $model->where('pos.updated_at', '>=', $update_from);
        }

        if ($update_to > 0) {
            $model->where('pos.updated_at', '<=', $update_to);
        }

        return $model->paginate($pageSize);
    }
}
