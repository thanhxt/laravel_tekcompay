<?php

namespace Modules\Bill\Entities;

use Illuminate\Database\Eloquent\Model;

class CardType extends Model
{

    protected $table = 'cardtypes';

    protected $fillable = ['name'];

    public $translatedAttributes = [];


}
