<?php

namespace Modules\Bill\Entities;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Modules\User\Entities\Sentinel\User;

class Member extends Model
{

    const SUPER_ADMIN_ROLE = 'super-admin';

    /**
     * @var \Cartalyst\Sentinel\Roles\EloquentRole
     */
    protected $role;

    protected $table = 'users';

    protected $fillable = ['email', 'password', 'first_name', 'last_name', 'status', 'phone', 'website', 'position', 'avarta', 'description', 'agency_id'];

    public $translatedAttributes = ['email', 'password', 'first_name', 'last_name', 'status', 'phone', 'website', 'position', 'avarta', 'description', 'agency_id'];


    public function __construct()
    {
        $this->role = Sentinel::getRoleRepository()->createModel();
    }

    public function search($name, $email, $pageSize = PAGE_SIZE)
    {
        $model = \DB::table('users')->select('users.id', 'users.first_name', 'users.last_name', 'users.email', 'users.created_at')
            ->join('role_users', 'role_users.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'role_users.role_id')
            ->whereNotIn('roles.slug', [self::SUPER_ADMIN_ROLE])
            ->groupBy('users.id', 'users.first_name', 'users.last_name', 'users.email', 'users.created_at');

        if ($name != '') {
            $search_name = '%' . $name . '%';
            $model->where('first_name', 'like', $search_name)->orWhere('last_name', 'like', $search_name);
        }

        if ($email != '') {
            $search_email = '%' . $email . '%';
            $model->where('email', 'like', $search_email);
        }

        $model = $model->paginate($pageSize);

        return $model;
    }

    public function getRoleOfUser($user_id)
    {
        $sql = "
                SELECT roles.name 
                FROM role_users 
                INNER JOIN roles ON roles.id = role_users.role_id
                WHERE 
                  role_users.user_id = " . $user_id;

        $roles = \DB::select($sql);

        return $roles;
    }

    public function getListMemberRole()
    {
        $data_roles = $this->role->where('slug', '<>', self::SUPER_ADMIN_ROLE)->get();
        $data = [];
        if (count($data_roles)) {
            foreach ($data_roles as $item) {
                $data[$item->id] = $item->name;
            }
        }
        return $data;
    }

    public function saveMember($user, $data)
    {
        $data['last_name'] = $data['first_name'];
        $roleData = array($data['roles']);
        //$roleData = array_values($roleData[0]);
        if (isset($data['member_id'])) {
            $user->updateAndSyncRoles($data['member_id'], $data, $roleData[0]);
        } else {
            $user->createWithRoles($data, $roleData[0], true);
        }
    }
}
