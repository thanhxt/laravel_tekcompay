<?php

namespace Modules\Bill\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Bill\Utility\Help as HelpBill;
use Modules\Bill\Utility\Upload;

class BatchList extends Model
{

    protected $table = 'batchlist';

    protected $fillable = [
        'name',
        'description',
        'status',
        'total',
        'create_by',
        'agency_id',
    ];

    public function search($agency_id, $name, $pageSize = PAGE_SIZE)
    {
        $model = self::where('id', '>', 0);
					
        if ($agency_id > 0 ) {
            $model->where('agency_id', '=', $agency_id);
        }

        if ($name != '') {
            $search_name = '%' . $name . '%';
            $model->where('name', 'like', $search_name);
        }
        $model->orderBy('id', 'DESC');
        return $model->paginate($pageSize);
    }

    public function saveData($data)
    {
        $model = self::fill($data);
        $bResult = $model->save();
        return $bResult;
    }


}
