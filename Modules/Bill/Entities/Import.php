<?php

namespace Modules\Bill\Entities;

use Illuminate\Database\Eloquent\Model;

class Import extends Model
{

    protected $table = 'imports';

    protected $fillable = [
        'file_name',
        'file_name_old',
        'file_path',
        'bill_type',
        'agency_id',
        'description',
        'status',
        'create_by'
    ];

    public $translatedAttributes = [];

    public function search($file_name, $file_name_old, $file_path, $agency_id, $paging = PAGE_SIZE) {

        $model = Import::where('imports.id', '>', 0)
                        ->select([
                                    'imports.id',
                                    'imports.file_name',
                                    'imports.file_name_old',
                                    'imports.file_path',
                                    'imports.bill_type',
                                    'imports.description',
                                    'imports.status',
                                    'imports.create_by',
                                    'agencies.name',
                                ]
            );

        $model->join('agencies', function ($join) {
            $join->on('imports.agency_id', '=', 'agencies.id');
        });

        if ($file_name != '') {
            $search_file_name = '%' . $file_name . '%';
            $model->where('file_name', 'like', $search_file_name);
        }

        if ($file_name_old != '') {
            $search_file_name_old = '%' . $file_name_old . '%';
            $model->where('file_name_old', 'like', $search_file_name_old);
        }

        if ($file_path != '') {
            $search_file_path = '%' . $file_path . '%';
            $model->where('file_path', 'like', $search_file_path);
        }

        if($agency_id != ''){
            $model->where('agency_id', '=', $agency_id);
        }

        return $model->paginate($paging);
    }

    public function createImport($data) {
        return self::create($data);
    }

}
