<?php

namespace Modules\Bill\Entities;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'banks';

    protected $fillable = [
        'code',
        'name',
        'status',
        'create_by',
        'edit_by',
    ];

    public function search($name, $code, $pageSize = PAGE_SIZE)
    {
        $model = self::where('id', '>', 0);

        if ($name != '') {
            $search_name = '%' . $name . '%';
            $model->where('name', 'like', $search_name);
        }

        if ($code != '') {
            $search_code = '%' . $code . '%';
            $model->where('code', 'like', $search_code);
        }

        return $model->paginate($pageSize);
    }

    public function saveData($data)
    {
        $model = self::fill($data);
        $bResult = $model->save();
        return $bResult;
    }
}
