<?php

namespace Modules\Bill\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Bill\Utility\Help as HelpBill;
use Modules\Bill\Utility\Upload;

class Batch extends Model
{

    protected $table = 'batch';

    protected $fillable = [
        'batch_list_id',
        'cust_name',
        'cust_card_name',
        'cust_phone',
        'base_amount',
        'amount',
        'find_amount',
        'status',
        'fee',
        'fee_amount',
        'create_by',
        'identify_card',
        'bank_id',
        'card_type',
        'card_number',
        'agency_id',
        'pos_id',
        'is_payment_merge',
        'created_at',
        'updated_at',
        'time_lock',
    ];

    public $timestamps = false;

    public function search($agency_id, $bank_id = 0, $pos_id = 0, $created_at, $pageSize = PAGE_SIZE)
    {
        $model = self::where('batch.id', '>', 0)
                    ->select([
                        'batch.id',
                        'batch.cust_name',
                        'batch.cust_card_name',
                        'batch.cust_phone',
                        'batch.base_amount',
                        'batch.amount',
                        'batch.fee',
                        'batch.fee_amount',
                        'batch.create_by',
                        'batch.identify_card',
                        'batch.bank_id',
                        'batch.card_type',
                        'batch.card_number',
                        'batch.agency_id',
                        'batch.pos_id',
                        'banks.name as bank_name',
                        'users.first_name as staff_name',
                    ]);


        $model->join('banks', function ($join) use ($bank_id) {
            $join->on('batch.bank_id', '=', 'banks.id');
            if ($bank_id > 0) {
                $join->where('batch.bank_id', '=', $bank_id);
            }

        });

        if($pos_id){
            $sql_pos ='batch.id in (select batch_id From batchpos where pos_id=' . $pos_id . ')';
            $model->whereRaw(\DB::raw($sql_pos));
        }

        $model->join('users', function ($join) {
            $join->on('batch.create_by', '=', 'users.id');
        });

        if ($agency_id >= 0 ) {
            $model->where('batch.agency_id', '=', $agency_id);
        }

        if ($created_at != '') {
            $start_date = strtotime($created_at);
            $end_date = strtotime($created_at. ' + 1 days');

            $model->where('batch.created_at', '>=', $start_date);
            $model->where('batch.created_at', '<=', $end_date);
        }

        return $model->paginate($pageSize);
    }

    public function saveData($data)
    {
        $model = self::fill($data);
        $bResult = $model->save();
        return $bResult;
    }

    public function getListPosByBatchId($batch_id = 0) {

        $batch_machine_table =  'batchpos as batch_machine';
        $model = \DB::table($batch_machine_table)->where('batch_id', '=', $batch_id);

        $model->join('pos', function ($join) {
            $join->on('batch_machine.pos_id', '=', 'pos.id');
        });
        $data = $model->select(
                                [
                                    'batch_machine.pos_id',
                                    'pos.name as pos_name',
                                    'batch_machine.transaction_code',
                                    'batch_machine.transaction_batch',
                                    'batch_machine.transaction_mid',
                                ]
        )->get();

        return $data;
    }

    public function getAchieveBill() {
        $batch_id = $this->id;
        $model = \DB::table('billachieves')->where('batch_id', '=', $batch_id);

        $model->join('billpos', function ($join) {
            $join->on('billachieves.id', '=', 'billpos.bill_id');
        });

        $data = $model->select(
            [
                'billachieves.id',
                'billachieves.pay_date',
                'billachieves.cust_number',
                'billachieves.cust_name',
                'billachieves.import_type',
                'billachieves.import_date',
                'billachieves.recurring_invoice',
                'billachieves.description',
                'billachieves.amount',
                'billachieves.bill_type',
                'billachieves.status',
                'billachieves.created_at',
                'billachieves.updated_at',
                'billachieves.create_by',
                'billachieves.update_by',
                'billachieves.time_lock',
                'billachieves.agency_id',
                'billachieves.batch_id',
                'billpos.batch_pos_id',
            ]
        )->get();

        return $data;
    }

    public function getBatchsBatchListId($batch_list_id = 0, $pageSize = -1)
    {
        $model = self::where('batch.id', '>', 0)
            ->select([
                'batch.id',
                'batch.batch_list_id',
                'batch.cust_name',
                'batch.cust_card_name',
                'batch.cust_phone',
                'batch.base_amount',
                'batch.amount',
                'batch.fee',
                'batch.fee_amount',
                'batch.create_by',
                'batch.identify_card',
                'batch.bank_id',
                'batch.card_type',
                'cardtypes.name as card_name',
                'batch.card_number',
                'batch.agency_id',
                'batch.pos_id',
                'banks.name as bank_name',
            ]);


        $model->join('banks', function ($join){
            $join->on('batch.bank_id', '=', 'banks.id');

        });

        $model->join('cardtypes', function ($join){
            $join->on('batch.card_type', '=', 'cardtypes.id');

        });

        $model->where('batch.batch_list_id', '=', $batch_list_id);

        $model->orderBy('id', 'DESC');

        return $model->get($pageSize);
    }

    public function getBills(){
        $batch_id = $this->id ;
        $time_lock = $this->time_lock;
        $orders = \DB::table('bills');
        $orders->select([
            'bills.id',
            'bills.pay_date',
            'bills.cust_number',
            'bills.cust_name',
            'bills.import_type',
            'bills.import_date',
            'bills.recurring_invoice',
            'bills.description',
            'bills.amount',
            'bills.bill_type',
            'bills.status',
            'bills.created_at',
            'bills.updated_at',
            'bills.create_by',
            'bills.update_by',
            'bills.time_lock',
            'bills.agency_id'
        ]);
        $orders->where('bills.time_lock','=',$time_lock);
        $data = $orders->orderBy('bills.amount', 'desc')->get();

        return $data;
    }

}
