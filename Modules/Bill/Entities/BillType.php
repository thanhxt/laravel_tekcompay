<?php

namespace Modules\Bill\Entities;

use Illuminate\Database\Eloquent\Model;

class BillType extends Model
{

    protected $table = 'billtypes';

    protected $fillable = ['name'];

    public $translatedAttributes = [];


}
