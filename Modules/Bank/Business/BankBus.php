<?php

namespace Modules\Bank\Business;

use Modules\Bill\Entities\Bank;

class BankBus
{
    public function search($name, $code, $pageSize = PAGE_SIZE)
    {
        $bank_model = new Bank();
        $data = $bank_model->search($name, $code, $pageSize);
        return $data;
    }

    public function saveData($data)
    {
        $id = $data["id"];
        $bank_model = $this->getBankModelById($id);
        return $bank_model->saveData($data);
    }

    public function getBankModelById($id) {
        if($id){
            $model = Bank::find($id);
        }else{
            $model = new Bank();
        }
        return $model;
    }

    public function delete($id) {
        $result = Bank::destroy($id);
        return $result;
    }
}