<?php

namespace Modules\Bank\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterBankSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }
    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        /*$menu->group(trans('member::members.title.system'), function (Group $group) {
            $group->weight(902);
            $group->item(trans('bank::manage.title.list'), function (Item $item) {
                $item->weight(3);
                $item->icon('fa fa-users');
                $item->authorize(
                    $this->auth->hasAccess('bank.manage.all')
                );

                $item->item(trans('bank::manage.title.list'), function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-user');
                    $item->route('admin.bank.manage.index');
                    $item->authorize(
                        $this->auth->hasAccess('bank.manage.all')
                    );
                });

            });
        });*/

        return $menu;
    }
}
