<?php

namespace Modules\Bank\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BankRequest extends FormRequest
{
    public function rules()
    {
        $id = request()->get('id');
        $code_unique = $name_unique = 'required|unique:banks';
        if ($id) {
            $code_unique = $code_unique . ',code,' . $id;
            $name_unique = $name_unique . ',name,' . $id;
        }

        return [
            'code' => $code_unique,
            'name' => $name_unique,
            'status' => 'required',
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'required' => trans('bank::manage.messages.valid required'),
            'code.unique' => trans('bank::manage.messages.valid code unique'),
            'name.unique' => trans('bank::manage.messages.valid name unique'),
        ];
    }
}
