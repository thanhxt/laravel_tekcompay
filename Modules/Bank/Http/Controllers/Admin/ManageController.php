<?php

namespace Modules\Bank\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Http\Request;
use Modules\Bank\Business\BankBus;
use Modules\Bank\Http\Requests\BankRequest;
use Modules\User\Contracts\Authentication;

class ManageController extends AdminBaseController
{
    /**
     * @var Authentication
     */
    protected $auth;

    public function __construct(
        Authentication $auth
    ) {
        parent::__construct();
        $this->auth = $auth;
    }

    public function index()
    {
        $name = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
        $code = isset($_REQUEST['code']) ? $_REQUEST['code'] : '';
        $bank_bus = new BankBus();
        $banks = $bank_bus->search($name, $code);

        return view('bank::admin.manage.index', compact('banks'));
    }

    public function edit(Request $request)
    {
        $data = $request->all();
        $id = @$data['id'];
        $bank_bus = new BankBus();
        $model = $bank_bus->getBankModelById($id);
        return view('bank::admin.manage.edit', compact('model'));
    }

    public function store(BankRequest $request)
    {
        $data = $request->all();
        $bank_bus = new BankBus();
        $current_id = $this->auth->id();
        if ($data['id']) {
            $data['edit_by'] = $current_id;
        } else {
            $data['edit_by'] = $current_id;
            $data['create_by'] = $current_id;
        }
        $bResult = $bank_bus->saveData($data);
        if ($bResult) {
            $msg_path = 'bank::manage.messages.created success';
            if ($data['id']) {
                $msg_path = 'bank::manage.messages.updated success';
            }
            return redirect()->route('admin.bank.manage.index')
                ->withSuccess(trans($msg_path));
        } else {
            $msg_path = 'bank::manage.messages.created fail';
            if ($data['id']) {
                $msg_path = 'bank::manage.messages.updated fail';
            }
            return redirect()->route('admin.bank.manage.index')
                ->withError(trans($msg_path));
        }
    }

    public function delete($id)
    {
        $bank_bus = new BankBus();
        $bResult = $bank_bus->delete($id);

        if ($bResult) {
            return redirect()->route('admin.bank.manage.index')
                ->withSuccess(trans('bank::manage.messages.delete success'));
        } else {
            return redirect()->route('admin.bank.manage.index')
                ->withError(trans('bank::manage.messages.delete fail'));
        }
    }

    public function view($id)
    {
        $bank_bus = new BankBus();
        $model = $bank_bus->getBankModelById($id);
        return view('bank::admin.manage.view', compact('model'));
    }

}
