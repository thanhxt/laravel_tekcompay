<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/bank'], function (Router $router) {
    // manage agency
    $router->get('manage/index', [
        'as' => 'admin.bank.manage.index',
        'uses' => 'ManageController@index',
        'middleware' => 'can:bank.manage.all',
    ]);

    $router->get('manage/edit', [
        'as' => 'admin.bank.manage.edit',
        'uses' => 'ManageController@edit',
        'middleware' => 'can:bank.manage.all',
    ]);

    $router->post('manage/store', [
        'as' => 'admin.bank.manage.store',
        'uses' => 'ManageController@store',
        'middleware' => 'can:bank.manage.all',
    ]);

    $router->delete('manage/delete/{id}', [
        'as' => 'admin.bank.manage.delete',
        'uses' => 'ManageController@delete',
        'middleware' => 'can:bank.manage.all',
    ]);

    $router->get('manage/view/{id}', [
        'as' => 'admin.bank.manage.view',
        'uses' => 'ManageController@view',
        'middleware' => 'can:bank.manage.all',
    ]);

});
