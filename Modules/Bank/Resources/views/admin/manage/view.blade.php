@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('bank::manage.title.view bank') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ URL::route('admin.bank.manage.index') }}">{{ trans('bank::manage.title.view bank') }}</a></li>
        <li class="active">{{ trans('bank::manage.title.view bank') }}</li>
    </ol>
@stop

@section('content')
@inject('help','Modules\Agency\Utility\Help')
@inject('helpMember','Modules\Member\Utility\Help')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-danger">
				<div class="box-header">
					<a class="btn btn-success" href="{{ route('admin.bank.manage.edit', ['id' => $model->id]) }}"><i class="fa fa-pencil"></i> {{ trans('agency::manage.button.edit') }}</a>
				</div>
				<div class="box-body">
					<table class='table table-striped table-bordered detail-view'>
						<tr>
							<td width='25%'>Id</td>
							<td>{{ $model->id }}</td>
						</tr>
						<tr>
							<td width='25%'>{{ trans('bank::manage.table.name') }}</td>
							<td>{{ $model->name }}</td>
						</tr>
						<tr>
							<td width='25%'>{{ trans('bank::manage.table.code') }}</td>
							<td>{{ $model->code }}</td>
						</tr>
						<tr>
							<td width='25%'>{{ trans('bank::manage.table.status') }}</td>
							<td>{{ $model->status }}</td>
						</tr>
						<tr>
							<?php 
								$created_at = $help->formatDate($model->created_at);
								$updated_at = $help->formatDate($model->updated_at);
							?>
							<td width='25%'>{{ trans('bank::manage.table.created_at') }}</td>
							<td>{{ $created_at }}</td>
						</tr>
						<tr>
							<td width='25%'>{{ trans('bank::manage.table.updated_at') }}</td>
							<td>{{ $updated_at }}</td>
						</tr>
						<tr>
							<?php 
								$create_by = $helpMember->getUserNameById($model->create_by);
								$edit_by = $helpMember->getUserNameById($model->edit_by);
							?>
							<td width='25%'>{{ trans('bank::manage.table.create_by') }}</td>
							<td>{{ $create_by }}</td>
						</tr>
						<tr>
							<td width='25%'>{{ trans('bank::manage.table.edit_by') }}</td>
							<td>{{ $edit_by	}}</td>
						</tr>
					</table>
				</div>
			</div>
        </div>
    </div>
	@include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('page::pages.navigation.back to index') }}</dd>
    </dl>
@stop
