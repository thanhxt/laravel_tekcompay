<div class="row">
	<div class="col-md-12">
		<div class="nav-tabs-custom">
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1-1">
					{!! Form::open(['route' => ['admin.bank.manage.index'], 'method' => 'get', 'id' => 'search_form']) !!}
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('name', trans('bank::manage.form.name')) !!}
									{!! Form::text("name",old("name"), ['class' => "form-control", 'placeholder' => trans('bank::manage.form.name'), "id"=>"name"]) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('code', trans('bank::manage.form.code')) !!}
									{!! Form::text("code", old("code"), ['class' => "form-control", 'placeholder' => trans('bank::manage.form.code'), "id"=>"code"]) !!}
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group search-control">
									<button type="submit" class="btn btn-primary btn-danger">{{ trans('member::members.button.search') }}</button>
									<label>&nbsp;</label>
									<a href="{{ URL::route('admin.bank.manage.edit') }}" class="btn btn-primary btn-success" style="padding: 4px 10px;">
										<i class="fa fa-pencil"></i> {{ trans('bank::manage.button.bank create') }}
									</a>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>	
			</div>	
		</div>	
	</div>	
</div>	

			
