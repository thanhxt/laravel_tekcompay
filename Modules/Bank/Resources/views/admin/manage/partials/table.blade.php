<table class="data-table table table-bordered table-hover">
	<thead>
		<tr>
			<th>#</th>
			<th>{{ trans('bank::manage.table.name') }}</th>
			<th>{{ trans('bank::manage.table.code') }}</th>
			<th data-sortable="false">{{ trans('member::members.table.actions') }}</th>
		</tr>
	</thead>
	<tbody>
	<?php if (count($banks)): ?>
		<?php foreach ($banks as $item): ?>
		<tr>
			<td>
				{{ $item->id }}
			</td>
			<td>
				{{ $item->name }}
			</td>
			<td>
				{{ $item->code }}
			</td>
			<td>
				<div class="btn-group">
					<a href="{{ route('admin.bank.manage.view', [$item->id]) }}" class="btn btn-default btn-flat" title="{{ trans('member::members.icon.caption view') }}"><i class="fa fa-eye"></i></a>
					<a href="{{ route('admin.bank.manage.edit', ['id' => $item->id]) }}" class="btn btn-default btn-flat" title="{{ trans('member::members.icon.caption edit') }}"><i class="fa fa-pencil"></i></a>
				</div>
			</td>
		</tr>
		<?php endforeach; ?>
	<?php else: ?>
		<tr>
			<td colspan='6'>{{ trans('member::members.table.empty') }}</td>
		</tr>	
	<?php endif; ?>
	</tbody>
</table>
<div class='text-center'>
	<?php
		$data = request()->all();
		unset($data["_token"]);
	?>
	<?php  echo $banks->appends($data)->render() ?>
</div>