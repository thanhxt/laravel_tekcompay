@extends('layouts.master')

@section('content-header')
	<?php 
		$translate_path = 'bank::manage.title.create bank';
		if(@$model->id > 0){
			$translate_path = 'bank::manage.title.edit bank';
		}
	?>
    <h1>
        {{ trans($translate_path) }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ URL::route('admin.bank.manage.index') }}">{{ trans('bank::manage.title.list') }}</a></li>
        <li class="active">{{ trans($translate_path) }}</li>
    </ol>
@stop

@section('content')
@inject('help','Modules\Agency\Utility\Help')
    {!! Form::open(['route' => ['admin.bank.manage.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <div class="tab-content">
					<div class="tab-pane active" id="tab_1-1">
						<div class="box-body">
							<div class="row">
								{!! Form::hidden('id', @$model->id, []) !!}
								<div class="col-sm-4">
									<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
										{!! Form::label('name', trans('bank::manage.form.name')) !!}
										{!! Form::text("name",@$model->name, ['class' => "form-control", 'placeholder' => trans('bank::manage.form.name'), "id"=>"name",'required'=>'required']) !!}
										{!! $errors->first('name', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
										{!! Form::label('code', trans('bank::manage.form.code')) !!}
										{!! Form::text("code",@$model->code, ['class' => "form-control", 'placeholder' => trans('bank::manage.form.code'), "id"=>"code",'required'=>'required']) !!}
										{!! $errors->first('code', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
								<?php 
									$status_arr = $help->getStatusArr();
								?>
								<div class="col-sm-4">
									<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
										{!! Form::label('status', trans('bank::manage.form.status')) !!}
										{!! Form::select("status", $status_arr, @$model->status, ['class' => "form-control", 'placeholder' => trans('bank::manage.form.status')]) !!}
										{!! $errors->first('status', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="box-footer">
						<?php 
							$btn_txt = 'core::core.button.save';
						?>
                        <button type="submit" class="btn btn-primary btn-success">{{ trans($btn_txt) }}</button>
                        <button class="btn btn-default btn-flat" name="button" type="reset">{{ trans('core::core.button.reset') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ URL::route('admin.bank.manage.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>

    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('page::pages.navigation.back to index') }}</dd>
    </dl>
@stop
