@inject('helpAgency','Modules\Agency\Utility\Help')
<div class="row">
	<div class="col-md-12">
		<div class="nav-tabs-custom">
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1-1">
					{!! Form::open(['route' => ['admin.pos.manage.index'], 'method' => 'get', 'id' => 'search_form']) !!}
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('name', trans('pos::manage.form.name')) !!}
									{!! Form::text("name",old("name"), ['class' => "form-control", 'placeholder' => trans('pos::manage.form.name')]) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('code', trans('pos::manage.form.code')) !!}
									{!! Form::text("code", old("code"), ['class' => "form-control", 'placeholder' => trans('pos::manage.form.code')]) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('agency_id', trans('bill::import.table.agency_name')) !!}
									<?php
										$agency_arr = $helpAgency->getAgencyArr();
									?>
									{!! Form::select("agency_id", $agency_arr, old("agency_id"), ['class' => "form-control select-search", 'placeholder' => trans('agency::manage.table.chosen')]) !!}
									{!! $errors->first('agency_id', '<span class="help-block">:message</span>') !!}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('spend', trans('pos::manage.table.spend')) !!}
									{!! Form::text("spend_from", old("spend_from"), ['class' => "form-control money-format", 'placeholder' => trans('pos::manage.form.from')]) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label>&nbsp;</label>
									{!! Form::text("spend_to", old("spend_to"), ['class' => "form-control money-format", 'placeholder' => trans('pos::manage.form.to')]) !!}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('balance', trans('pos::manage.table.balance')) !!}
									{!! Form::text("balance_from", old("balance_from"), ['class' => "form-control money-format", 'placeholder' => trans('pos::manage.form.from')]) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label>&nbsp;</label>
									{!! Form::text("balance_to", old("balance_to"), ['class' => "form-control money-format", 'placeholder' => trans('pos::manage.form.to')]) !!}
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group search-control">
									<button type="submit" class="btn btn-primary btn-danger">{{ trans('member::members.button.search') }}</button>
									<label>&nbsp;</label>
									<a href="{{ URL::route('admin.pos.manage.edit') }}" class="btn btn-primary btn-success" style="padding: 4px 10px;">
										<i class="fa fa-pencil"></i> {{ trans('pos::manage.button.create') }}
									</a>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>	
			</div>	
		</div>	
	</div>	
</div>	

			
