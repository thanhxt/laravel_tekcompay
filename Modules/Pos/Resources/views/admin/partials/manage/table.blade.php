@inject('help','Modules\Agency\Utility\Help')
@inject('helpBill','Modules\Bill\Utility\Help')
<table class="data-table table table-bordered table-hover">
	<thead>
		<tr>
			<th>#</th>
			<th>{{ trans('pos::manage.table.name') }}</th>
			<th>{{ trans('pos::manage.table.code') }}</th>
			<th>{{ trans('agency::manage.table.name') }}</th>
			<th class='text-right'>{{ trans('pos::manage.table.spend') }}</th>
			<th class='text-right'>{{ trans('pos::manage.table.balance') }}</th>
			<th data-sortable="false">{{ trans('member::members.table.actions') }}</th>
		</tr>
	</thead>
	<tbody>
	<?php if (count($machines)): ?>
		<?php foreach ($machines as $item): ?>
		<tr>
			<td>
				{{ $item->id }}
			</td>
			<td>
				{{ $item->name }}
			</td>
			<td>
				{{ $item->code }}
			</td>
			<td>
				<?php 
					$agency_arr = $help->getAgencyArr();
					$agency_name = isset($agency_arr[$item->agency_id]) ? $agency_arr[$item->agency_id] : trans("member::members.system.common.not assign");
				?>
				{{ $agency_name }}
			</td>
			<?php 
				$spend = $helpBill->formatMoney($item->spend);
				$balance = $helpBill->formatMoney($item->balance);
			?>
			<td class='text-right'>
				{{ $spend }}&nbsp;{{ trans('member::members.system.common.currency txt') }}
			</td>
			<td class='text-right'>
				{{ $balance }}&nbsp;{{ trans('member::members.system.common.currency txt') }}
			</td>
			<td>
				<div class="btn-group">
					<a href="{{ route('admin.pos.manage.view', [$item->id]) }}" class="btn btn-default btn-flat" title="{{ trans('member::members.icon.caption view') }}"><i class="fa fa-eye"></i></a>
					<a href="{{ route('admin.pos.manage.edit', ['id' => $item->id]) }}" class="btn btn-default btn-flat" title="{{ trans('member::members.icon.caption edit') }}"><i class="fa fa-pencil"></i></a>
				</div>
			</td>
		</tr>
		<?php endforeach; ?>
	<?php else: ?>
		<tr>
			<td colspan='7'>{{ trans('member::members.table.empty') }}</td>
		</tr>	
	<?php endif; ?>
	</tbody>
</table>
<div class='text-center'>
	<?php
		$data = request()->all();
		unset($data["_token"]);
	?>
	<?php  echo $machines->appends($data)->render() ?>
</div>