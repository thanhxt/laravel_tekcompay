<div class="row">
	<div class="col-md-12">
		<div class="nav-tabs-custom">
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1-1">
					{!! Form::open(['route' => ['admin.pos.quota.index'], 'method' => 'get', 'id' => 'search_form']) !!}
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									{!! Form::label('name', trans('pos::quota.form.name')) !!}
									{!! Form::text("name",old("name"), ['class' => "form-control", 'placeholder' => trans('pos::quota.form.name')]) !!}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('quota_from', trans('pos::quota.table.amount')) !!}
									{!! Form::text("quota_from", old("quota_from"), ['class' => "form-control money-format", 'placeholder' => trans('pos::manage.form.from')]) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label>&nbsp;</label>
									{!! Form::text("quota_to", old("quota_to"), ['class' => "form-control money-format", 'placeholder' => trans('pos::manage.form.to')]) !!}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('updated_at', trans('pos::quota.table.updated_at')) !!}
									{!! Form::text("update_from", old("update_from"), ['class' => "form-control date-effect", 'placeholder' => trans('pos::manage.form.from'), 'id' => 'update_from']) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label>&nbsp;</label>
									{!! Form::text("update_to", old("update_to"), ['class' => "form-control date-effect", 'placeholder' => trans('pos::manage.form.to'), 'id' => 'update_to']) !!}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group search-control">
									<button type="submit" class="btn btn-primary btn-danger">{{ trans('member::members.button.search') }}</button>
									<label>&nbsp;</label>
									<a href="{{ URL::route('admin.pos.quota.edit') }}" class="btn btn-primary btn-success" style="padding: 4px 10px;">
										<i class="fa fa-pencil"></i> {{ trans('pos::quota.button.create') }}
									</a>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>	
			</div>	
		</div>	
	</div>	
</div>	

			
