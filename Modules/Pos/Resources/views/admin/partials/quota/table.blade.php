@inject('helpBill','Modules\Bill\Utility\Help')
<table class="data-table table table-bordered table-hover">
	<thead>
		<tr>
			<th>#</th>
			<th width="150">{{ trans('pos::quota.table.amount') }}</th>
			<th>{{ trans('pos::quota.table.name') }}</th>
			<th>{{ trans('pos::quota.table.updated_at') }}</th>
			<th data-sortable="false">{{ trans('member::members.table.actions') }}</th>
		</tr>
	</thead>
	<tbody>
	<?php if (count($pos_quota)): ?>
		<?php foreach ($pos_quota as $item): ?>
		<tr>
			<td>
				{{ $item->id }}
			</td>
			<td class='text-right'>
				<?php 
					$amount = $helpBill->formatMoney($item->amount);
				?>
				{{ $amount }}&nbsp;₫
			</td>
			<td>
				{{ $item->name }}
			</td>
			<td>
				<?php 
					$updated_at = $helpBill->formatDateForDisplay($item->updated_at);
				?>
				{{ $updated_at }}
			</td>
			<td>
				<div class="btn-group">
					<a href="{{ route('admin.pos.quota.view', [$item->id]) }}" class="btn btn-default btn-flat" title="{{ trans('member::members.icon.caption view') }}"><i class="fa fa-eye"></i></a>
					<a href="{{ route('admin.pos.quota.edit', ['id' => $item->id]) }}" class="btn btn-default btn-flat" title="{{ trans('member::members.icon.caption edit') }}"><i class="fa fa-pencil"></i></a>
				</div>
			</td>
		</tr>
		<?php endforeach; ?>
	<?php else: ?>
		<tr>
			<td colspan='5'>{{ trans('member::members.table.empty') }}</td>
		</tr>	
	<?php endif; ?>
	</tbody>
</table>
<div class='text-center'>
	<?php
		$data = request()->all();
		unset($data["_token"]);
	?>
	<?php  echo $pos_quota->appends($data)->render() ?>
</div>