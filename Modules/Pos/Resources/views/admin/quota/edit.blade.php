@extends('layouts.master')

@section('content-header')
	<?php 
		$translate_path = 'pos::quota.title.create quota';
		if(@$model->id > 0){
			$translate_path = 'pos::quota.title.edit quota';
		}
	?>
    <h1>
        {{ trans($translate_path) }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ URL::route('admin.pos.manage.index') }}">{{ trans('pos::quota.title.list') }}</a></li>
        <li class="active">{{ trans($translate_path) }}</li>
    </ol>
@stop

@section('content')
@inject('helpBill','Modules\Bill\Utility\Help')
    {!! Form::open(['route' => ['admin.pos.quota.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <div class="tab-content">
					<div class="tab-pane active" id="tab_1-1">
						<div class="box-body">
							<div class="row">
								{!! Form::hidden('id', @$model->id, []) !!}
								<div class="col-sm-3">
									<div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
										{!! Form::label('amount', trans('pos::quota.form.amount')) !!}
										<?php 
											$amount = 0;
											if (isset($model->amount)) {
												$amount = round($model->amount, 0);
											}
										?>
										{!! Form::text("amount",$amount, ['class' => "form-control money-format", 'placeholder' => trans('pos::quota.form.amount'), "id"=>"amount",'required'=>'required']) !!}
										{!! $errors->first('amount', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
								<?php 
									$pos_arr = $helpBill->getPosByAgencyId();
								?>
								<div class="col-sm-3">
									<div class="form-group{{ $errors->has('pos_id') ? ' has-error' : '' }}">
										{!! Form::label('pos_id', trans('pos::quota.form.pos name')) !!}
										{!! Form::select("pos_id", $pos_arr, @$model->pos_id, ['class' => "form-control", 'placeholder' => trans('pos::quota.form.chosen pos name') ,'required'=>'required']) !!}
										{!! $errors->first('pos_id', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
							<?php if (!$model->id): ?>
								<div class="row">
									<div class="col-sm-3">
										<div class="form-group">
											{!! Form::checkbox('update_balance', '1'); !!}
											<span>&nbsp;{!! Form::label('update_balance', trans('pos::quota.form.update_balance')) !!}</span>
										</div>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
                    <div class="box-footer">
						<?php 
							$btn_txt = 'core::core.button.save';
						?>
                        <button type="submit" class="btn btn-primary btn-success">{{ trans($btn_txt) }}</button>
                        <button class="btn btn-default btn-flat" name="button" type="reset">{{ trans('core::core.button.reset') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ URL::route('admin.pos.quota.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>

    {!! Form::close() !!}
@stop

@section('scripts')
	{!! Theme::script('vendor/inputmask/js/jquery.inputmask.js') !!}
	<script>
        $( document ).ready(function() {
			$(".money-format").inputmask({"alias":"numeric","groupSeparator":",","autoGroup":true,"removeMaskOnSubmit":true});
			
        });
    </script>
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('page::pages.navigation.back to index') }}</dd>
    </dl>
@stop
