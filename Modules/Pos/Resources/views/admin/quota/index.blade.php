@extends('layouts.master')

@section('styles')
	{!! Theme::style('vendor/datepicker/css/bootstrap-datepicker.css') !!}
@stop

@section('content-header')
<h1>
    {{ trans('pos::quota.title.list') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class="active">{{ trans('pos::quota.title.list') }}</li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-body">
				@include('pos::admin.partials.quota.search')
			</div>
		</div>
		<div class="box box-danger">
			<div class="box-body">
				@include('pos::admin.partials.quota.table')
			</div>
		</div>
	</div>
</div>

@include('core::partials.delete-modal')
@stop

@section('scripts')
	{!! Theme::script('vendor/datepicker/js/bootstrap-datepicker.js') !!}
	{!! Theme::script('vendor/inputmask/js/jquery.inputmask.js') !!}
    <script>
        $( document ).ready(function() {
			$.fn.datepicker.defaults.format = "dd-mm-yyyy";
			$('.date-effect').datepicker({});
			/*  */
			$("#update_from").datepicker({
				todayBtn:  1,
				autoclose: true,
			}).on('changeDate', function (selected) {
				var minDate = new Date(selected.date.valueOf());
				$('#update_to').datepicker('setStartDate', minDate);
			});

			$("#update_to").datepicker()
			.on('changeDate', function (selected) {
				var maxDate = new Date(selected.date.valueOf());
				$('#update_from').datepicker('setEndDate', maxDate);
			});
			
			$(".money-format").inputmask({"alias":"numeric","groupSeparator":",","autoGroup":true,"removeMaskOnSubmit":true});
			
        });
    </script>
@stop