@extends('layouts.master')

@section('content-header')
<h1>
    {{ trans('pos::manage.title.list') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class="active">{{ trans('pos::manage.title.list') }}</li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-body">
				@include('pos::admin.partials.manage.search')
			</div>
		</div>
		<div class="box box-danger">
			<div class="box-body">
				@include('pos::admin.partials.manage.table')
			</div>
		</div>
	</div>
</div>

@include('core::partials.delete-modal')
@stop
@section('scripts')
	{!! Theme::script('vendor/inputmask/js/jquery.inputmask.js') !!}
	<script>
        $( document ).ready(function() {
			$(".money-format").inputmask({"alias":"numeric","groupSeparator":",","autoGroup":true,"removeMaskOnSubmit":true});
			
        });
    </script>
@stop