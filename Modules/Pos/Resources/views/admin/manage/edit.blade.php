@extends('layouts.master')

@section('content-header')
	<?php 
		$translate_path = 'pos::manage.title.create pos';
		if(@$model->id > 0){
			$translate_path = 'pos::manage.title.edit pos';
		}
	?>
    <h1>
        {{ trans($translate_path) }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ URL::route('admin.pos.manage.index') }}">{{ trans('pos::manage.title.list') }}</a></li>
        <li class="active">{{ trans($translate_path) }}</li>
    </ol>
@stop

@section('content')
@inject('help','Modules\Agency\Utility\Help')
    {!! Form::open(['route' => ['admin.pos.manage.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <div class="tab-content">
					<div class="tab-pane active" id="tab_1-1">
						<div class="box-body">
							<div class="row">
								{!! Form::hidden('id', @$model->id, []) !!}
								<div class="col-sm-4">
									<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
										{!! Form::label('name', trans('pos::manage.form.name')) !!}
										{!! Form::text("name",@$model->name, ['class' => "form-control", 'placeholder' => trans('pos::manage.form.name'), "id"=>"name",'required'=>'required']) !!}
										{!! $errors->first('name', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
										{!! Form::label('code', trans('pos::manage.form.code')) !!}
										{!! Form::text("code",@$model->code, ['class' => "form-control", 'placeholder' => trans('pos::manage.form.code'), "id"=>"code",'required'=>'required']) !!}
										{!! $errors->first('code', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
								<?php 
									$status_arr = $help->getStatusArr();
								?>
								<div class="col-sm-4">
									<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
										{!! Form::label('status', trans('pos::manage.form.status')) !!}
										{!! Form::select("status", $status_arr, @$model->status, ['class' => "form-control", 'placeholder' => trans('pos::manage.form.status')]) !!}
										{!! $errors->first('status', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
								<?php 
									$agency_arr = $help->getAgencyArr();
								?>
								<div class="col-sm-4">
									<div class="form-group{{ $errors->has('agency_id') ? ' has-error' : '' }}">
										{!! Form::label('agency_id', trans('agency::manage.form.name')) !!}
										{!! Form::select("agency_id", $agency_arr, @$model->agency_id, ['class' => "form-control", 'placeholder' => trans('agency::manage.form.chosen')]) !!}
										{!! $errors->first('agency_id', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group{{ $errors->has('branch_name') ? ' has-error' : '' }}">
										{!! Form::label('branch_name', trans('pos::manage.form.branch_name')) !!}
										{!! Form::text("branch_name",@$model->branch_name, ['class' => "form-control", 'placeholder' => trans('pos::manage.form.branch_name')]) !!}
										{!! $errors->first('branch_name', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group{{ $errors->has('branch_address') ? ' has-error' : '' }}">
										{!! Form::label('branch_address', trans('pos::manage.form.branch_address')) !!}
										{!! Form::text("branch_address",@$model->branch_address, ['class' => "form-control", 'placeholder' => trans('pos::manage.form.branch_address')]) !!}
										{!! $errors->first('branch_address', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
								
								
							</div>
						</div>
					</div>
                    <div class="box-footer">
						<?php 
							$btn_txt = 'core::core.button.save';
						?>
                        <button type="submit" class="btn btn-primary btn-success">{{ trans($btn_txt) }}</button>
                        <button class="btn btn-default btn-flat" name="button" type="reset">{{ trans('core::core.button.reset') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ URL::route('admin.pos.manage.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>

    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('page::pages.navigation.back to index') }}</dd>
    </dl>
@stop
