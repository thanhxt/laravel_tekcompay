<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/pos'], function (Router $router) {
    // manage agency
    $router->get('manage/index', [
        'as' => 'admin.pos.manage.index',
        'uses' => 'ManageController@index',
        'middleware' => 'can:pos.manage.all',
    ]);

    $router->get('manage/edit', [
        'as' => 'admin.pos.manage.edit',
        'uses' => 'ManageController@edit',
        'middleware' => 'can:pos.manage.all',
    ]);

    $router->post('manage/store', [
        'as' => 'admin.pos.manage.store',
        'uses' => 'ManageController@store',
        'middleware' => 'can:pos.manage.all',
    ]);

    $router->delete('manage/delete/{id}', [
        'as' => 'admin.pos.manage.delete',
        'uses' => 'ManageController@delete',
        'middleware' => 'can:pos.manage.all',
    ]);

    $router->get('manage/view/{id}', [
        'as' => 'admin.pos.manage.view',
        'uses' => 'ManageController@view',
        'middleware' => 'can:pos.manage.all',
    ]);

    // manage quota
    $router->get('quota/index', [
        'as' => 'admin.pos.quota.index',
        'uses' => 'QuotaController@index',
        'middleware' => 'can:pos.manage.all',
    ]);

    $router->get('quota/edit', [
        'as' => 'admin.pos.quota.edit',
        'uses' => 'QuotaController@edit',
        'middleware' => 'can:pos.manage.all',
    ]);

    $router->post('quota/store', [
        'as' => 'admin.pos.quota.store',
        'uses' => 'QuotaController@store',
        'middleware' => 'can:pos.manage.all',
    ]);

    $router->get('quota/view/{id}', [
        'as' => 'admin.pos.quota.view',
        'uses' => 'QuotaController@view',
        'middleware' => 'can:pos.manage.all',
    ]);

});
