<?php

namespace Modules\Pos\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Http\Request;
use Modules\Pos\Business\QuotaBus;
use Modules\Pos\Http\Requests\QuotaRequest;
use Modules\User\Contracts\Authentication;
use Modules\Bill\Utility\Help as HelpBill;

class QuotaController extends AdminBaseController
{
    /**
     * @var Authentication
     */
    protected $auth;

    protected $helpBill;

    public function __construct(
        Authentication $auth,
        HelpBill $helpBill
    ) {
        parent::__construct();
        $this->auth = $auth;
        $this->helpBill = $helpBill;
    }

    public function index()
    {
        $quota_from = isset($_REQUEST['quota_from']) ? $_REQUEST['quota_from'] : 0;
        $quota_to = isset($_REQUEST['quota_to']) ? $_REQUEST['quota_to'] : 0;
        $update_from = isset($_REQUEST['update_from']) ? $_REQUEST['update_from'] : '';
        $update_to = isset($_REQUEST['update_to']) ? $_REQUEST['update_to'] : '';
        $pos_name = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';

        $quota_bus = new QuotaBus();
        $agency_id = $this->helpBill->getAgencyByCurrentUser();
        $pos_quota = $quota_bus->search($agency_id, $quota_from, $quota_to, $update_from, $update_to, $pos_name);

        return view('pos::admin.quota.index', compact('pos_quota'));
    }

    public function edit(Request $request)
    {
        $data = $request->all();
        $id = @$data['id'];
        $quota_bus = new QuotaBus();
        $model = $quota_bus->getQuotaModelById($id);

        return view('pos::admin.quota.edit', compact('model'));
    }

    public function store(QuotaRequest $request)
    {
        $data = $request->all();
        $quota_bus = new QuotaBus();
        $current_user = $this->auth->id();
        if ($data['id']) {
            $data['edit_by'] = $current_user;
        } else {
            $data['create_by'] = $current_user;
            $data['edit_by'] = $current_user;
        }

        $bResult = $quota_bus->saveData($data);
        if ($bResult) {
            $msg_path = 'pos::quota.messages.store success';
            return redirect()->route('admin.pos.quota.index')
                ->withSuccess(trans($msg_path));
        } else {
            $msg_path = 'pos::quota.messages.store fail';
            return redirect()->back()->withInput($request->input())->withError(trans($msg_path));
        }
    }

    public function view($id)
    {
        $quota_bus = new QuotaBus();
        $model = $quota_bus->getQuotaModelById($id);
        return view('pos::admin.quota.view', compact('model'));
    }

}
