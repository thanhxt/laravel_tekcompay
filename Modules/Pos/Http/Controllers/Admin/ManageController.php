<?php

namespace Modules\Pos\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Http\Request;
use Modules\Pos\Business\PosBus;
use Modules\Pos\Http\Requests\PosRequest;
use Modules\User\Contracts\Authentication;

class ManageController extends AdminBaseController
{
    /**
     * @var Authentication
     */
    protected $auth;

    public function __construct(
        Authentication $auth
    ) {
        parent::__construct();
        $this->auth = $auth;
    }

    public function index()
    {
        $name = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
        $code = isset($_REQUEST['code']) ? $_REQUEST['code'] : '';
        $agency_id = isset($_REQUEST['agency_id']) ? $_REQUEST['agency_id'] : '';
        $spend_from = isset($_REQUEST['spend_from']) ? $_REQUEST['spend_from'] : '';
        $spend_to = isset($_REQUEST['spend_to']) ? $_REQUEST['spend_to'] : '';
        $balance_from = isset($_REQUEST['balance_from']) ? $_REQUEST['balance_from'] : '';
        $balance_to = isset($_REQUEST['balance_to']) ? $_REQUEST['balance_to'] : '';

        $pos_bus = new PosBus();
        $machines = $pos_bus->search($name, $code, $agency_id, $spend_from, $spend_to, $balance_from, $balance_to);

        return view('pos::admin.manage.index', compact('machines'));
    }

    public function edit(Request $request)
    {
        $data = $request->all();
        $id = @$data['id'];
        $pos_bus = new PosBus();
        $model = $pos_bus->getPosModelById($id);
        return view('pos::admin.manage.edit', compact('model'));
    }

    public function store(PosRequest $request)
    {
        $data = $request->all();
        $pos_bus = new PosBus();
        $current_id = $this->auth->id();
        if ($data['id']) {
            $data['edit_by'] = $current_id;
        } else {
            $data['create_by'] = $current_id;
            $data['edit_by'] = $current_id;
        }
        $bResult = $pos_bus->saveData($data);
        if ($bResult) {
            $msg_path = 'pos::manage.messages.created success';
            if ($data['id']) {
                $msg_path = 'pos::manage.messages.updated success';
            }
            return redirect()->route('admin.pos.manage.index')
                ->withSuccess(trans($msg_path));
        } else {
            $msg_path = 'pos::manage.messages.created fail';
            if ($data['id']) {
                $msg_path = 'pos::manage.messages.updated fail';
            }
            return redirect()->route('admin.pos.manage.index')
                ->withError(trans($msg_path));
        }
    }

    public function delete($id)
    {
        $pos_bus = new PosBus();
        $bResult = $pos_bus->delete($id);

        if ($bResult) {
            return redirect()->route('admin.pos.manage.index')
                ->withSuccess(trans('pos::manage.messages.delete success'));
        } else {
            return redirect()->route('admin.pos.manage.index')
                ->withError(trans('pos::manage.messages.delete fail'));
        }
    }

    public function view($id)
    {
        $pos_bus = new PosBus();
        $model = $pos_bus->getPosModelById($id);
        return view('pos::admin.manage.view', compact('model'));
    }

}
