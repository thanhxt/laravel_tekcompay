<?php

namespace Modules\Pos\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuotaRequest extends FormRequest
{
    public function rules()
    {
        $id = request()->get('id');
        $pos_unique = 'required|unique:posquota';

        if ($id) {
            $pos_unique = $pos_unique . ',pos_id,' . $id;
        }

        return [
            'amount' => 'required|numeric|min:0',
            'pos_id' => $pos_unique,
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'required' => trans('pos::quota.messages.valid required'),
            'amount.min' => trans('pos::quota.messages.valid amount min'),
            'pos_id.unique' => trans('pos::quota.messages.valid pos_id unique'),
        ];
    }
}
