<?php

namespace Modules\Pos\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PosRequest extends FormRequest
{
    public function rules()
    {
        $id = request()->get('id');
        $code_unique = $name_unique = 'required|unique:pos';
        if ($id) {
            $code_unique = $code_unique . ',code,' . $id;
            $name_unique = $name_unique . ',name,' . $id;
        }

        return [
            'code' => $code_unique,
            'name' => $name_unique,
            'status' => 'required',
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'required' => trans('pos::manage.messages.valid required'),
            'code.unique' => trans('pos::manage.messages.valid code unique'),
            'name.unique' => trans('pos::manage.messages.valid name unique'),
        ];
    }
}
