<?php

namespace Modules\Pos\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterPosSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }
    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('member::members.title.system'), function (Group $group) {
            $group->weight(902);
            $group->item(trans('pos::manage.title.list'), function (Item $item) {
                $item->weight(3);
                $item->icon('fa fa-print');
                $item->authorize(
                    $this->auth->hasAccess('pos.manage.all')
                );

                $item->item(trans('pos::manage.title.list'), function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-print');
                    $item->route('admin.pos.manage.index');
                    $item->authorize(
                        $this->auth->hasAccess('pos.manage.all')
                    );
                });

                $item->item(trans('pos::quota.title.list'), function (Item $item) {
                    $item->weight(1);
                    $item->icon('fa fa-money');
                    $item->route('admin.pos.quota.index');
                    $item->authorize(
                        $this->auth->hasAccess('pos.quota.all')
                    );
                });

            });
        });

        return $menu;
    }
}
