<?php

namespace Modules\Pos\Business;

use Modules\Bill\Entities\Pos;

class PosBus
{
    public function search($name, $code, $agency_id, $spend_from, $spend_to, $balance_from, $balance_to, $pageSize = PAGE_SIZE){
        $pos_model = new Pos();
        $data = $pos_model->search($name, $code, $agency_id, $spend_from, $spend_to, $balance_from, $balance_to, $pageSize);
        return $data;
    }

    public function saveData($data) {
        $id = $data["id"];
        $agency_model = $this->getPosModelById($id);
        return $agency_model->saveData($data);
    }

    public function getPosModelById($id) {
        if($id){
            $model = Pos::find($id);
        }else{
            $model = new Pos();
        }
        return $model;
    }

    public function delete($id) {
        $result = Pos::destroy($id);
        return $result;
    }

    public function getPosListByAgencyId($agency_id = 0, $status = 1) {
        $pos_model = new Pos();
        $result = $pos_model->getPosListByAgencyId($agency_id, $status);
        return $result;
    }
}