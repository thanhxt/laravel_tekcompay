<?php

namespace Modules\Pos\Business;

use Modules\Bill\Entities\PosQuota;
use Modules\Bill\Utility\Help as HelpBill;
use Modules\Bill\Entities\Pos;

class QuotaBus
{
    public function search($agency_id = 0, $quota_from, $quota_to, $update_from, $update_to, $pos_name, $pageSize = PAGE_SIZE){
        $pos_model = new PosQuota();
        if ($update_from != '') {
            $update_from = HelpBill::formatDateForDB($update_from);
        }

        if ($update_to > 0) {
            $update_to = HelpBill::formatDateForDB($update_to);
        }
        $data = $pos_model->search($agency_id = 0, $quota_from, $quota_to, $update_from, $update_to, $pos_name, $pageSize);
        return $data;
    }

    public function saveData($data)
    {
        $bResult = false;
        $id = $data["id"];
        $quota_model = $this->getQuotaModelById($id);
        $model = $quota_model->fill($data);
        \DB::beginTransaction();
        try {
            $model->save();
            if (isset($data['update_balance']) && $data['update_balance'] == 1) {
                $pos_id = $model->pos_id;
                $pos_model = Pos::find($pos_id);
                $pos_model->balance = $model->amount;
                $pos_model->quota = $model->amount;
                $pos_model->spend = 0;
                $pos_model->save();
            }
            \DB::commit();
            $bResult = true;
        }catch (\Exception $e) {
            \DB::rollBack();
        }
        return $bResult;
    }

    public function getQuotaModelById($id) {
        if($id){
            $model = PosQuota::find($id);
        }else{
            $model = new PosQuota();
        }
        return $model;
    }

    public function delete($id) {
        $result = PosQuota::destroy($id);
        return $result;
    }
}