<?php

namespace Modules\Member\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterMemberSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }
    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('member::members.title.system'), function (Group $group) {
            $group->weight(909);
            $group->item(trans('member::members.sidebar.title'), function (Item $item) {
                $item->weight(6);
                $item->icon('fa fa-users');
                $item->authorize(
                    $this->auth->hasAccess('member.all')
                );

                $item->item(trans('member::members.sidebar.list'), function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-user');
                    $item->route('admin.member.index');
                    $item->authorize(
                        $this->auth->hasAccess('member.all')
                    );
                });

            });
        });

        return $menu;
    }
}
