<?php

namespace Modules\Member\Business;

use Modules\User\Repositories\UserRepository;
use Modules\Bill\Entities\Member;


class MemberBus
{
    protected $_user;

    public function __construct(UserRepository $user)
    {
        $this->_user = $user;
    }

    public function search($name, $email, $pageSize = PAGE_SIZE) {
        $member_model = new Member();
        $data = $member_model->search($name, $email, $pageSize);
        return $data;
    }
    public function getRoleOfUser($user_id) {
        $member_model = new Member();
        $data = $member_model->getRoleOfUser($user_id);
        return $data;
    }
    public function getListMemberRole() {
        $member_model = new Member();
        $data = $member_model->getListMemberRole();
        return $data;
    }
    public function saveMember($data) {
        $member_model = new Member();
        $user = $this->_user;
        $data = $member_model->saveMember($user, $data);
        return $data;
    }

    public function getMemberModelById($id) {
        if($id){
            $model = Member::find($id);
        }else{
            $model = new Member();
        }
        return $model;
    }

    public function delete($id) {
        $result = $this->_user->delete($id);
        return $result;
    }

    public function getUserForEdit($id) {
        $data['user'] = $this->_user->find($id);
        $data['roles'] = $this->getListMemberRole();
        return $data;
    }
}