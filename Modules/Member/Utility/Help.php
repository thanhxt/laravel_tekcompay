<?php


namespace Modules\Member\Utility;

use Modules\Member\Entities\BillType;
use Modules\User\Entities\Sentinel\User;

class Help
{
    public function getUserNameById($user_id) {
        $user_name = trans("member::members.system.common.not assign");
        $model = User::find($user_id);
        if ($model) {
            $user_name = $model->first_name;
        }

        return $user_name;
    }

}