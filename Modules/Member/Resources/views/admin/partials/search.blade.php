<div class="row">
	<div class="col-md-12">
		<div class="nav-tabs-custom">
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1-1">
					{!! Form::open(['route' => ['admin.member.index'], 'method' => 'get', 'id' => 'search_form']) !!}
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('name', trans('member::members.form.first-name')) !!}
									{!! Form::text("name",old("name"), ['class' => "form-control", 'placeholder' => trans('member::members.form.first-name')]) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('email', trans('member::members.form.email')) !!}
									{!! Form::text("email", old("email"), ['class' => "form-control", 'placeholder' => trans('member::members.form.email')]) !!}
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group search-control">
									<button type="submit" class="btn btn-primary btn-danger">{{ trans('member::members.button.search') }}</button>
									<label>&nbsp;</label>
									<a href="{{ URL::route('admin.member.create') }}" class="btn btn-primary btn-success" style="padding: 4px 10px;">
										<i class="fa fa-pencil"></i> {{ trans('member::members.button.member create') }}
									</a>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>	
			</div>	
		</div>	
	</div>	
</div>	

			
