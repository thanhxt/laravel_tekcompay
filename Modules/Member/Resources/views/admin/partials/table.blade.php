@inject('userRepository', 'Modules\User\Repositories\UserRepository')
<table class="data-table table table-bordered table-hover">
	<thead>
		<tr>
			<th>Id</th>
			<th>{{ trans('member::members.table.name') }}</th>
			<th>{{ trans('member::members.table.email') }}</th>
			<th>{{ trans('member::members.table.created-at') }}</th>
			<th>{{ trans('member::members.table.status') }}</th>
			<th data-sortable="false">{{ trans('member::members.table.actions') }}</th>
		</tr>
	</thead>
	<tbody>
	<?php if (count($users)): ?>
		<?php foreach ($users as $user): ?>
			<tr>
				<td>
					<a href="{{ route('admin.member.edit', ['member_id' => $user->id]) }}">
						{{ $user->id }}
					</a>
				</td>
				<td>
					<a href="{{ route('admin.member.edit', ['member_id' => $user->id]) }}">
						{{ $user->first_name }}
					</a>
				</td>
				<td>
					<a href="{{ route('admin.member.edit', ['member_id' => $user->id]) }}">
						{{ $user->email }}
					</a>
				</td>
				<td>
					<a href="{{ route('admin.member.edit', ['member_id' => $user->id]) }}">
						{{ $user->created_at }}
					</a>
				</td>
				<td>
					<a href="{{ route('admin.member.edit', ['member_id' => $user->id]) }}">
					<?php 
						$userData = $userRepository->find($user->id);
						$status = (bool) $userData->isActivated() ? trans('member::members.table.active') : trans('member::members.table.deactive'); 
					?>
					{{ $status}}
					
				</td>
				<td>
					<div class="btn-group">
						<a href="{{ route('admin.member.edit', ['member_id' => $user->id]) }}" class="btn btn-default btn-flat" title="{{ trans('member::members.icon.caption edit') }}"><i class="fa fa-pencil"></i></a>
						<button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.member.user.destroy', [$user->id]) }}" title="{{ trans('member::members.icon.caption delete') }}"><i class="fa fa-trash"></i></button>
					</div>
				</td>
			</tr>
		<?php endforeach; ?>
	<?php else: ?>
		<tr>
			<td colspan='6'>{{ trans('member::members.table.empty') }}</td>
		</tr>		
	<?php endif; ?>
	</tbody>
</table>
<div class='text-center'>
	<?php
		$data = request()->all();
		unset($data["_token"]);
	?>
	<?php  echo $users->appends($data)->render() ?>
</div>