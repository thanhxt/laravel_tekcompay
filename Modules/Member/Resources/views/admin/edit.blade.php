@extends('layouts.master')

@section('content-header')
<h1>
    {{ trans('member::members.title.edit-member') }} <small>{{ $user->present()->first_name }}</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class=""><a href="{{ URL::route('admin.member.index') }}">{{ trans('member::members.title.member') }}</a></li>
    <li class="active">{{ trans('member::members.title.edit-member') }}</li>
</ol>
@stop

@section('content')
{!! Form::open(['route' => ['admin.member.update', $user->id], 'method' => 'post']) !!}
@inject('help','Modules\Agency\Utility\Help')
{{ Form::hidden('member_id', $user->id) }}
<div class="row">
    <div class="col-md-10">
        <div class="nav-tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    {!! Form::label('first_name', trans('member::members.form.first-name')) !!}
                                    {!! Form::text('first_name', old('first_name', $user->first_name), ['class' => 'form-control', 'placeholder' => trans('member::members.form.first-name')]) !!}
                                    {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                           <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    {!! Form::label('email', trans('member::members.form.email')) !!}
                                    {!! Form::email('email', old('email', $user->email), ['class' => 'form-control', 'placeholder' => trans('member::members.form.email')]) !!}
                                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
							<div class="col-sm-6">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    {!! Form::label('password', trans('member::members.form.password')) !!}
                                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('member::members.form.password')]) !!}
                                    {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
							<div class="col-sm-6">
                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    {!! Form::label('password_confirmation', trans('member::members.form.password-confirmation')) !!}
                                    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                                    {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-6">
								<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
									{!! Form::label('phone', trans('member::members.form.phone')) !!}
									{!! Form::text('phone', old('phone', $user->phone), ['class' => 'form-control', 'placeholder' => trans('member::members.form.phone')]) !!}
									{!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
									{!! Form::label('website', trans('member::members.form.website')) !!}
									{!! Form::text('website', old('website', $user->website), ['class' => 'form-control', 'placeholder' => trans('member::members.form.website')]) !!}
									{!! $errors->first('website', '<span class="help-block">:message</span>') !!}
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
									{!! Form::label('position', trans('member::members.form.position')) !!}
									{!! Form::text('position', old('position', $user->position), ['class' => 'form-control', 'placeholder' => trans('member::members.form.position')]) !!}
									{!! $errors->first('position', '<span class="help-block">:message</span>') !!}
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group{{ $errors->has('avarta') ? ' has-error' : '' }}">
									{!! Form::label('avarta', trans('member::members.form.avarta')) !!}
									{!! Form::text('avarta', old('avarta', $user->avarta), ['class' => 'form-control', 'placeholder' => trans('member::members.form.avarta')]) !!}
									{!! $errors->first('avarta', '<span class="help-block">:message</span>') !!}
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
									{!! Form::label('description', trans('member::members.form.description')) !!}
									{!! Form::textarea('description', old('description', $user->description), ['class' => 'form-control', 'placeholder' => trans('member::members.form.description')]) !!}
									{!! $errors->first('description', '<span class="help-block">:message</span>') !!}
								</div>
							</div>
                        </div>
                    </div>
                </div>
                
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btn-success">{{ trans('core::core.button.save') }}</button>
                    <button class="btn btn-default btn-flat" name="button" type="reset">{{ trans('core::core.button.reset') }}</button>
                    <a class="btn btn-danger pull-right btn-flat" href="{{ URL::route('admin.user.user.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                </div>
            </div>
        </div>
    </div>
	<div class="col-md-2">
		<div class="box box-primary">
			<div class="box-body">
				<div class="form-group{{ $errors->has('activated') ? ' has-error' : '' }}">
					{!! Form::label('activated', trans('member::members.form.status')) !!}
					<?php $oldValue = (bool) $user->isActivated() ? 'checked' : ''; ?>
					<label for="activated">
						<input id="activated"
							   name="activated"
							   type="checkbox"
							   class="flat-blue"
							   {{ $user->id === $currentUser->id ? 'disabled' : '' }}
							   {{ old('activated', $oldValue) }}
							   value="1" />
						{{ trans('user::users.form.is activated') }}
						{!! $errors->first('activated', '<span class="help-block">:message</span>') !!}
					</label>
				</div>
				<?php 
					$role_id = 0;
					foreach($roles as $id => $name) {
						if ($user->hasRoleId($id)) {
							$role_id = $id;
							break;
						}
					}
				?>
				<div class="form-group{{ $errors->has('roles') ? ' has-error' : '' }}">
                    {!! Form::label('roles', trans('member::members.form.role')) !!}
                    {!! Form::select("roles[]", $roles, $role_id, ['class' => "form-control select-search", 'placeholder' => trans('member::members.form.chosen role'), 'required' => true] ) !!}
                    {!! $errors->first('roles', '<span class="help-block">:message</span>') !!}
                </div>
				
				<div class="form-group{{ $errors->has('agency_id') ? ' has-error' : '' }}">
                    {!! Form::label('agency_id', trans('agency::fee.form.agency_id')) !!}
                    <?php
						$agency_arr = $help->getAgencyArr();
                    ?>
                    {!! Form::select("agency_id", $agency_arr, old('agency_id', $user->agency_id), ['class' => "form-control select-search", 'placeholder' => trans('agency::fee.form.agency_id')]) !!}
                    {!! $errors->first('agency_id', '<span class="help-block">:message</span>') !!}
                </div>
			</div>
		</div>
	</div>
</div>
{!! Form::close() !!}
@stop
@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('member::members.navigation.back to index') }}</dd>
    </dl>
@stop
@section('scripts')
<script>
$( document ).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $(document).keypressAction({
        actions: [
            { key: 'b', route: "<?= route('admin.user.role.index') ?>" }
        ]
    });
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });
});
</script>
@stop
