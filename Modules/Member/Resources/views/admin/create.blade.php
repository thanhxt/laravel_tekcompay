@extends('layouts.master')

@section('content-header')
<h1>
    {{ trans('member::members.title.new-member') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class=""><a href="{{ URL::route('admin.member.index') }}">{{ trans('member::members.title.member') }}</a></li>
    <li class="active">{{ trans('member::members.title.new-member') }}</li>
</ol>
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('member::members.navigation.back to index') }}</dd>
    </dl>
@stop
@section('content')
@inject('help','Modules\Agency\Utility\Help')
{!! Form::open(['route' => 'admin.member.store', 'method' => 'post']) !!}
<div class="row">
    <div class="col-md-10">
        <div class="nav-tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    {!! Form::label('first_name', trans('member::members.form.first-name')) !!}
                                    {!! Form::text('first_name', '', ['class' => 'form-control', 'placeholder' => trans('member::members.form.first-name')]) !!}
                                    {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    {!! Form::label('email', trans('member::members.form.email')) !!}
                                    {!! Form::email('email', '', ['class' => 'form-control', 'placeholder' => trans('member::members.form.email')]) !!}
                                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
							<div class="col-sm-6">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    {!! Form::label('password', trans('member::members.form.password')) !!}
                                    {!! Form::password('password', ['class' => 'form-control']) !!}
                                    {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    {!! Form::label('password_confirmation', trans('member::members.form.password-confirmation')) !!}
                                    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                                    {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
							<div class="col-sm-6">
								<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
									{!! Form::label('phone', trans('member::members.form.phone')) !!}
									{!! Form::text('phone', '', ['class' => 'form-control', 'placeholder' => trans('member::members.form.phone')]) !!}
									{!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
									{!! Form::label('website', trans('member::members.form.website')) !!}
									{!! Form::text('website', '', ['class' => 'form-control', 'placeholder' => trans('member::members.form.website')]) !!}
									{!! $errors->first('website', '<span class="help-block">:message</span>') !!}
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
									{!! Form::label('position', trans('member::members.form.position')) !!}
									{!! Form::text('position', '', ['class' => 'form-control', 'placeholder' => trans('member::members.form.position')]) !!}
									{!! $errors->first('position', '<span class="help-block">:message</span>') !!}
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group{{ $errors->has('avarta') ? ' has-error' : '' }}">
									{!! Form::label('avarta', trans('member::members.form.avarta')) !!}
									{!! Form::text('avarta', '', ['class' => 'form-control', 'placeholder' => trans('member::members.form.avarta')]) !!}
									{!! $errors->first('avarta', '<span class="help-block">:message</span>') !!}
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
									{!! Form::label('description', trans('member::members.form.description')) !!}
									{!! Form::textarea('description', '', ['class' => 'form-control', 'placeholder' => trans('member::members.form.description')]) !!}
									{!! $errors->first('description', '<span class="help-block">:message</span>') !!}
								</div>
							</div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btn-success">{{ trans('core::core.button.save') }}</button>
                    <button class="btn btn-default btn-flat" name="button" type="reset">{{ trans('core::core.button.reset') }}</button>
                    <a class="btn btn-danger pull-right btn-flat" href="{{ URL::route('admin.user.user.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                </div>
            </div>
        </div>

    </div>
	<div class="col-md-2">
		<div class="box box-primary">
			<div class="box-body">
				<div class="form-group{{ $errors->has('roles') ? ' has-error' : '' }}">
                    {!! Form::label('roles', trans('member::members.form.role')) !!}
                    {!! Form::select("roles[]", $roles, null, ['class' => "form-control select-search", 'placeholder' => trans('member::members.form.chosen role'), 'required' => true] ) !!}
                    {!! $errors->first('roles', '<span class="help-block">:message</span>') !!}
                </div>
				
                <div class="form-group{{ $errors->has('agency_id') ? ' has-error' : '' }}">
                    {!! Form::label('agency_id', trans('agency::fee.form.agency_id')) !!}
                    <?php
                    $agency_arr = $help->getAgencyArr();
                    ?>
                    {!! Form::select("agency_id", $agency_arr, null, ['class' => "form-control select-search", 'placeholder' => trans('agency::manage.form.chosen')]) !!}
                    {!! $errors->first('agency_id', '<span class="help-block">:message</span>') !!}
                </div>
			</div>
		</div>
	</div>
</div>
{!! Form::close() !!}
@stop
@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('member::members.navigation.back to index') }}</dd>
    </dl>
@stop
@section('scripts')
<script>
$( document ).ready(function() {
    $(document).keypressAction({
        actions: [
            { key: 'b', route: "<?= route('admin.user.user.index') ?>" }
        ]
    });
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });
});
</script>
@stop
