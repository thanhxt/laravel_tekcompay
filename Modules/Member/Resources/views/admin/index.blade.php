@extends('layouts.master')

@section('content-header')
<h1>
    {{ trans('member::members.title.member') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class="active">{{ trans('member::members.title.member') }}</li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box box-danger">
			<div class="box-body">
				@include('member::admin.partials.search')
			</div>
		</div>
		<div class="box box-danger">
			<div class="box-body">
				@include('member::admin.partials.table')
			</div>
		</div>
	</div>
</div>

@include('core::partials.delete-modal')
@stop
