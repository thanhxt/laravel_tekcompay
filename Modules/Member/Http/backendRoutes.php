<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/member'], function (Router $router) {
// append
    $router->get('list', [
        'as' => 'admin.member.index',
        'uses' => 'MemberController@index',
        'middleware' => 'can:member.all',
    ]);
    $router->get('create', [
        'as' => 'admin.member.create',
        'uses' => 'MemberController@create',
        'middleware' => 'can:member.all',
    ]);

    $router->get('edit', [
        'as' => 'admin.member.edit',
        'uses' => 'MemberController@edit',
        'middleware' => 'can:member.all',
    ]);

    $router->post('store', [
        'as' => 'admin.member.store',
        'uses' => 'MemberController@store',
        'middleware' => 'can:member.all',
    ]);

    $router->post('{member}/update', [
        'as' => 'admin.member.update',
        'uses' => 'MemberController@update',
        'middleware' => 'can:member.all',
    ]);

    $router->delete('{member}/destroy', [
        'as' => 'admin.member.user.destroy',
        'uses' => 'MemberController@destroy',
        'middleware' => 'can:member.all',
    ]);


    // ajax suggest email
    $router->post('ajax-account-suggest', [
        'as' => 'admin.member.suggest',
        'uses' => 'MemberController@ajaxAccountSuggest',
        'middleware' => 'can:member.all',
    ]);

    // change password form
    $router->get('change-pwd', [
        'as' => 'admin.member.changePwd',
        'uses' => 'MemberController@formChangePwd',
        'middleware' => 'can:member.all',
    ]);
    // update password
    $router->post('update-pwd', [
        'as' => 'admin.member.updatePwd',
        'uses' => 'MemberController@updatePassword',
        'middleware' => 'can:member.all',
    ]);
    // change status form
    $router->get('change-status', [
        'as' => 'admin.member.changeStatus',
        'uses' => 'MemberController@formChangeStatus',
        'middleware' => 'can:member.all',
    ]);

    // update password
    $router->post('update-status', [
        'as' => 'admin.member.updateStatus',
        'uses' => 'MemberController@updateStatus',
        'middleware' => 'can:member.all',
    ]);
});
