<?php

namespace Modules\Member\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Http\Response;
use Modules\User\Contracts\Authentication;
use Modules\User\Permissions\PermissionManager;
use Modules\User\Repositories\RoleRepository;
use Modules\User\Repositories\UserRepository;
use Illuminate\Http\Request;
use Modules\Member\Business\MemberBus;
use Modules\Member\Http\Requests\CreateMemberRequest;
use Modules\Member\Http\Requests\UpdateMemberRequest;

class MemberController extends AdminBaseController
{
    /**
     * @var UserRepository
     */
    private $user;
    /**
     * @var RoleRepository
     */
    private $role;
    /**
     * @var Authentication
     */
    private $auth;

    /**
     * @param PermissionManager $permissions
     * @param UserRepository    $user
     * @param RoleRepository    $role
     * @param Authentication    $auth
     */
    public function __construct(
        PermissionManager $permissions,
        UserRepository $user,
        RoleRepository $role,
        Authentication $auth
    ) {
        parent::__construct();

        $this->permissions = $permissions;
        $this->user = $user;
        $this->role = $role;
        $this->auth = $auth;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $name = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
        $email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';

        $member_bus = new MemberBus($this->user);
        $users = $member_bus->search($name, $email);

        return view('member::admin.index', compact('users'));
    }

    public function create()
    {
        $member_bus = new MemberBus($this->user);
        $roles = $member_bus->getListMemberRole();

        return view('member::admin.create', compact('roles'));
    }

    public function edit(Request $request)
    {
        $member_id = $request->get('member_id');
        $member_bus = new MemberBus($this->user);
        $user_info = $member_bus->getUserForEdit($member_id) ;
        $user = $user_info["user"];
        $roles  = $user_info["roles"];
        return view('member::admin.edit', compact('user', 'roles'));
    }

    public function store(CreateMemberRequest $request)
    {
        $member_bus = new MemberBus($this->user);
        $data = $request->all();
        $member_bus->saveMember($data);
        return redirect()->route('admin.member.index')
            ->withSuccess(trans('member::members.messages.user created'));
    }

    public function update(UpdateMemberRequest $request)
    {
        $member_bus = new MemberBus($this->user);
        $data = $request->all();
        $member_bus->saveMember($data);
        return redirect()->route('admin.member.index')
            ->withSuccess(trans('member::members.messages.user update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Block $block
     * @return Response
     */
    public function destroy($id)
    {
        $member_bus = new MemberBus($this->user);
        $member_bus->delete($id);
        return redirect()->route('admin.member.index')
            ->withSuccess(trans('member::members.messages.user deleted'));
    }

    public function ajaxAccountSuggest(Request $request)
    {
        $data["list"] = [];
        return response($data);
    }
}
