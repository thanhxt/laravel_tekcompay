<?php

namespace Modules\Member\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMemberRequest extends FormRequest
{
    public function rules()
    {
        $member = $this->route()->parameter('member');

        return [
            'email' => "required|email|unique:users,email,{$member}",
            'first_name' => 'required',
            'roles' => 'required',
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }
}
