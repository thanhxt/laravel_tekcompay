<?php

namespace Modules\Agency\Utility;

use Modules\Bill\Entities\Agency;
use Modules\Bill\Entities\AgencyType;

class Help
{

    public function getAgencyArr() {
        $agencies = Agency::all();
        $data = [];
        if ($agencies->count()) {
            foreach ($agencies as $item) {
                $data[$item->id] = $item->name;
            }
        }

        return $data;
    }

    public function getAgencyTypeArr() {
        $data_type = AgencyType::all();
        $data = [];
        if ($data_type->count()) {
            foreach ($data_type as $type) {
                $data[$type->id] = $type->name;
            }
        }

        return $data;
    }

    public function getVisibleArr() {
        $data = ['Chưa kích hoạt', 'Kích hoạt'];

        return $data;
    }

    public function getStatusArr() {
        $data = ['Ẩn', 'Hiện'];

        return $data;
    }

    public function formatDate($date, $type = 'Y-m-d') {
        $date = \Carbon\Carbon::parse($date)->format($type);
        return $date;
    }
}