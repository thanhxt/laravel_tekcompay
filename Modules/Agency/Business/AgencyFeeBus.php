<?php

namespace Modules\Agency\Business;

use Modules\Bill\Entities\Agency;
use Modules\Bill\Entities\AgencyType;
use Modules\Bill\Entities\AgencyFee;

class AgencyFeeBus
{
    public function search($agency_name, $fee_name, $pageSize = PAGE_SIZE) {
        $agency_model = new AgencyFee();
        $data = $agency_model->search($agency_name, $fee_name, $pageSize);
        return $data;
    }

    public function saveData($data) {
        $id = $data["id"];
        $fee_model = $this->getFeeModelById($id);
        return $fee_model->saveData($data);
    }

    public function getFeeModelById($id) {
        if($id){
            $model = AgencyFee::find($id);
        }else{
            $model = new AgencyFee();
        }
        return $model;
    }

    public function delete($id) {
        $result = AgencyFee::destroy($id);
        return $result;
    }

    public function getFeeByAgencyId($agency_id)
    {
        $fee = 2;
        $model = AgencyFee::where('agency_id', '=', $agency_id)->first();
        if (isset($model)) {
            $fee = $model->fee;
        }
        return $fee;
    }
}