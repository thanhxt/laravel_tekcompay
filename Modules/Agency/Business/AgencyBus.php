<?php

namespace Modules\Agency\Business;

use Modules\Bill\Entities\Agency;
use Modules\Bill\Entities\AgencyType;
use Modules\Bill\Entities\AgencyFee;

class AgencyBus
{
    public function search($name, $code, $contact_name, $phone, $pageSize = PAGE_SIZE) {
        $agency_model = new Agency();
        $data = $agency_model->search($name, $code, $contact_name, $phone, $pageSize);
        return $data;
    }

    public function saveData($data) {
        $id = $data["id"];
        $agency_model = $this->getAgencyModelById($id);
        return $agency_model->saveData($data);
    }

    public function getAgencyModelById($id) {
        if($id){
            $model = Agency::find($id);
        }else{
            $model = new Agency();
        }
        return $model;
    }

    public function delete($id) {
        $result = Agency::destroy($id);
        return $result;
    }
}