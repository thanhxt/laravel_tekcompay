@extends('layouts.master')

@section('content-header')
	<?php 
		$translate_path = 'agency::manage.title.create agency';
		if(@$model->id > 0){
			$translate_path = 'agency::manage.title.edit agency';
		}
	?>
    <h1>
        {{ trans($translate_path) }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ URL::route('admin.agency.manage.index') }}">{{ trans('agency::manage.title.list') }}</a></li>
        <li class="active">{{ trans($translate_path) }}</li>
    </ol>
@stop

@section('content')
@inject('help','Modules\Agency\Utility\Help')
    {!! Form::open(['route' => ['admin.agency.manage.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <div class="tab-content">
					<div class="tab-pane active" id="tab_1-1">
						<div class="box-body">
							<div class="row">
								{!! Form::hidden('id', @$model->id, []) !!}
								<div class="col-sm-6">
									<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
										{!! Form::label('name', trans('agency::manage.form.name')) !!}
										{!! Form::text("name",@$model->name, ['class' => "form-control", 'placeholder' => trans('agency::manage.form.name'), "id"=>"name",'required'=>'required']) !!}
										{!! $errors->first('name', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
										{!! Form::label('code', trans('agency::manage.form.code')) !!}
										{!! Form::text("code",@$model->code, ['class' => "form-control", 'placeholder' => trans('agency::manage.form.code'), "id"=>"code",'required'=>'required']) !!}
										{!! $errors->first('code', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group{{ $errors->has('contact_name') ? ' has-error' : '' }}">
										{!! Form::label('contact_name', trans('agency::manage.form.contact_name')) !!}
										{!! Form::text("contact_name",@$model->contact_name, ['class' => "form-control", 'placeholder' => trans('agency::manage.form.contact_name'), 'required'=>'required']) !!}
										{!! $errors->first('contact_name', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
										{!! Form::label('phone', trans('agency::manage.form.phone')) !!}
										{!! Form::text("phone",@$model->phone, ['class' => "form-control", 'placeholder' => trans('agency::manage.form.phone'), 'required'=>'required']) !!}
										{!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group{{ $errors->has('type_id') ? ' has-error' : '' }}">
										{!! Form::label('type_id', trans('agency::manage.form.type_id')) !!}
										<?php
											$agency_types = $help->getAgencyTypeArr();
										?>
										{!! Form::select("type_id", $agency_types, @$model->type_id, ['class' => "form-control select-search", 'placeholder' => trans('agency::manage.form.type_id')]) !!}
										{!! $errors->first('type_id', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group{{ $errors->has('visible') ? ' has-error' : '' }}">
										{!! Form::label('visible', trans('agency::manage.form.visible')) !!}
										<?php
											$visible_arr = $help->getVisibleArr();
										?>
										{!! Form::select("visible", $visible_arr, @$model->visible, ['class' => "form-control select-search", 'placeholder' => trans('agency::manage.form.visible')]) !!}
										{!! $errors->first('visible', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
										{!! Form::label('address', trans('agency::manage.form.address')) !!}
										{!! Form::textarea('address', @$model->address, ['class' => 'form-control', 'placeholder' => trans('agency::manage.form.address')]) !!}
										{!! $errors->first('address', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="box-footer">
						<?php 
							$btn_txt = 'core::core.button.save';
						?>
                        <button type="submit" class="btn btn-primary btn-success">{{ trans($btn_txt) }}</button>
                        <button class="btn btn-default btn-flat" name="button" type="reset">{{ trans('core::core.button.reset') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ URL::route('admin.agency.manage.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>

    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('page::pages.navigation.back to index') }}</dd>
    </dl>
@stop

@section('scripts')
    <script type="text/javascript">
        $(function() {
           /* CKEDITOR.replaceAll(function( textarea, config ) {
                if (!$(textarea).hasClass('ckeditor')) {
                    return false;
                }
                config.language = '<?php echo App::getLocale() ?>';
            } );*/
        });
    </script>
    <script>
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.page.page.index') ?>" }
                ]
            });
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

            $('input[type="checkbox"]').on('ifChecked', function(){
                $(this).parent().find('input[type=hidden]').remove();
            });

            $('input[type="checkbox"]').on('ifUnchecked', function(){
                var name = $(this).attr('name'),
                    input = '<input type="hidden" name="' + name + '" value="0" />';
                $(this).parent().append(input);
            });
        });
    </script>
@stop
