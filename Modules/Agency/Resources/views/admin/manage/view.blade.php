@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('agency::manage.title.view agency') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ URL::route('admin.agency.manage.index') }}">{{ trans('agency::manage.title.list') }}</a></li>
        <li class="active">{{ trans('agency::manage.title.view agency') }}</li>
    </ol>
@stop

@section('content')
@inject('help','Modules\Agency\Utility\Help')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-danger">
				<div class="box-header">
					<a class="btn btn-success" href="{{ route('admin.agency.manage.edit', ['id' => $model->id]) }}"><i class="fa fa-pencil"></i> {{ trans('agency::manage.button.edit') }}</a>
				</div>
				<div class="box-body">
					<table class='table table-striped table-bordered detail-view'>
						<tr>
							<td width='25%'>Id</td>
							<td>{{ $model->id }}</td>
						</tr>
						<tr>
							<td width='25%'>{{ trans('agency::manage.table.name') }}</td>
							<td>{{ $model->name }}</td>
						</tr>
						<tr>
							<td width='25%'>{{ trans('agency::manage.table.code') }}</td>
							<td>{{ $model->code }}</td>
						</tr>
						<tr>
							<td width='25%'>{{ trans('agency::manage.table.contact_name') }}</td>
							<td>{{ $model->contact_name }}</td>
						</tr>
						<tr>
							<td width='25%'>{{ trans('agency::manage.table.phone') }}</td>
							<td>{{ $model->phone }}</td>
						</tr>
						<tr>
							<td width='25%'>{{ trans('agency::manage.table.address') }}</td>
							<td>{{ $model->address }}</td>
						</tr>
						<tr>
							<td width='25%'>{{ trans('agency::manage.table.type_id') }}</td>
							<td>
								<?php 
									$type_arr = $help->getAgencyTypeArr();
									$type_txt = $type_arr[$model->type_id];
								?>
								{{ $type_txt }}
							</td>
						</tr>
						<tr>
							<td width='25%'>{{ trans('agency::manage.table.visible') }}</td>
							<td>
								<?php 
									$visible_arr = $help->getVisibleArr();
									$visible_txt = $visible_arr[$model->visible];
								?>
								{{ $visible_txt }}
							</td>
						</tr>
					</table>
				</div>
			</div>
        </div>
    </div>
	@include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('page::pages.navigation.back to index') }}</dd>
    </dl>
@stop

@section('scripts')
    <script type="text/javascript">
        $(function() {
           /* CKEDITOR.replaceAll(function( textarea, config ) {
                if (!$(textarea).hasClass('ckeditor')) {
                    return false;
                }
                config.language = '<?php echo App::getLocale() ?>';
            } );*/
        });
    </script>
    <script>
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.page.page.index') ?>" }
                ]
            });
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

            $('input[type="checkbox"]').on('ifChecked', function(){
                $(this).parent().find('input[type=hidden]').remove();
            });

            $('input[type="checkbox"]').on('ifUnchecked', function(){
                var name = $(this).attr('name'),
                    input = '<input type="hidden" name="' + name + '" value="0" />';
                $(this).parent().append(input);
            });
        });
    </script>
@stop
