@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('agency::fee.title.view fee') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ URL::route('admin.agency.manage.index') }}">{{ trans('agency::fee.title.list') }}</a></li>
        <li class="active">{{ trans('agency::fee.title.view fee') }}</li>
    </ol>
@stop

@section('content')
@inject('help','Modules\Agency\Utility\Help')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-danger">
				<div class="box-header">
					<a class="btn btn-success" href="{{ route('admin.agency.fee.edit', ['id' => $model->id]) }}"><i class="fa fa-pencil"></i> {{ trans('agency::fee.button.edit') }}</a>
				</div>
				<div class="box-body">
					<table class='table table-striped table-bordered detail-view'>
						<tr>
							<td width='25%'>Id</td>
							<td>{{ $model->id }}</td>
						</tr>
						<tr>
							<td width='25%'>{{ trans('agency::fee.table.agency_id') }}</td>
							<td>
								<?php 
									$agency_arr = $help->getAgencyArr();
									$agency_txt = $agency_arr[$model->agency_id];
								?>
								{{ $agency_txt }}
							</td>
						</tr>
						<tr>
							<td width='25%'>{{ trans('agency::fee.table.fee') }}</td>
							<td>{{ $model->fee }}</td>
						</tr>
						<tr>
							<td width='25%'>{{ trans('agency::fee.table.fee_name') }}</td>
							<td>{{ $model->fee_name }}</td>
						</tr>
						<tr>
							<td width='25%'>{{ trans('agency::fee.table.date_effect') }}</td>
							<td>
								<?php 
									$date_effect = $help->formatDate($model->date_effect);
								?>
								{{ $date_effect }}
							</td>
						</tr>
						<tr>
							<td width='25%'>{{ trans('agency::fee.table.status') }}</td>
							<td>
								<?php 
									$visible_arr = $help->getVisibleArr();
									$visible_txt = $visible_arr[$model->status];
								?>
								{{ $visible_txt }}
							</td>
						</tr>
					</table>
				</div>
			</div>
        </div>
    </div>
	@include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('page::pages.navigation.back to index') }}</dd>
    </dl>
@stop
