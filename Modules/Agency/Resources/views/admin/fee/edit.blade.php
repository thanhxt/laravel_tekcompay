@extends('layouts.master')

@section('styles')
	{!! Theme::style('vendor/datepicker/css/bootstrap-datepicker.css') !!}
@stop

@section('content-header')
	<?php 
		$translate_path = 'agency::fee.title.create fee';
		if(@$model->id > 0){
			$translate_path = 'agency::fee.title.edit fee';
		}
	?>
    <h1>
        {{ trans($translate_path) }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ URL::route('admin.agency.fee.index') }}">{{ trans('agency::fee.title.list') }}</a></li>
        <li class="active">{{ trans($translate_path) }}</li>
    </ol>
@stop

@section('content')
@inject('help','Modules\Agency\Utility\Help')
    {!! Form::open(['route' => ['admin.agency.fee.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <div class="tab-content">
					<div class="tab-pane active" id="tab_1-1">
						<div class="box-body">
							<div class="row">
								{!! Form::hidden('id', @$model->id, []) !!}
								<div class="col-sm-4">
									<div class="form-group{{ $errors->has('agency_id') ? ' has-error' : '' }}">
										{!! Form::label('agency_id', trans('agency::fee.form.agency_id')) !!}
										<?php
											$agency_arr = $help->getAgencyArr();
										?>
										{!! Form::select("agency_id", $agency_arr, @$model->agency_id, ['class' => "form-control select-search", 'placeholder' => trans('agency::fee.form.agency_id')]) !!}
										{!! $errors->first('agency_id', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group{{ $errors->has('fee_name') ? ' has-error' : '' }}">
										{!! Form::label('fee_name', trans('agency::fee.form.fee_name')) !!}
										{!! Form::text("fee_name",@$model->fee_name, ['class' => "form-control", 'placeholder' => trans('agency::fee.form.fee_name'), "id"=>"fee_name",'required'=>'required']) !!}
										{!! $errors->first('fee_name', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group{{ $errors->has('fee') ? ' has-error' : '' }}">
										{!! Form::label('fee', trans('agency::fee.form.fee')) !!}
										<div class="input-group">
											{!! Form::text("fee",@$model->fee, ['class' => "form-control", 'placeholder' => trans('agency::fee.form.fee'), "id"=>"fee",'required'=>'required']) !!}
											<span class="input-group-addon">%</span>
										</div>
										{!! $errors->first('fee', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group{{ $errors->has('date_effect') ? ' has-error' : '' }}">
										{!! Form::label('date_effect', trans('agency::fee.form.date_effect')) !!}
										{!! Form::text("date_effect",@$model->date_effect, ['class' => "form-control", 'placeholder' => trans('agency::fee.form.date_effect'), "id"=>"date_effect",'required'=>'required', 'readonly' => true]) !!}
										{!! $errors->first('date_effect', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
							<div class='row'>
								<div class="col-sm-4">
									<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
										{!! Form::label('status', trans('agency::fee.form.status')) !!}
										<?php
											$visible_arr = $help->getVisibleArr();
										?>
										{!! Form::select("status", $visible_arr, @$model->status, ['class' => "form-control select-search", 'placeholder' => trans('agency::fee.form.status')]) !!}
										{!! $errors->first('status', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="box-footer">
						<?php 
							$btn_txt = 'core::core.button.save';
						?>
                        <button type="submit" class="btn btn-primary btn-success">{{ trans($btn_txt) }}</button>
                        <button class="btn btn-default btn-flat" name="button" type="reset">{{ trans('core::core.button.reset') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ URL::route('admin.agency.fee.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>

    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('page::pages.navigation.back to index') }}</dd>
    </dl>
@stop

@section('scripts')
	{!! Theme::script('vendor/datepicker/js/bootstrap-datepicker.js') !!}
	{!! Theme::script('vendor/inputmask/js/jquery.inputmask.js') !!}
    <script>
        $( document ).ready(function() {
			$.fn.datepicker.defaults.format = "yyyy-mm-dd";
			$('#date_effect').datepicker({});
			
			$("#fee").inputmask({"alias":"numeric","groupSeparator":",","autoGroup":true,"removeMaskOnSubmit":true});
        });
    </script>
@stop
