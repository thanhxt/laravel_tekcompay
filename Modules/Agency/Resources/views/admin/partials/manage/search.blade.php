<div class="row">
	<div class="col-md-12">
		<div class="nav-tabs-custom">
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1-1">
					{!! Form::open(['route' => ['admin.agency.manage.index'], 'method' => 'get', 'id' => 'search_form']) !!}
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('name', trans('agency::manage.form.name')) !!}
									{!! Form::text("name",old("name"), ['class' => "form-control", 'placeholder' => trans('agency::manage.form.name')]) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('code', trans('agency::manage.form.code')) !!}
									{!! Form::text("code", old("code"), ['class' => "form-control", 'placeholder' => trans('agency::manage.form.code')]) !!}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('contact_name', trans('agency::manage.form.contact_name')) !!}
									{!! Form::text("contact_name",old("contact_name"), ['class' => "form-control", 'placeholder' => trans('agency::manage.form.contact_name')]) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('phone', trans('agency::manage.form.phone')) !!}
									{!! Form::text("phone", old("phone"), ['class' => "form-control", 'placeholder' => trans('agency::manage.form.phone')]) !!}
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group search-control">
									<button type="submit" class="btn btn-primary btn-danger">{{ trans('member::members.button.search') }}</button>
									<label>&nbsp;</label>
									<a href="{{ URL::route('admin.agency.manage.edit') }}" class="btn btn-primary btn-success" style="padding: 4px 10px;">
										<i class="fa fa-pencil"></i> {{ trans('agency::manage.button.agency create') }}
									</a>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>	
			</div>	
		</div>	
	</div>	
</div>	

			
