<table class="data-table table table-bordered table-hover">
	<thead>
		<tr>
			<th>#</th>
			<th>{{ trans('agency::manage.table.name') }}</th>
			<th>{{ trans('agency::manage.table.code') }}</th>
			<th>{{ trans('agency::manage.table.contact_name') }}</th>
			<th>{{ trans('agency::manage.table.phone') }}</th>
			<th data-sortable="false">{{ trans('member::members.table.actions') }}</th>
		</tr>
	</thead>
	<tbody>
	<?php if (count($agencies)): ?>
		<?php foreach ($agencies as $item): ?>
		<tr>
			<td>
				{{ $item->id }}
			</td>
			<td>
				{{ $item->name }}
			</td>
			<td>
				{{ $item->code }}
			</td>
			<td>
				{{ $item->contact_name }}
			</td>
			<td>
				{{ $item->phone }}
			</td>
			<td>
				<div class="btn-group">
					<a href="{{ route('admin.agency.manage.view', [$item->id]) }}" class="btn btn-default btn-flat" title="{{ trans('member::members.icon.caption view') }}"><i class="fa fa-eye"></i></a>
					<a href="{{ route('admin.agency.manage.edit', ['id' => $item->id]) }}" class="btn btn-default btn-flat" title="{{ trans('member::members.icon.caption edit') }}"><i class="fa fa-pencil"></i></a>
				</div>
			</td>
		</tr>
		<?php endforeach; ?>
	<?php else: ?>
		<tr>
			<td colspan='6'>{{ trans('member::members.table.empty') }}</td>
		</tr>	
	<?php endif; ?>
	</tbody>
</table>
<div class='text-center'>
	<?php
		$data = request()->all();
		unset($data["_token"]);
	?>
	<?php  echo $agencies->appends($data)->render() ?>
</div>