<div class="row">
	<div class="col-md-12">
		<div class="nav-tabs-custom">
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1-1">
					{!! Form::open(['route' => ['admin.agency.fee.index'], 'method' => 'get', 'id' => 'search_form']) !!}
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('agency_name', trans('agency::manage.form.name')) !!}
									{!! Form::text("agency_name",old("agency_name"), ['class' => "form-control", 'placeholder' => trans('agency::manage.form.name')]) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									{!! Form::label('fee_name', trans('agency::fee.form.fee_name')) !!}
									{!! Form::text("fee_name", old("fee_name"), ['class' => "form-control", 'placeholder' => trans('agency::fee.form.fee_name')]) !!}
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group search-control">
									<button type="submit" class="btn btn-primary btn-danger">{{ trans('member::members.button.search') }}</button>
									<label>&nbsp;</label>
									<a href="{{ URL::route('admin.agency.fee.edit') }}" class="btn btn-primary btn-success" style="padding: 4px 10px;">
										<i class="fa fa-pencil"></i> {{ trans('agency::fee.button.fee create') }}
									</a>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>	
			</div>	
		</div>	
	</div>	
</div>	

			
