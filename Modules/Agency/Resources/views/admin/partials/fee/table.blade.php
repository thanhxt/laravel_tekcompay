@inject('help','Modules\Agency\Utility\Help')
<table class="data-table table table-bordered table-hover">
	<thead>
		<tr>
			<th>#</th>
			<th>{{ trans('agency::fee.table.agency_id') }}</th>
			<th>{{ trans('agency::fee.table.fee') }}</th>
			<th>{{ trans('agency::fee.table.fee_name') }}</th>
			<th>{{ trans('agency::fee.table.date_effect') }}</th>
			<th data-sortable="false">{{ trans('member::members.table.actions') }}</th>
		</tr>
	</thead>
	<tbody>
	<?php if (count($fees)): ?>
		<?php foreach ($fees as $item): ?>
		<tr>
			<td>
				{{ $item->id }}
			</td>
			<td>
				<?php 
					$agency_arr = $help->getAgencyArr();
					$agency_txt = $agency_arr[$item->agency_id];
				?>
				{{ $agency_txt }}
			</td>
			<td>
				{{ $item->fee }}
			</td>
			<td>
				{{ $item->fee_name }}
			</td>
			<td>
				<?php 
					$date_effect = $help->formatDate($item->date_effect);
				?>
				{{ $date_effect }}
			</td>
			<td>
				<div class="btn-group">
					<a href="{{ route('admin.agency.fee.view', [$item->id]) }}" class="btn btn-default btn-flat" title="{{ trans('member::members.icon.caption view') }}"><i class="fa fa-eye"></i></a>
					<a href="{{ route('admin.agency.fee.edit', ['id' => $item->id]) }}" class="btn btn-default btn-flat" title="{{ trans('member::members.icon.caption edit') }}"><i class="fa fa-pencil"></i></a>
					<button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.agency.fee.delete', [$item->id]) }}" title="{{ trans('member::members.icon.caption delete') }}"><i class="fa fa-trash"></i></button>
				</div>
			</td>
		</tr>
		<?php endforeach; ?>
	<?php else: ?>
		<tr>
			<td colspan='6'>{{ trans('member::members.table.empty') }}</td>
		</tr>	
	<?php endif; ?>
	</tbody>
</table>
<div class='text-center'>
	<?php
		$data = request()->all();
		unset($data["_token"]);
	?>
	<?php echo $fees->appends($data)->render() ?>
</div>
