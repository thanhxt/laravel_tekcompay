<?php

namespace Modules\Agency\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ManageRequest extends FormRequest
{
    public function rules()
    {
        $id = request()->get('id');
        $code_unique = $name_unique = 'required|unique:agencies';
        if ($id) {
            $code_unique = $code_unique . ',code,' . $id;
            $name_unique = $name_unique . ',name,' . $id;
        }

        return [
            'code' => $code_unique,
            'name' => $name_unique,
            'type_id' => 'required',
            'visible' => 'required',
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'required' => trans('agency::manage.messages.valid required'),
            'code.unique' => trans('agency::manage.messages.valid code unique'),
            'name.unique' => trans('agency::manage.messages.valid name unique'),
        ];
    }
}
