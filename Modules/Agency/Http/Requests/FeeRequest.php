<?php

namespace Modules\Agency\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FeeRequest extends FormRequest
{
    public function rules()
    {
        $id = request()->get('id');
        $name_unique = 'required|unique:agencyfees';
        if ($id) {
            $name_unique = $name_unique . ',fee_name,' . $id;
        }

        return [
            'agency_id' => 'required',
            'fee' => 'required',
            'fee_name' => $name_unique,
            'fee' => 'required',
            'date_effect' => 'required',
            'status' => 'required',
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'required' => trans('agency::fee.messages.valid required'),
            'fee_name.unique' => trans('agency::fee.messages.valid fee_name unique'),
        ];
    }
}
