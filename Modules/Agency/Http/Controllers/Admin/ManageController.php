<?php

namespace Modules\Agency\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Http\Request;
use Modules\Agency\Business\AgencyBus;
use Modules\Agency\Http\Requests\ManageRequest;

class ManageController extends AdminBaseController
{

    public function index()
    {
        $name = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
        $code = isset($_REQUEST['code']) ? $_REQUEST['code'] : '';
        $contact_name = isset($_REQUEST['contact_name']) ? $_REQUEST['contact_name'] : '';
        $phone = isset($_REQUEST['phone']) ? $_REQUEST['phone'] : '';
        $agency_business = new AgencyBus();
        $agencies = $agency_business->search($name, $code, $contact_name, $phone);

        return view('agency::admin.manage.index', compact('agencies'));
    }

    public function edit(Request $request)
    {
        $data = $request->all();
        $id = @$data['id'];
        $agency_business = new AgencyBus();
        $model = $agency_business->getAgencyModelById($id);
        return view('agency::admin.manage.edit', compact('model'));
    }

    public function store(ManageRequest $request)
    {
        $data = $request->all();
        $agency_business = new AgencyBus();
        $bResult = $agency_business->saveData($data);
        if ($bResult) {
            $msg_path = 'agency::manage.messages.created success';
            if ($data['id']) {
                $msg_path = 'agency::manage.messages.updated success';
            }
            return redirect()->route('admin.agency.manage.index')
                ->withSuccess(trans($msg_path));
        } else {
            $msg_path = 'agency::manage.messages.created fail';
            if ($data['id']) {
                $msg_path = 'agency::manage.messages.updated fail';
            }
            return redirect()->route('admin.agency.manage.index')
                ->withError(trans($msg_path));
        }
    }

    public function delete($id)
    {
        $agency_business = new AgencyBus();
        $bResult = $agency_business->delete($id);

        if ($bResult) {
            return redirect()->route('admin.agency.manage.index')
                ->withSuccess(trans('agency::manage.messages.delete success'));
        } else {
            return redirect()->route('admin.agency.manage.index')
                ->withError(trans('agency::manage.messages.delete fail'));
        }
    }

    public function view($id)
    {
        $agency_business = new AgencyBus();
        $model = $agency_business->getAgencyModelById($id);
        return view('agency::admin.manage.view', compact('model'));
    }

}
