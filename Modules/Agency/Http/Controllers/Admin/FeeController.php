<?php

namespace Modules\Agency\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Http\Request;
use Modules\Agency\Business\AgencyFeeBus;
use Modules\Agency\Http\Requests\FeeRequest;

class FeeController extends AdminBaseController
{
    public function index()
    {
        $agency_name = isset($_REQUEST['agency_name']) ? $_REQUEST['agency_name'] : '';
        $fee_name = isset($_REQUEST['fee_name']) ? $_REQUEST['fee_name'] : '';
        $fee_bus = new AgencyFeeBus();
        $fees = $fee_bus->search($agency_name, $fee_name);

        return view('agency::admin.fee.index',compact('fees'));
    }

    public function edit(Request $request)
    {
        $data = $request->all();
        $id = @$data['id'];
        $fee_bus = new AgencyFeeBus();
        $model = $fee_bus->getFeeModelById($id);
        return view('agency::admin.fee.edit',compact('model'));
    }

    public function store(FeeRequest $request)
    {
        $data = $request->all();
        $fee_bus = new AgencyFeeBus();
        $bResult = $fee_bus->saveData($data);
        if ($bResult) {
            $msg_path = 'agency::fee.messages.created success';
            if ($data['id']) {
                $msg_path = 'agency::fee.messages.updated success';
            }
            return redirect()->route('admin.agency.fee.index')
                ->withSuccess(trans($msg_path));
        } else {
            $msg_path = 'agency::fee.messages.created fail';
            if ($data['id']) {
                $msg_path = 'agency::fee.messages.updated fail';
            }
            return redirect()->route('admin.agency.fee.index')
                ->withError(trans($msg_path));
        }
    }

    public function delete($id)
    {
        $fee_bus = new AgencyFeeBus();
        $bResult = $fee_bus->delete($id);
        if ($bResult) {
            return redirect()->route('admin.agency.fee.index')
                ->withSuccess(trans('agency::fee.messages.delete success'));
        } else {
            return redirect()->route('admin.agency.fee.index')
                ->withError(trans('agency::fee.messages.delete fail'));
        }
    }

    public function view($id)
    {
        $fee_bus = new AgencyFeeBus();
        $model = $fee_bus->getFeeModelById($id);
        return view('agency::admin.fee.view', compact('model'));
    }
}
