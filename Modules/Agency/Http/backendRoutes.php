<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/agency'], function (Router $router) {
    // manage agency
    $router->get('manage/index', [
        'as' => 'admin.agency.manage.index',
        'uses' => 'ManageController@index',
        'middleware' => 'can:agency.manage.all',
    ]);

    $router->get('manage/edit', [
        'as' => 'admin.agency.manage.edit',
        'uses' => 'ManageController@edit',
        'middleware' => 'can:agency.manage.all',
    ]);

    $router->post('manage/store', [
        'as' => 'admin.agency.manage.store',
        'uses' => 'ManageController@store',
        'middleware' => 'can:agency.manage.all',
    ]);

    /*$router->delete('manage/delete/{id}', [
        'as' => 'admin.agency.manage.delete',
        'uses' => 'ManageController@delete',
        'middleware' => 'can:agency.manage.all',
    ]);*/

    $router->get('manage/view/{id}', [
        'as' => 'admin.agency.manage.view',
        'uses' => 'ManageController@view',
        'middleware' => 'can:agency.manage.all',
    ]);

    // =============== manage fee agency ===============
    $router->get('fee/index', [
        'as' => 'admin.agency.fee.index',
        'uses' => 'FeeController@index',
        'middleware' => 'can:agency.fee.all',
    ]);

    $router->get('fee/edit', [
        'as' => 'admin.agency.fee.edit',
        'uses' => 'FeeController@edit',
        'middleware' => 'can:agency.fee.all',
    ]);

    $router->post('fee/store', [
        'as' => 'admin.agency.fee.store',
        'uses' => 'FeeController@store',
        'middleware' => 'can:agency.fee.all',
    ]);

    $router->delete('fee/delete/{id}', [
        'as' => 'admin.agency.fee.delete',
        'uses' => 'FeeController@delete',
        'middleware' => 'can:agency.fee.all',
    ]);

    $router->get('fee/view/{id}', [
        'as' => 'admin.agency.fee.view',
        'uses' => 'FeeController@view',
        'middleware' => 'can:agency.fee.all',
    ]);
});
