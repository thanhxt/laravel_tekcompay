<?php

namespace Modules\Agency\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterAgencySidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }
    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('member::members.title.system'), function (Group $group) {
            $group->weight(902);
            $group->item(trans('agency::manage.title.list'), function (Item $item) {
                $item->weight(2);
                $item->icon('fa fa-building-o');
                $item->authorize(
                    $this->auth->hasAccess('agency.manage.all')
                );

                $item->item(trans('agency::manage.title.list'), function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-building');
                    $item->route('admin.agency.manage.index');
                    $item->authorize(
                        $this->auth->hasAccess('agency.manage.all')
                    );
                });

                $item->item(trans('agency::fee.title.list'), function (Item $item) {
                    $item->weight(1);
                    $item->icon('fa fa-money');
                    $item->route('admin.agency.fee.index');
                    $item->authorize(
                        $this->auth->hasAccess('agency.fee.all')
                    );
                });
            });
        });

        return $menu;
    }
}
